//
//  BottomView.swift
//  Favourite StationsExtension
//
//  Created by Александр Краснощеков on 21.10.2021.
//  Copyright © 2021 Александр Краснощеков. All rights reserved.
//

import SwiftUI

struct BottomView: View {
    
    var stationsArray: [StationData]
    
    var body: some View {
        VStack(alignment: .leading, spacing: 0) {
            HStack {
                ForEach(0..<stationsArray.count) { index in
                    if index > 0 && index < 3 {
                        StationView(station: stationsArray[index])
                        if index != 2 {
                            Spacer()
                        }
                    }
                        if (stationsArray.count == 1) {
                            Spacer()
                                .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: .infinity)
                        }

                }
            }
            .frame(minWidth: 0, maxWidth: .infinity)
//            if stationsArray.count >= 3 {
//                HStack {
//                    ForEach(0..<stationsArray.count) { index in
//
//                        if index >= 3 && index < 5 {
//                            StationView(station: stationsArray[index])
//                        }
//                    }
//                }
//            }
//
//            if stationsArray.count >= 5 {
//                HStack {
//                    ForEach(0..<stationsArray.count) { index in
//
//                        if index >= 5 && index < 7 {
//                            StationView(station: stationsArray[index])
//                        }
//                    }
//                }
//            }
        }
        .padding(.horizontal, 14)
        .padding(.top, 10)
        .padding(.bottom, 12)
        //.padding(.init(entry.stations.count > 4 ? 8 : 16))
    }
}
