//
//  TopView.swift
//  Favourite StationsExtension
//
//  Created by Александр Краснощеков on 21.10.2021.
//  Copyright © 2021 Александр Краснощеков. All rights reserved.
//

import SwiftUI

struct TopView: View {
    
    var stationsArray: [StationData]
    
    var body: some View {
        VStack(alignment: .leading, spacing: 0) {//
            HStack {
                ForEach(0..<stationsArray.count) { index in
                    if index == 0 {
                        Link(destination: URL(string: "radiovanya://station/?streamId=\(index)")!, label: {
//                            if entry.track.image200 != "" {
//                                NetworkImage(url: URL(string: entry.track.image200))
//                                    .scaledToFit()
//                                    .padding(.leading, 2)
//                                    .shadow(color: Color.black.opacity(0.2), radius: 4, x: 4, y: 4)
//                            }else{
                                NetworkImage(url: URL(string: stationsArray[index].icon_gray))
                                    .scaledToFit()
//                                    .padding(.leading, 2)
                                    .shadow(color: Color.black.opacity(0.2), radius: 4, x: 4, y: 4)
//                            }
                            
                            Spacer()
                                .frame(width: 10)
                            
                            VStack (alignment: .leading, spacing: 0) {
                                if stationsArray[0].title != "" {
                                    Text(stationsArray[0].title)
                                        .foregroundColor(Color("titleLabel"))
                                        .lineLimit(1)
                                        .font(
                                            .system(size: 16)
                                                .bold()
                                        )
                                }
                                
                                Spacer()
                                    .frame(height: 4)
                                
                                if stationsArray[0].track.artist != "" {
                                    Text(stationsArray[0].track.artist)
                                        .foregroundColor(Color("artistLabel"))
                                        .lineLimit(1)
                                        .font(
                                            .system(size: 14)
                                                //.bold()
                                        )
                                }
//                                else{
//                                    Text(entry.stations[0].title).foregroundColor(Color(#colorLiteral(red: 0.631372549, green: 0.631372549, blue: 0.631372549, alpha: 1))).lineLimit(1).font(.system(size: 15).bold())
//                                }
                                
                                if stationsArray[0].track.song != "" {
                                    Text(stationsArray[0].track.song)
                                        .foregroundColor(Color("songLabel"))
                                        .lineLimit(1)
                                        .font(
                                            .system(size: 14)
                                                //.bold()
                                        )
                                }else{
                                    Text(stationsArray[0].tooltip)
                                        .foregroundColor(Color("songLabel"))
                                        .lineLimit(1)
                                        .font(
                                            .system(size: 14)
                                                //.bold()
                                        )
                                }
                                
                            }
                            .padding(.top, 4)
                            .padding(.bottom, 4)
                            
                            Spacer()
                            
                            VStack{
                                Image("record_image600_white_fill")
                                    .renderingMode(.template)
                                    .resizable()
                                    .aspectRatio(contentMode: .fit)
                                    .frame(width: 40.0)
                                    .foregroundColor(Color("recordIconColor"))
                                
                                Spacer()
                            }
                            .padding(.top, -10)
                            .padding(.trailing, -4)
                        })
                    }
                }
            }
        }
        .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: .infinity)
        .padding(.horizontal, 14)
        .padding(.top, 10)
        .padding(.bottom, 10)
        .background(Color("secondBG"))
        //.background(LinearGradient(gradient: Gradient(colors: [Color(#colorLiteral(red: 0.999315083, green: 0.3765257597, blue: 0.009756078944, alpha: 1)), Color(#colorLiteral(red: 0.9946888089, green: 0.1914457083, blue: 0.003981977236, alpha: 1))]), startPoint: .top, endPoint: .bottom))
    }
}
