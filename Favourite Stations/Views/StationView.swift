//
//  StationView.swift
//  Favourite StationsExtension
//
//  Created by Александр Краснощеков on 21.10.2021.
//  Copyright © 2021 Александр Краснощеков. All rights reserved.
//

import SwiftUI

struct StationView: View {
    
    var station: StationData
    
    var body: some View {
        Link(destination: URL(string: "radiovanya://station/?streamId=\(station.id)")!, label: {
            NetworkImage(url: URL(string: station.icon_gray))//\(entry.stations[index].id)
            Spacer()
                .frame(width: 10)
            VStack(alignment: .leading, spacing: 0) {
                
                if station.title != "" {
                    HStack {
                        Text(station.title)
                            .foregroundColor(Color("titleLabel"))
                            .lineLimit(1)
                            .font(
                                .system(size: 13)
                                    .bold()
                            )
                        Spacer()
                    }
                    Spacer()
                        .frame(height: 4)
                }
                
                if station.track.artist != "" {
                    Text(station.track.artist)
                        .foregroundColor(Color("artistLabel"))
                        .lineLimit(1)
                        .font(
                            .system(size: 12)
                                //.bold()
                        )
                }
                
                
                if station.track.song != "" {
                    Text(station.track.song)
                        .foregroundColor(Color("songLabel"))
                        .lineLimit(1)
                        .font(
                            .system(size: 11)
                                //.bold()
                        )
                }
                
                
            }//.padding(.trailing, 8)

        })
    }
}
