//
//  Model.swift
//  Favourite StationsExtension
//
//  Created by Александр Краснощеков on 21.10.2021.
//  Copyright © 2021 Александр Краснощеков. All rights reserved.
//

import Foundation

struct StationsNow: Decodable {
    let result: [Result]
}

struct Result: Decodable {
    let id: Int
    let track: Track
}

struct Track: Decodable {
    let artist: String
    let song: String
    let image200: String
}

struct Stations: Decodable {
    let result: ResultStations
}

struct ResultStations: Decodable {
    let genre: [Genre]
    let stations: [Station]
}

struct Genre: Decodable {
    let id: Int
    let name: String
}

struct Station: Decodable {
    let id: Int
    let title: String
    let tooltip: String
    let icon_gray: String
    let icon_fill_white: String
}

struct StationData: Decodable {
    let id: Int
    let title: String
    let tooltip: String
    let icon_gray: String
    let icon_fill_white: String
    let track: Track
}
