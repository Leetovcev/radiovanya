//
//  NetworkManager.swift
//  Favourite StationsExtension
//
//  Created by Александр Краснощеков on 06.05.2021.
//  Copyright © 2021 Александр Краснощеков. All rights reserved.
//

import SwiftUI

class NetworkManager: ObservableObject {
    
    @Published var stationsnow = [Result]()
    
    func fetchData(stations: [Station], completion: @escaping ([StationData]) -> Void) {
        if let url = URL(string: "https://app-api.radiorecord.ru/api/stations/now/") {
            let session = URLSession(configuration: .default)
            let task = session.dataTask(with: url) { (gettingInfo, response, error) in
                if error == nil {
                    let decoder = JSONDecoder()
                    if let safeData = gettingInfo {
                        do {
                            let results = try decoder.decode(StationsNow.self, from: safeData)
                            var arrayStation: [Int] = []
                            for st in stations {
                                arrayStation.append(st.id)
                            }
                            let resultFilterTrack = results.result.filter { arrayStation.contains($0.id) }
                            
                            var stationDataArray: [StationData] = []
                            for station in stations {
                                stationDataArray.append(StationData.init(id: station.id, title: station.title, tooltip: station.tooltip, icon_gray: station.icon_gray, icon_fill_white: station.icon_fill_white, track: resultFilterTrack.filter{$0.id == station.id}.first?.track ?? Track.init(artist: "", song: "", image200: "")))
                            }

                            completion(stationDataArray)
                            
                        } catch {
                            print(error)
                        }
                    }
                }
            }
            task.resume()
        }
    }
    
    @Published var stations = [Station]()
    
    func fetchDataStations(completion: @escaping ([Station]) -> Void) {
        if let url = URL(string: "https://app-api.radiorecord.ru/api/stations/") {
            let session = URLSession(configuration: .default)
            let task = session.dataTask(with: url) { (gettingInfo, response, error) in
                if error == nil {
                    let decoder = JSONDecoder()
                    if let safeData = gettingInfo {
                        do {
                            let results = try decoder.decode(Stations.self, from: safeData)
                            
//                            if let userDefaults = UserDefaults(suiteName: "group.com.cronos.recordplayer") {
//                                if let favoriteStations = userDefaults.object(forKey: "favoriteStationsOrderArray") as? [Int] {
                                    let arrayStation: [Int] = [42715, 15016, 42891]//, 42650, 494, 528, 42509

                                    completion(results.result.stations.filter { arrayStation.contains($0.id) })
//                                }
//                            }
                            
                                                        
                        } catch {
                            print(error)
                        }
                    }
                }
            }
            task.resume()
        }
    }
}

struct NetworkImage: View {
    
    public let url: URL?
    
    var body: some View {
        
        Group {
            if let url = url, let imageData = try? Data(contentsOf: url),
               let uiImage = UIImage(data: imageData) {
                
                Image(uiImage: uiImage)
                    .resizable()
                    .aspectRatio(contentMode: .fit)
            }
            else {
                Image("audioPlaceholderBig")
                    .resizable()
                    .aspectRatio(contentMode: .fit)
            }
        }
        .background(Color("iconBG"))
        .cornerRadius(8.0)
    }
    
}

struct NetworkIcon: View {
    
    public let url: URL?
    
    var body: some View {
        
        Group {
            if let url = url, let imageData = try? Data(contentsOf: url),
               let uiImage = UIImage(data: imageData) {
                
                Image(uiImage: uiImage)
                    .renderingMode(.template)
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(width: 34.0)
                    .foregroundColor(Color("iconBG"))
            }
            else {
                Image("record_image600_white_fill")
                    .renderingMode(.template)
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(width: 34.0)
                    .foregroundColor(Color("iconBG"))
            }
        }
    }
}
