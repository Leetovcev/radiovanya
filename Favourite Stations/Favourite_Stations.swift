//
//  Favourite_Stations.swift
//  Favourite Stations
//
//  Created by Александр Краснощеков on 05.05.2021.
//  Copyright © 2021 Александр Краснощеков. All rights reserved.
//

import WidgetKit
import SwiftUI
import URLImage

struct Provider: TimelineProvider {
    
    var stationsArray: [StationData] = [
        StationData.init(id: 0, title: "Record", tooltip: "Лёгкий вайб для отдыха и хорошего настроения", icon_gray: "", icon_fill_white: "", track: Track.init(artist: "Dance Radio", song: "", image200: "")),
        StationData.init(id: 0, title: "Record", tooltip: "", icon_gray: "", icon_fill_white: "", track: Track.init(artist: "Dance Radio", song: "", image200: "")),
        StationData.init(id: 0, title: "Record", tooltip: "", icon_gray: "", icon_fill_white: "", track: Track.init(artist: "Dance Radio", song: "", image200: "")),
        StationData.init(id: 0, title: "Record", tooltip: "", icon_gray: "", icon_fill_white: "", track: Track.init(artist: "Dance Radio", song: "", image200: ""))
    ]
    
    func placeholder(in context: Context) -> StationEntry {
        StationEntry(date: Date(), stations: self.stationsArray)
    }

    func getSnapshot(in context: Context, completion: @escaping (StationEntry) -> ()) {
        completion(StationEntry(date: Date(), stations: self.stationsArray))
    }

    func getTimeline(in context: Context, completion: @escaping (Timeline<Entry>) -> Void) {
        let networkManager = NetworkManager()

        var entries: [StationEntry] = []
        
        networkManager.fetchDataStations { stations in
            
            networkManager.fetchData(stations: stations, completion: { result in
                
                let entry = StationEntry(date: Date(), stations: result)
                entries.append(entry)

                let timeline = Timeline(entries: entries, policy: .atEnd)
                completion(timeline)
            })
        }
    }
}

struct StationEntry: TimelineEntry {
    let date: Date
    let stations: [StationData]
}

struct Favourite_StationsEntryView : View {
    @Environment(\.widgetFamily) var family
    
    var entry: Provider.Entry
    
    var body: some View {
        
        switch family {
        case .systemSmall:
            SystemSmallView(entry: entry)
        default:
            SystemMediumView(entry: entry)
        }
    }
}

@main
struct Favourite_Stations: Widget {
    let kind: String = "Favourite_Stations"

    var body: some WidgetConfiguration {
        FavouriteStations(kind)
    }
}

fileprivate func FavouriteStations(_ kind:  String) -> some WidgetConfiguration {
    return StaticConfiguration(kind: kind, provider: Provider()) { entry in
        Favourite_StationsEntryView(entry: entry)
            .frame(maxWidth: .infinity, maxHeight: .infinity)
            //.background(Color("WidgetBackground"))
    }
    .configurationDisplayName("Избранные станции")
    .description("Запускай радио в одно касание")//любимую станцию
    .supportedFamilies([.systemSmall, .systemMedium])//, .systemLarge
}

struct Favourite_Stations_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            Favourite_StationsEntryView(entry: StationEntry(date: Date(), stations: [
                StationData.init(id: 0, title: "Radio", tooltip: "", icon_gray: "", icon_fill_white: "", track: Track.init(artist: "Record", song: "Dance Radio", image200: ""))
            ]))
                .previewContext(WidgetPreviewContext(family: .systemSmall))
            Favourite_StationsEntryView(entry: StationEntry(date: Date(), stations: [
                StationData.init(id: 0, title: "", tooltip: "", icon_gray: "", icon_fill_white: "", track: Track.init(artist: "", song: "", image200: ""))
            ]))
            .previewContext(WidgetPreviewContext(family: .systemMedium))
//            Favourite_StationsEntryView(entry: StationEntry(date: Date(), stations: [
//                StationData.init(id: 0, title: "", tooltip: "", icon_gray: "", icon_fill_white: "", track: Track.init(artist: "", song: "", image200: ""))
//            ]))
//                .previewContext(WidgetPreviewContext(family: .systemLarge))
        }.background(Color(.gray))
    }
}



