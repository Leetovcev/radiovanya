//
//  SystemSmallView.swift
//  Favourite StationsExtension
//
//  Created by Александр Краснощеков on 21.10.2021.
//  Copyright © 2021 Александр Краснощеков. All rights reserved.
//

import SwiftUI

struct SystemSmallView: View {
    
    var entry: StationEntry
    
    var body: some View {
        ZStack(alignment: .top) {
            Color("wrapperBG")
                .ignoresSafeArea()
            VStack(alignment: .leading, spacing: 8) {
                
                ForEach(0..<entry.stations.count) { index in
                    if index == 0 {
                        HStack {
//                            if entry.stations[0].track.image200 != "" {
//                                NetworkImage(url: URL(string: entry.stations[0].track.image200))
//                                    .scaledToFit()
//                            }else{
                                NetworkImage(url: URL(string: entry.stations[0].icon_gray))
                                    .scaledToFit()
//                            }
                            Spacer()
                            VStack{
                                Image("record_image600_white_fill")
                                    .renderingMode(.template)
                                    .resizable()
                                    .aspectRatio(contentMode: .fit)
                                    .frame(width: 40.0)
                                    .foregroundColor(Color("recordIconColor"))
                                //NetworkIcon(url: URL(string: entry.stations[0].icon_fill_white))
                                Spacer()
                            }
                            .padding(.top, -10)
                            .padding(.leading, -4)
                            .padding(.trailing, 8)
                            .widgetURL(URL(string: "radiovanya://station/?streamId=\(index)"))
                        }
                        VStack(alignment: .leading, spacing: 0) {
                            if entry.stations[0].title != "" {
                                Text(entry.stations[0].title)
                                    .foregroundColor(Color("titleLabel"))
                                    .lineLimit(1)
                                    .font(
                                        .system(size: 14)
                                            .bold()
                                    )
                            }
                            
                            Spacer()
                                .frame(height: 4)
                            
                            if entry.stations[0].track.artist != "" {
                                Text(entry.stations[0].track.artist)
                                    .foregroundColor(Color("artistLabel"))
                                    .lineLimit(1)
                                    .font(
                                        .system(size: 11)
                                            //.bold()
                                    )
                            }
//                            else{
//                                Text(entry.stations[0].title).foregroundColor(.white).lineLimit(1).font(.system(size: 14).bold())
//                            }
                            if entry.stations[0].track.song != "" {
                                Text(entry.stations[0].track.song)
                                    .foregroundColor(Color("songLabel"))
                                    .lineLimit(1)
                                    .font(
                                        .system(size: 11)
                                            //.bold()
                                    )
                            }else{
                                Text(entry.stations[0].tooltip)
                                    .foregroundColor(Color("songLabel"))
                                    .lineLimit(1)
                                    .font(
                                        .system(size: 11)
                                            //.bold()
                                    )
                            }
                            
                        }.padding(.trailing, 14)
                    }
                }
            }
            .padding(.top, 14)
            .padding(.leading, 14)
            .padding(.bottom, 12)
        }
    }
}
