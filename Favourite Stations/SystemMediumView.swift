//
//  SystemMediumView.swift
//  Favourite StationsExtension
//
//  Created by Александр Краснощеков on 21.10.2021.
//  Copyright © 2021 Александр Краснощеков. All rights reserved.
//

import SwiftUI

struct SystemMediumView: View {
    
    var entry: StationEntry
    
    var body: some View {
        ZStack {
            Color("wrapperBG")
                .ignoresSafeArea()
            VStack(alignment: .leading, spacing: 0) {
                TopView(stationsArray: entry.stations)
                BottomView(stationsArray: entry.stations)
            }
        }
    }
}
