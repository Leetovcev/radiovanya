//
//  Request.swift
//  RadioVanya
//
//  Created by Александр Краснощеков on 22.01.2021.
//  Copyright © 2021 Александр Краснощеков. All rights reserved.
//

import Foundation
import RealmSwift

let BaseURL = "http://www.radiorecord63.ru/vanya/"
let APIPostfix = "api/"
let StationsMethod = "stations/"
let questionmark = "?"
let VideosMethod = "videos/"

class Request {

    
    static let timestampString = NSDate().timeIntervalSince1970.string
    
    static var stationArray: Results<RadioStation> {
        get{
            return try! Realm().objects(RadioStation.self)//.sorted(byKeyPath: "streamId", ascending: true)
        }
    }
    
    static var videosArray: Results<Videos> {
        get{
            return try! Realm().objects(Videos.self)//.sorted(byKeyPath: "streamId", ascending: true)
        }
    }
    
    static func jsonStationLoad(){
        print(Realm.Configuration.defaultConfiguration.fileURL)
        let urlString = BaseURL+APIPostfix+StationsMethod+questionmark+timestampString
        guard let url = URL(string: urlString) else { return }
        //genericURLSession(url: url, modelType: RadioStations.self)
        genericURLSession(url: url, modelType: RadioStations.self, modelTypeArray: nil, completion: nil)
    }
    
    static func jsonVideoLoad(completion: @escaping ([Videos]) -> ()){
//        print(Realm.Configuration.defaultConfiguration.fileURL)
        let urlString = BaseURL+APIPostfix+VideosMethod+questionmark+timestampString
        guard let url = URL(string: urlString) else { return }
//        genericURLSession(url: url, modelType: Videos.self) { (Videos) in
//            completion(Videos)
//        }
//        genericURLSession(url: url, modelType: Videos.self) { (Video) in
//            completion(Video)
//        }
        genericURLSession(url: url, modelType: nil, modelTypeArray: [Videos].self, completion: { (bool) in
            completion(Request.videosArray.map { $0 })
        })
    }
    
    static func genericURLSession<T:Object>(url: URL, modelType: T.Type?, modelTypeArray: [T].Type?, completion: ((Bool)->())?) where T:Decodable {
        URLSession.shared.dataTask(with: url) { (data, _, err) in
            DispatchQueue.main.async {
                if let err = err {
                    print("Failed to get data from url:", err)
                    completion?(false)
                    return
                }
                
                guard let data = data else { return }
                                
                do {
                    if let modelType = modelType {
                        let result = try JSONDecoder().decode(modelType, from: data)
                        let realm = try! Realm()
                        try realm.write {
                            realm.add(result, update: .all)
                        }
                        completion?(true)
                    }else {
                        if let modelTypeArray = modelTypeArray {
                            let result = try JSONDecoder().decode(modelTypeArray, from: data)
                            let realm = try! Realm()
                            try realm.write {
                                realm.add(result, update: .all)
                            }
                            completion?(true)
                        }
                    }
                    
                } catch let jsonErr {
                    print("Failed to decode:", jsonErr)
                    completion?(false)
                }
            }
        }.resume()
    }
    
    static func openUrl(urlString: String){
        if let url = URL(string: urlString) {
            UIApplication.shared.open(url, options: [:]) { (_ ) in }
        }
    }
}


