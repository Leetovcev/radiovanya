//
//  VideosCell.swift
//  RadioVanya
//
//  Created by Александр Краснощеков on 13.10.2020.
//  Copyright © 2020 Александр Краснощеков. All rights reserved.
//

import UIKit
import Foundation

class VideosCell: BaseVideosCell {
    
    var isAnimated: Bool = false
    
    var videoItem: Videos? {
        didSet{
            if let video = videoItem {
                if let stringImg = video.preview_img, let imgUrl = URL(string: stringImg) {
                    self.previewImgView.sd_setImage(with: imgUrl, completed: nil)
                }else{
                    self.previewImgView.image = #imageLiteral(resourceName: "600x600bb")
                }
                
                self.nameLabel.text = video.name
            }
        }
    }
    
    let nameLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 2
        label.textColor = .white
        label.font = UIFont.systemFont(ofSize: 18, weight: .regular)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let shimmer: UIView = {
        let view = UIView()
        view.backgroundColor = .black
        view.layer.cornerRadius = 12
        view.layer.masksToBounds = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let playImageView: UIImageView = {
        let iv = UIImageView()
        let config = UIImage.SymbolConfiguration(pointSize: 34, weight: .thin, scale: .medium)
        iv.image = UIImage(systemName: "play.circle", withConfiguration: config)//"play.rectangle"
        iv.tintColor = .white
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    let previewImgView: UIImageView = {
        let iv = UIImageView()
        iv.layer.cornerRadius = 12
        iv.layer.masksToBounds = true
        iv.contentMode = .scaleAspectFill
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupViews()
    }
    
    func setupViews(){
        contentView.addSubview(previewImgView)
        contentView.addSubview(shimmer)
        contentView.addSubview(nameLabel)
        contentView.addSubview(playImageView)
        
        previewImgView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        previewImgView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        previewImgView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        previewImgView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        
        nameLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16).isActive = true
        nameLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16).isActive = true
        nameLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -16).isActive = true
        
        playImageView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        playImageView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
