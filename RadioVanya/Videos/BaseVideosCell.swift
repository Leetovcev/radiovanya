//
//  BaseVideosCell.swift
//  RadioVanya
//
//  Created by Александр Краснощеков on 13.10.2020.
//  Copyright © 2020 Александр Краснощеков. All rights reserved.
//

import UIKit

class BaseVideosCell: UICollectionViewCell {
    
    
    override var isHighlighted: Bool {
        didSet{
            var transform: CGAffineTransform = .identity
            var alphaBGV: CGFloat = 1
            if isHighlighted {
                transform = .init(scaleX: 0.95, y: 0.95)
                alphaBGV = 0
            }
            
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.transform = transform
                self.backgroundView?.alpha = alphaBGV
            })
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        //CELL SHADOW
        //self.backgroundColor = .white
        self.backgroundView = UIView()
        self.backgroundView?.backgroundColor = .secondarySystemBackground
        //addSubview(self.backgroundView!)
        insertSubview(self.backgroundView!, belowSubview: contentView)
        self.backgroundView?.translatesAutoresizingMaskIntoConstraints = false
        self.backgroundView?.topAnchor.constraint(equalTo: topAnchor).isActive = true
        self.backgroundView?.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        self.backgroundView?.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        self.backgroundView?.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        
        //CELL SHADOW
        self.backgroundView?.layer.shadowOffset = CGSize(width: -3.0, height: 3.0)
        self.backgroundView?.layer.shadowRadius = 6.0//6.0
        self.backgroundView?.layer.shadowOpacity = 0.7
        self.backgroundView?.layer.masksToBounds = false
        self.backgroundView?.layer.shadowPath = UIBezierPath(roundedRect: self.backgroundView?.bounds ?? .zero, cornerRadius: 12).cgPath
        self.backgroundView?.layer.backgroundColor = UIColor.clear.cgColor
        self.backgroundView?.layer.shouldRasterize = true
        self.backgroundView?.layer.rasterizationScale = UIScreen.main.scale
        //CELL SHADOW
    }
    //???
    override func layoutSubviews() {
        super.layoutSubviews()
        self.backgroundView?.layer.shadowColor = UIColor(named: "shadow")?.cgColor//systemGray6
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
