//
//  VideosViewController.swift
//  RadioVanya
//
//  Created by Александр Краснощеков on 13.10.2020.
//  Copyright © 2020 Александр Краснощеков. All rights reserved.
//

import UIKit
import Foundation
import SDWebImage

extension VideosViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return videoArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! VideosCell
        cell.backgroundColor = .clear
        
        let video = videoArray[indexPath.item]
        cell.videoItem = video
        
        /*
        cell.shimmer.frame = CGRect(x:0, y: 0, width: cell.frame.width, height: cell.frame.height)
        
        let gradientLayer = CAGradientLayer()
        // if you are following along, make sure to use cgColor
        gradientLayer.colors = [UIColor.clear.cgColor, UIColor.black.cgColor]
        gradientLayer.locations = [0, 1.0]//[0.2, 1.2]//0.45
        gradientLayer.frame = cell.shimmer.frame
        
        //let angle = 180 * CGFloat.pi / 180
        //gradientLayer.transform = CATransform3DMakeRotation(angle, 0, 0, 1)
        
        cell.shimmer.layer.mask = gradientLayer
        //cell.shimmer.alpha = 0.4
        cell.shimmer.alpha = 0.75
        */
        
        //Appearance Animated cell
        if !cell.isAnimated {

            UIView.animate(withDuration: 0.5, delay: 0.5 * Double(indexPath.row), usingSpringWithDamping: 1, initialSpringVelocity: 0.5, options: .transitionFlipFromTop, animations: {
                AnimationUtility.viewSlideInFromBottom(toTop: cell)
            }, completion: { (done) in
                cell.isAnimated = true
            })
        }
        //Appearance Animated cell

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width: CGFloat = view.frame.width - 32
        let height: CGFloat = width * 0.5625//0.75
        return CGSize(width: width, height: height)//0,75
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 16
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let cell = collectionView.cellForItem(at: indexPath) as? VideosCell {
            animationSelectItem(cell)
        }
    }
}

class VideosViewController: UIViewController {
    
    private let cellId = "cellId"
    
    let collectionView: UICollectionView = {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 16, bottom: 16, right: 16)
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.backgroundColor = .systemBackground//.white
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        return collectionView
    }()
    
    private let spinnerIndicatorView: UIActivityIndicatorView = {
        let iv = UIActivityIndicatorView()
        iv.style = .large
        iv.tintColor = .lightGray
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    var videoArray = [Videos]() {
        didSet{
            collectionView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNavBar()
        
        setupCollectionView()
        
        setupLoadinView()
        
//        jsonVideoLoad()
        Request.jsonVideoLoad(completion: { Videos in
            self.videoArray = Videos
        })
    }
    
    private func setupLoadinView() {
        view.addSubview(spinnerIndicatorView)
        spinnerIndicatorView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        spinnerIndicatorView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    
    func setupNavBar() {
//        let titleLabel = UILabel()
//        titleLabel.text = "НОВИНКИ НА РАДИО ВАНЯ"
//        titleLabel.font = UIFont.init(name: "BebasNeueBold", size: 24)
//        self.navigationItem.titleView = titleLabel
        self.navigationItem.title = "НОВИНКИ НА РАДИО ВАНЯ"
        self.navigationController?.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.label, NSAttributedString.Key.font: UIFont(name: "BebasNeueBold", size: 30) ?? UIFont.systemFont(ofSize: 30)]
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.label, NSAttributedString.Key.font: UIFont(name: "BebasNeueBold", size: 24) ?? UIFont.systemFont(ofSize: 18)]
    }

    private func setupCollectionView(){
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
        view.addSubview(collectionView)
        collectionView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        collectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        collectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
        collectionView.register(VideosCell.self, forCellWithReuseIdentifier: cellId)
    }
    
//    private func jsonVideoLoad(){
//        startLoadinView()
//        let timestamp = NSDate().timeIntervalSince1970
//        let urlString = "https://dev.radiovanya.ru/api/v1/videos/?\(timestamp)"
//        guard let url = URL(string: urlString) else { return }
//        URLSession.shared.dataTask(with: url) { (data, _, err) in
//            DispatchQueue.main.async {
//                if let err = err {
//                    print("Failed to get data from url:", err)
//                    self.notLoadTryAgain()
//                    return
//                }
//
//                guard let data = data else { return } //self.notLoadTryAgain();
//
//                do {
//                    let videos = try JSONDecoder().decode([Videos].self, from: data)
//                    DispatchQueue.main.async() {
//                        self.videoArray = videos
//                        self.loadingGood()
//                        self.collectionView.reloadData()
//                    }
//                } catch let jsonErr {
//                    print("Failed to decode:", jsonErr)
//                    self.notLoadTryAgain()
//                }
//            }
//        }.resume()
//    }
    
    private func startLoadinView() {
        spinnerIndicatorView.startAnimating()
    }
    
    private func notLoadTryAgain() {
        spinnerIndicatorView.stopAnimating()
    }
    
    private func loadingGood() {
        spinnerIndicatorView.stopAnimating()
    }
    
    private func animationSelectItem(_ cell: VideosCell) {
        let videoWebViewController = WebViewController()
        let viewController = UINavigationController(rootViewController: videoWebViewController)
        
        videoWebViewController.video = cell.videoItem
        self.navigationController?.present(viewController, animated: true, completion: {
            
        })
        
        UIView.animate(withDuration: 0.15, delay: 0, options: .curveEaseOut, animations: {
            cell.transform = CGAffineTransform(scaleX: 0.95, y: 0.95)
            cell.backgroundView?.alpha = 0
        }, completion: { (_ ) in
            UIView.animate(withDuration: 0.15, delay: 0, options: .curveEaseInOut, animations: {
                cell.transform = .identity
                cell.backgroundView?.alpha = 1
            })
        })
    }
}
