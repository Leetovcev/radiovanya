//
//  RadioStationCell.swift
//  RadioVanya
//
//  Created by Александр Краснощеков on 07.10.2020.
//  Copyright © 2020 Александр Краснощеков. All rights reserved.
//

import UIKit
import Foundation

class RadioStationCell: BaseStationCell {
    
    var radioItem: RadioStation? {
        didSet{
            
        }
    }
    
    var isAnimated: Bool = false
    
    var playPauseCellAction : (() -> ())?
    
    let backGroundShadowView: UIView = {
        let view = UIView()
        view.backgroundColor = .secondarySystemBackground//.secondarySystemBackground//.white
        view.layer.masksToBounds = false
        view.layer.cornerRadius = 8
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let wrapperStackView: UIStackView = {
        let sv = UIStackView()
        sv.contentMode = .center
        sv.layoutMargins = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 6)
        sv.isLayoutMarginsRelativeArrangement = true
        sv.translatesAutoresizingMaskIntoConstraints = false
        return sv
    }()
    
    let imageView: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFill
        iv.layer.masksToBounds = true
        iv.layer.cornerRadius = 8
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    
    let maskImageView: UIImageView = {
        let iv = UIImageView()
        iv.image = #imageLiteral(resourceName: "mask_radio_icon").withRenderingMode(.alwaysTemplate)
        iv.tintColor = .secondarySystemBackground
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    let title: UILabel = {
        let label = UILabel()
        label.lineBreakMode = .byTruncatingMiddle
        label.text = "Радио Ваня"
        return label
    }()
    
    
    let playButton: UIButton = {
        let button = UIButton()
        //button.setImage(#imageLiteral(resourceName: "play_button"), for: .normal)
        let config = UIImage.SymbolConfiguration(pointSize: 24, weight: .bold, scale: .small)
        button.setImage(UIImage(systemName: "play.fill", withConfiguration: config)?.withRenderingMode(.alwaysTemplate), for: .normal)
        button.tintColor = .label//.black
        button.imageView?.contentMode = .scaleAspectFit
        button.tag = 2
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    let loadingActivityIndicatorView: UIActivityIndicatorView = {
        let ai = UIActivityIndicatorView()
        ai.color = .secondaryLabel//.gray
        ai.style = .medium
        ai.translatesAutoresizingMaskIntoConstraints = false
        return ai
    }()
    
    var imageViewHeightAnchor: NSLayoutConstraint?
    
    let bottomMarginImage = UIView()
    let topMarginPlayButton = UIView()
    
    var loadingAICenterXAnchor: NSLayoutConstraint?
    var loadingAICenterYAnchor: NSLayoutConstraint?
    
    let darkView: UIView = {
        let view = UIView()
        view.backgroundColor = .init(white: 0, alpha: 0.5)
        view.alpha = 0
        view.layer.cornerRadius = 8
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupViews()
    }
    
    func setupViews(){

        contentView.addSubview(backGroundShadowView)
        backGroundShadowView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        backGroundShadowView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        backGroundShadowView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        backGroundShadowView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        
        backGroundShadowView.addSubview(wrapperStackView)
        
        wrapperStackView.topAnchor.constraint(equalTo: backGroundShadowView.topAnchor, constant: 0).isActive = true
        wrapperStackView.leadingAnchor.constraint(equalTo: backGroundShadowView.leadingAnchor, constant: 0).isActive = true
        wrapperStackView.trailingAnchor.constraint(equalTo: backGroundShadowView.trailingAnchor, constant: 0).isActive = true
        wrapperStackView.bottomAnchor.constraint(equalTo: backGroundShadowView.bottomAnchor, constant: 0).isActive = true
        
        bottomMarginImage.translatesAutoresizingMaskIntoConstraints = false
        
        [imageView, bottomMarginImage, title, topMarginPlayButton, playButton].forEach { wrapperStackView.addArrangedSubview($0) }
        //[imageView, title, playButton].forEach { wrapperStackView.addArrangedSubview($0) }
        bottomMarginImage.widthAnchor.constraint(equalToConstant: 16).isActive = true
        
        imageViewHeightAnchor = imageView.heightAnchor.constraint(equalTo: heightAnchor)
        imageViewHeightAnchor?.isActive = true
        imageView.widthAnchor.constraint(equalTo: imageView.heightAnchor, multiplier: 2).isActive = true
        
        playButton.widthAnchor.constraint(equalToConstant: 44).isActive = true
        playButton.addTarget(self, action: #selector(playButtonHandler), for: .touchUpInside)
        
        wrapperStackView.insertSubview(loadingActivityIndicatorView, aboveSubview: playButton)
        loadingAICenterXAnchor = loadingActivityIndicatorView.centerXAnchor.constraint(equalTo: playButton.centerXAnchor)
        loadingAICenterXAnchor?.isActive = true
        loadingAICenterYAnchor = loadingActivityIndicatorView.centerYAnchor.constraint(equalTo: playButton.centerYAnchor)
        loadingAICenterYAnchor?.isActive = true
        
        imageView.addSubview(maskImageView)
        maskImageView.heightAnchor.constraint(equalTo: imageView.heightAnchor, multiplier: 1, constant: -10).isActive = true
        maskImageView.widthAnchor.constraint(equalTo: imageView.heightAnchor, multiplier: 0.42546584).isActive = true
        maskImageView.trailingAnchor.constraint(equalTo: imageView.trailingAnchor).isActive = true
        maskImageView.bottomAnchor.constraint(equalTo: imageView.bottomAnchor).isActive = true
        
        wrapperStackView.insertSubview(darkView, aboveSubview: playButton)
        darkView.topAnchor.constraint(equalTo: topAnchor, constant: 0).isActive = true
        darkView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0).isActive = true
        darkView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 0).isActive = true
        darkView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0).isActive = true
        
    }
    
    @objc func playButtonHandler(_ button: UIButton) {
        playPauseCellAction?()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override open var isSelected: Bool
    {
        set {

        }

        get {
            return super.isSelected
        }
    }
    
    override open var isHighlighted: Bool
    {
        set {
            
        }
        
        get {
            return super.isHighlighted
        }
    }
    
    

}
