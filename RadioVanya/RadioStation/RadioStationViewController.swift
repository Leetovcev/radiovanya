//
//  RadioStationViewController.swift
//  RadioVanya
//
//  Created by Александр Краснощеков on 10.10.2020.
//  Copyright © 2020 Александр Краснощеков. All rights reserved.
//

import UIKit
import SDWebImage
import LNPopupController
import StreamingKit


protocol RadioStationSelectDelegate {
    func selectRadio(station: RadioStation)
    
    func playPauseCell()
}

extension RadioStationViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        didSelectItemAtRadioStation(radio: isFiltering ? stationArraySearch[indexPath.item] : stationArray[indexPath.item], indexPath: indexPath, user: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return isFiltering ? stationArraySearch.count : stationArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! RadioStationCell
        cell.backgroundColor = .clear

        var radio = RadioStation()
        if isFiltering {
            radio = stationArraySearch[indexPath.item]
        }else{
            radio = stationArray[indexPath.item]
        }
        
//        if selecedIndexPath == indexPath {
//            cell.gifAnimationView.startAnimating()
//        }
        
        cell.radioItem = radio
        
        if let stringImg = radio.image300, let imgUrl = URL(string: stringImg) {
            cell.imageView.sd_setImage(with: imgUrl)
//            cell.imageView.sd_setImage(with: imgUrl) { (img, erro, type, utl) in
//                cell.tileImageView.image = img
//            }
        }else{
            //??
            cell.imageView.image = #imageLiteral(resourceName: "600x600bb")
        }
        
        let radioTitle = radio.title
        cell.title.text = radioTitle
//        cell.tileTitle.text = radioTitle
        
        if !cell.isAnimated {

            UIView.animate(withDuration: 0.5, delay: 0.5 * Double(indexPath.row), usingSpringWithDamping: 1, initialSpringVelocity: 0.5, options: indexPath.row % 2 == 0 ? .transitionFlipFromLeft : .transitionFlipFromRight, animations: {

                if indexPath.row % 2 == 0 {
                    AnimationUtility.viewSlideInFromLeft(toRight: cell)
                }
                else {
                    AnimationUtility.viewSlideInFromRight(toLeft: cell)
                }

            }, completion: { (done) in
                cell.isAnimated = true
            })
        }
        
        cell.playPauseCellAction = {
            self.playPauseCellButtonHandlre(radio: radio, indexPath: indexPath)
        }
        
        //??
        if searchCellSelected(radio: radio) == radio, statePlaying == .playing {//self.searchCellSelected(radio: selecedRadioItem)  selecedRadioItem == radio
//            cell.gifAnimationView.startAnimating()
            cellLoadingIndicatorStop(cell)

            let config = UIImage.SymbolConfiguration(pointSize: 24, weight: .bold, scale: .small)
            cell.playButton.setImage(UIImage(systemName: "pause.fill", withConfiguration: config)?.withRenderingMode(.alwaysTemplate), for: .normal)
            cell.playButton.tintColor = #colorLiteral(red: 0.774078846, green: 0.1601946652, blue: 0.1577996314, alpha: 1)
            
            //cell.contentView.layer.opacity = self.tileStyle ? 0.4 : 1
            cell.darkView.alpha = self.tileStyle ? 1 : 0
        }else{
//            cell.gifAnimationView.stopAnimating()
            
            let config = UIImage.SymbolConfiguration(pointSize: 24, weight: .bold, scale: .small)
            cell.playButton.setImage(UIImage(systemName: "play.fill", withConfiguration: config)?.withRenderingMode(.alwaysTemplate), for: .normal)

            cell.playButton.tintColor = .label
            
            //cell.contentView.layer.opacity = 1
            cell.darkView.alpha = 0
        }
        
//        if selecedIndexPath == indexPath, statePlaying == .buffering {
//            cellLoadingIndicatorStart(cell)
//        }

        
        //cell.isHighlighted = true
        
        
//        cell.tileStackView.alpha = tileStyle ? 1 : 0
//        cell.wrapperStackView.alpha = tileStyle ? 0 : 1
//
        if let colorHex = radio.colorHex {
            let color = UIColor(hex: colorHex)
            cell.backGroundShadowView.backgroundColor = tileStyle ? color : .secondarySystemBackground
            cell.title.textColor = self.tileStyle ? color.isLight() ? .black : .white : .label
        }
        
        if tileStyle {
            cell.loadingAICenterXAnchor?.isActive = false
            cell.loadingAICenterXAnchor = cell.loadingActivityIndicatorView.centerXAnchor.constraint(equalTo: cell.centerXAnchor)
            cell.loadingAICenterXAnchor?.isActive = true
            
            cell.loadingAICenterYAnchor?.isActive = false
            cell.loadingAICenterYAnchor = cell.loadingActivityIndicatorView.centerYAnchor.constraint(equalTo: cell.centerYAnchor)
            cell.loadingAICenterYAnchor?.isActive = true
            
            cell.loadingActivityIndicatorView.style = .large
        }else{
            cell.loadingAICenterXAnchor?.isActive = false
            cell.loadingAICenterXAnchor = cell.loadingActivityIndicatorView.centerXAnchor.constraint(equalTo: cell.playButton.centerXAnchor)
            cell.loadingAICenterXAnchor?.isActive = true
            
            cell.loadingAICenterYAnchor?.isActive = false
            cell.loadingAICenterYAnchor = cell.loadingActivityIndicatorView.centerYAnchor.constraint(equalTo: cell.playButton.centerYAnchor)
            cell.loadingAICenterYAnchor?.isActive = true
            
            cell.loadingActivityIndicatorView.style = .medium
        }
        
        cell.loadingActivityIndicatorView.color = tileStyle ? .white : .secondaryLabel
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: tileStyle ? (view.frame.width / 3) - 16 : view.frame.width - 32, height: hieghtForItemAt)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return tileStyle ? 8 : 16
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return tileStyle ? 8 : 16
    }

    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        didDeselectItemAtRadioStation(indexPath: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        if let cell = collectionView.cellForItem(at: indexPath) as? RadioStationCell {//indexPath != selecedIndexPath
            if indexPath != selecedIndexPath {
                cellLoadingIndicatorStart(cell)
                animationSelectItem(cell)
            }else{
                animationSelectItem(cell)
            }

        }
        return true
    }
    
}

class RadioStationViewController: UIViewController {

//    var stationArray = [RadioStation]()
    
    var delegate: RadioStationSelectDelegate?
    
    var selecedIndexPath: IndexPath?
    var selecedRadioItem: RadioStation?
    var selecedRadioCell: RadioStationCell?
    
    var statePlaying: STKAudioPlayerState = .stopped
    
    var tileStyle: Bool = false {
        didSet{
//            collectionView.reloadData()
//            if tileStyle { return }
//            collectionView.performBatchUpdates(nil, completion: { (result) in
//                self.testFunc()
//            })
            
            collectionView.performBatchUpdates {
                hieghtForItemAt = tileStyle ? tileHieghtForItemAt : lineHieghtForItemAt
                gifAnimationView.alpha = 0
                
                collectionView.visibleCells.forEach { visCell in
                    if let cell = visCell as? RadioStationCell {
                        
                        if self.tileStyle {
                            cell.imageViewHeightAnchor?.isActive = false
                            cell.imageViewHeightAnchor = cell.imageView.heightAnchor.constraint(equalTo: cell.widthAnchor, constant: -16)
                            cell.imageViewHeightAnchor?.isActive = true
                            
                            cell.loadingAICenterXAnchor?.isActive = false
                            cell.loadingAICenterXAnchor = cell.loadingActivityIndicatorView.centerXAnchor.constraint(equalTo: cell.centerXAnchor)
                            cell.loadingAICenterXAnchor?.isActive = true
                            
                            cell.loadingAICenterYAnchor?.isActive = false
                            cell.loadingAICenterYAnchor = cell.loadingActivityIndicatorView.centerYAnchor.constraint(equalTo: cell.centerYAnchor)
                            cell.loadingAICenterYAnchor?.isActive = true
                            
                            cell.loadingActivityIndicatorView.style = .large
                        }else{
                            cell.imageViewHeightAnchor?.isActive = false
                            cell.imageViewHeightAnchor = cell.imageView.heightAnchor.constraint(equalTo: cell.heightAnchor)
                            cell.imageViewHeightAnchor?.isActive = true
                            
                            cell.loadingAICenterXAnchor?.isActive = false
                            cell.loadingAICenterXAnchor = cell.loadingActivityIndicatorView.centerXAnchor.constraint(equalTo: cell.playButton.centerXAnchor)
                            cell.loadingAICenterXAnchor?.isActive = true
                            
                            cell.loadingAICenterYAnchor?.isActive = false
                            cell.loadingAICenterYAnchor = cell.loadingActivityIndicatorView.centerYAnchor.constraint(equalTo: cell.playButton.centerYAnchor)
                            cell.loadingAICenterYAnchor?.isActive = true
                            
                            cell.loadingActivityIndicatorView.style = .medium
                        }
                        
                        cell.loadingActivityIndicatorView.color = tileStyle ? .white : .secondaryLabel
                        
                        UIView.animate(withDuration: 0.25) {
//                            cell.tileStackView.alpha = self.tileStyle ? 1 : 0
//                            cell.wrapperStackView.alpha = self.tileStyle ? 0 : 1
                            cell.wrapperStackView.axis = self.tileStyle ? .vertical : .horizontal
                            cell.wrapperStackView.contentMode = self.tileStyle ? .scaleToFill : .center
                            
                            cell.maskImageView.alpha = self.tileStyle ? 0 : 1
                            
                            let margins: CGFloat = self.tileStyle ? 8 : 0
                            let marginsRight: CGFloat = self.tileStyle ? 8 : 6
                            cell.wrapperStackView.layoutMargins = UIEdgeInsets(top: margins, left: margins, bottom: margins, right: marginsRight)
                            
                            cell.playButton.isHidden = self.tileStyle ? true : false
                            cell.title.textAlignment = self.tileStyle ? .center : .left
                            
                            cell.bottomMarginImage.isHidden = self.tileStyle ? true : false
                            cell.topMarginPlayButton.isHidden = self.tileStyle ? true : false
                            
                            cell.title.numberOfLines = self.tileStyle ? 2 : 1
                            cell.title.font = UIFont.systemFont(ofSize: self.tileStyle ? 14 : 17)
                            cell.title.lineBreakMode = self.tileStyle ? .byTruncatingTail : .byTruncatingMiddle
                            
                            cell.layoutIfNeeded()
                            
                            if let colorHex = cell.radioItem?.colorHex {
                                let color = UIColor(hex: colorHex)
                                cell.backGroundShadowView.backgroundColor = self.tileStyle ? color : .secondarySystemBackground
                                cell.title.textColor = self.tileStyle ? color.isLight() ? .black : .white : .label
                            }
                        }
                    }
                }
                
            } completion: { (_ ) in
                //if self.tileStyle { return }
                UIView.animate(withDuration: 0.25) {
                    self.gifAnimationView.alpha = 1
                }
                self.testFunc()
            }

        }
    }
    
    func startStationOnLink() {
        if let stationId = self.stationOnLink {
            for (index, station) in self.stationArray.enumerated() {
                if station.streamId == stationId {
                    self.didSelectItemAtRadioStation(radio: self.stationArray[index], indexPath: .init(item: index, section: 0))
                }
            }
            self.stationOnLink = nil
        }
    }
    
    var stationOnLink: Int? {
        didSet{
            if !stationArray.isEmpty {
                self.startStationOnLink()
            }
        }
    }
    
    var stationArray = [RadioStation]() {
        didSet {
            collectionView.reloadData()
            collectionView.performBatchUpdates(nil, completion: { (result) in
                self.startStationOnLink()
            })
        }
    }
    
    var stationArraySearch = [RadioStation]() {
        didSet {
            collectionView.reloadData()
            //if tileStyle { return }
            collectionView.performBatchUpdates(nil, completion: { (result) in
                
                self.testFunc(false)

            })
        }
    }
    
    lazy var searchController: UISearchController = {
        let search = UISearchController(searchResultsController: nil)
        //search.view.backgroundColor = .systemBackground
        search.delegate = self
        search.searchResultsUpdater = self
        search.obscuresBackgroundDuringPresentation = false
        search.searchBar.placeholder = "Поиск по радиоканалам"
        search.searchBar.tintColor = UIColor.tintSelectColor
//        search.hidesNavigationBarDuringPresentation = false
        search.obscuresBackgroundDuringPresentation = false

        return search
    }()
    
    var isSearchBarEmpty: Bool {
        return searchController.searchBar.text?.isEmpty ?? true
    }
    
    var isFiltering: Bool {
        return searchController.isActive && !isSearchBarEmpty
    }
    
    //try! Realm().objects(RadioStationRealm.self).sorted(byKeyPath: "streamId", ascending: true)
//    print(Realm.Configuration.defaultConfiguration.fileURL)
    
    let collectionView: UICollectionView = {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 16, left: 16, bottom: 16, right: 16)
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.backgroundColor = .clear//.systemBackground//.white
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        return collectionView
    }()
    
    var cellId = "Cell"
    var hieghtForItemAt: CGFloat = 61
    let lineHieghtForItemAt: CGFloat = 61
    lazy var tileHieghtForItemAt: CGFloat = ((view.frame.width / 3) - 16) * 1.38
    let widthPlayedIndicator: CGFloat = 4
    
    let playedIndicatorView: IndicatorView = {//IndicatorView
        let view = IndicatorView()
        view.backgroundColor = .tintSelectColor
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var playedIndicatorViewTopAnchor: NSLayoutConstraint?
    var playedIndicatorViewWidthAnchor: NSLayoutConstraint?
    
    let gifAnimationView: UIImageView = {
        let iv = UIImageView.fromGif(frame: .zero, resourceName: "play_animation")
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    var gifAnimationLeadingAnchor: NSLayoutConstraint?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCollectionView()
        // Do any additional setup after loading the view.
        view.backgroundColor = .systemBackground
        setupNavBar()
        
        setupIndicatorView()
        
        definesPresentationContext = true
        


//        jsonStationLoad()
//        print(Realm.Configuration.defaultConfiguration.fileURL)
        Request.jsonStationLoad()

    }
 
    var firsLoad: Bool = true
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if firsLoad {
            self.navigationController?.setNavigationBarHidden(true, animated: false)
            firsLoad = false
        }
        
    }
    
    var firsLoadView: Int = 0
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if firsLoadView < 2 {
            self.navigationController?.navigationBar.prefersLargeTitles = true
            self.collectionView.contentOffset.y = -68
            firsLoadView += 1
        }
        
    }

    private func setupNavBar(){
//        let titleLabel = UILabel()
//        titleLabel.text = "ОНЛАЙН РАДИО"
//        titleLabel.font = UIFont.init(name: "BebasNeueBold", size: 24)
//        self.navigationItem.titleView = titleLabel
        self.navigationItem.title = "Онлайн радио"
        self.navigationController?.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.label, NSAttributedString.Key.font: UIFont(name: "BebasNeueBold", size: 30) ?? UIFont.systemFont(ofSize: 30)]
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.label, NSAttributedString.Key.font: UIFont(name: "BebasNeueBold", size: 24) ?? UIFont.systemFont(ofSize: 18)]
        
        self.navigationItem.hidesSearchBarWhenScrolling = true
        
//        let changeStyleButton = UIBarButtonItem(image: UIImage(systemName: "rectangle.grid.2x2"), style: .done, target: self, action: #selector(presentSeachBar))
//        let searchButton = UIBarButtonItem(image: UIImage(systemName: "magnifyingglass"), style: .done, target: self, action: #selector(presentSeachBar))
//        self.navigationItem.rightBarButtonItems = [searchButton, changeStyleButton]
//        self.navigationItem.rightBarButtonItems?.forEach { $0.tintColor = UIColor.tintSelectColor }
        addRightBarButtonItems()
    }
    
    let configNavigatioBarButton = UIImage.SymbolConfiguration(pointSize: 24, weight: .regular, scale: .unspecified)
    
    lazy var btnChangeStyleCell: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(systemName: "rectangle.grid.2x2", withConfiguration: configNavigatioBarButton)?.withRenderingMode(.alwaysTemplate), for: .normal)//rectangle.grid.1x2
        button.imageView?.tintColor = .tintSelectColor
        button.addTarget(self, action: #selector(toggleStyleCell), for: .touchUpInside)
        return button
    }()
    
    func addRightBarButtonItems(){
        
        let btnSearch = UIButton.init(type: .custom)
        btnSearch.setImage(UIImage(systemName: "magnifyingglass", withConfiguration: configNavigatioBarButton)?.withRenderingMode(.alwaysTemplate), for: .normal)
        btnSearch.imageView?.tintColor = .tintSelectColor
        btnSearch.addTarget(self, action: #selector(presentSeachBar), for: .touchUpInside)

        let stackview = UIStackView.init(arrangedSubviews: [btnChangeStyleCell, btnSearch])
        stackview.distribution = .equalSpacing
        stackview.axis = .horizontal
        stackview.alignment = .center
        stackview.spacing = 12

        let rightBarButton = UIBarButtonItem(customView: stackview)
        self.navigationItem.rightBarButtonItem = rightBarButton
    }

    @objc private func toggleStyleCell() {
        tileStyle = !tileStyle
        if tileStyle {
            btnChangeStyleCell.setImage(UIImage(systemName: "rectangle.grid.1x2", withConfiguration: configNavigatioBarButton)?.withRenderingMode(.alwaysTemplate), for: .normal)
            self.playedIndicatorView.isHidden = true
            self.gifAnimationView.isHidden = true
        }else{
            btnChangeStyleCell.setImage(UIImage(systemName: "rectangle.grid.2x2", withConfiguration: configNavigatioBarButton)?.withRenderingMode(.alwaysTemplate), for: .normal)
//            self.playedIndicatorView.isHidden = false
//            self.gifAnimationView.isHidden = false
        }
    }
    
    @objc private func presentSeachBar() {
        if self.navigationItem.searchController == nil {
            self.navigationItem.searchController = searchController
        }
        //DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
//            self.searchController.isActive = true
            self.searchController.searchBar.becomeFirstResponder()
        //}
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if !scrollView.frame.isEmpty, navigationItem.searchController == nil {
            navigationItem.searchController = searchController
        }
    }
    
    var gifAnimationViewBottomAnchor: NSLayoutConstraint?
    var gifAnimationViewHeightAnchor: NSLayoutConstraint?
    var gifAnimationViewCenterXAnchor: NSLayoutConstraint?

    private func setupIndicatorView() {
        collectionView.addSubview(playedIndicatorView)
        playedIndicatorView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        playedIndicatorViewTopAnchor = playedIndicatorView.topAnchor.constraint(equalTo: view.topAnchor, constant: -hieghtForItemAt)
        playedIndicatorViewTopAnchor?.isActive = true
        playedIndicatorView.heightAnchor.constraint(equalToConstant: hieghtForItemAt).isActive = true
        playedIndicatorViewWidthAnchor = playedIndicatorView.widthAnchor.constraint(equalToConstant: widthPlayedIndicator)
        playedIndicatorViewWidthAnchor?.isActive = true
        
        collectionView.addSubview(gifAnimationView)
        gifAnimationLeadingAnchor = gifAnimationView.leadingAnchor.constraint(equalTo: playedIndicatorView.leadingAnchor, constant: -200)
        gifAnimationLeadingAnchor?.isActive = true
        gifAnimationViewBottomAnchor = gifAnimationView.bottomAnchor.constraint(equalTo: playedIndicatorView.bottomAnchor, constant: 0)
        gifAnimationViewBottomAnchor?.isActive = true
        gifAnimationViewHeightAnchor = gifAnimationView.heightAnchor.constraint(equalTo: playedIndicatorView.heightAnchor, multiplier: 0.75)
        gifAnimationViewHeightAnchor?.isActive = true
        gifAnimationView.widthAnchor.constraint(equalTo: gifAnimationView.heightAnchor, multiplier: 1.16363636).isActive = true
        
        gifAnimationViewCenterXAnchor = gifAnimationView.centerXAnchor.constraint(equalTo: playedIndicatorView.centerXAnchor)
        gifAnimationViewCenterXAnchor?.isActive = false
    }
    
    private func setupCollectionView(){
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
        collectionView.keyboardDismissMode = .interactive
        collectionView.contentInsetAdjustmentBehavior = .automatic
        
        view.addSubview(collectionView)
        collectionView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        collectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        collectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
        collectionView.register(RadioStationCell.self, forCellWithReuseIdentifier: cellId)
    }
    
    
    func nextSelectedItemStatio(){
        if let selecedIndexPath = selecedIndexPath, !stationArray.isEmpty {
            var nextItem = selecedIndexPath.item + 1
            if nextItem > stationArray.count - 1{
                nextItem = 0
                didSelectItemAtRadioStation(radio: stationArray[nextItem], indexPath: .init(item: nextItem, section: 0))
                return
            }else{
                didSelectItemAtRadioStation(radio: stationArray[nextItem], indexPath: .init(item: nextItem, section: 0))
            }
        }
    }
    
    func backSelectedItemStatio(){
        if let selecedIndexPath = selecedIndexPath, !stationArray.isEmpty {
            var backItem = selecedIndexPath.item - 1
            if backItem >= 0 {
                didSelectItemAtRadioStation(radio: stationArray[backItem], indexPath: .init(item: backItem, section: 0))
                return
            }else{
                backItem = stationArray.count - 1
                didSelectItemAtRadioStation(radio: stationArray[backItem], indexPath: .init(item: backItem, section: 0))
            }
        }
    }
    
    func didSelectItemAtRadioStation(radio: RadioStation, indexPath: IndexPath, user: Bool = false) {
        //if let cell = collectionView.cellForItem(at: indexPath) as? RadioStationCell, indexPath != selecedIndexPath {
        if let cell = searchCellSelected(radio: radio), searchCellSelected(radio: radio)?.radioItem != selecedRadioItem {
            if !tileStyle {
                playedIndicatorView.isHidden = false
                gifAnimationView.isHidden = false
                cellLoadingIndicatorStart(cell)
                playedIndicatorViewTopAnchor?.isActive = false
                playedIndicatorViewTopAnchor = playedIndicatorView.topAnchor.constraint(equalTo: cell.topAnchor)
                playedIndicatorViewTopAnchor?.isActive = true
                
                gifAnimationViewBottomAnchor?.isActive = false
                gifAnimationViewBottomAnchor = gifAnimationView.bottomAnchor.constraint(equalTo: playedIndicatorView.bottomAnchor, constant: 0)
                gifAnimationViewBottomAnchor?.isActive = true
                
                gifAnimationViewHeightAnchor?.isActive = false
                gifAnimationViewHeightAnchor = gifAnimationView.heightAnchor.constraint(equalTo: playedIndicatorView.heightAnchor, multiplier: 0.75)
                gifAnimationViewHeightAnchor?.isActive = true
                
                UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.75, initialSpringVelocity: 0.75, options: .curveEaseIn, animations: {
                    if let colorHex = radio.colorHex{
                        self.playedIndicatorView.backgroundColor = UIColor(hex: colorHex)
                    }
                    self.playedIndicatorViewWidthAnchor?.constant = self.widthPlayedIndicator
                    self.view.layoutIfNeeded()
                })
            }else{
                gifAnimationView.isHidden = false
                
                gifAnimationViewBottomAnchor?.isActive = false
                gifAnimationViewBottomAnchor = gifAnimationView.bottomAnchor.constraint(equalTo: cell.bottomAnchor, constant: 0)
                gifAnimationViewBottomAnchor?.isActive = true
                
                gifAnimationViewHeightAnchor?.isActive = false
                gifAnimationViewHeightAnchor = gifAnimationView.heightAnchor.constraint(equalTo: cell.imageView.heightAnchor, multiplier: 0.75)
                gifAnimationViewHeightAnchor?.isActive = true
                
                //cell.contentView.layer.opacity = 0.4
                cell.darkView.alpha = 1
                gifAnimationLeadingAnchor?.isActive = false
                gifAnimationViewCenterXAnchor?.isActive = false
                
                gifAnimationViewCenterXAnchor = gifAnimationView.centerXAnchor.constraint(equalTo: cell.centerXAnchor)
                gifAnimationViewCenterXAnchor?.isActive = true
            }
        }
        
        if !user {
            if let selecedIndexPath = selecedIndexPath {
                collectionView.deselectItem(at: selecedIndexPath, animated: false)
                didDeselectItemAtRadioStation(indexPath: selecedIndexPath)
            }
            collectionView.selectItem(at: indexPath, animated: false, scrollPosition: .init())
        }
        
        delegate?.selectRadio(station: radio)
        selecedIndexPath = indexPath
        selecedRadioItem = radio
        
        
    }
    
    private func searchCellSelected(radio: RadioStation) -> RadioStationCell?{
        if let visibleCells = self.collectionView.visibleCells as? [RadioStationCell] {
            return visibleCells.filter { (cell) -> Bool in
                return cell.radioItem == radio
            }.first
        }else{
            return nil
        }
    }
    
    private func animationSelectItem(_ cell: RadioStationCell) {
        UIView.animate(withDuration: 0.15, delay: 0, options: .curveEaseOut, animations: {
            cell.transform = CGAffineTransform(scaleX: 0.95, y: 0.95)
            cell.backgroundView?.alpha = 0
            self.playedIndicatorView.transform = CGAffineTransform(scaleX: 1, y: 0.95)
        }, completion: { (_ ) in
            UIView.animate(withDuration: 0.15, delay: 0, options: .curveEaseInOut, animations: {
                cell.transform = .identity
                cell.backgroundView?.alpha = 1
                self.playedIndicatorView.transform = .identity
            })
        })
    }
    
    func didDeselectItemAtRadioStation(indexPath: IndexPath) {
        if let cell = collectionView.cellForItem(at: indexPath) as? RadioStationCell {
            
            gifAnimationView.stopAnimating()
            gifAnimationLeadingAnchor?.constant = -200
            
            let config = UIImage.SymbolConfiguration(pointSize: 24, weight: .bold, scale: .small)
            cell.playButton.setImage(UIImage(systemName: "play.fill", withConfiguration: config)?.withRenderingMode(.alwaysTemplate), for: .normal)
            
            cell.playButton.tintColor = .label
            
            cell.contentView.layer.opacity = 1
            cell.darkView.alpha = 0
            
            cellLoadingIndicatorStop(cell)
        }
    }
    
    func changeCellRadioStatusPlaying (state: STKAudioPlayerState, animation: Bool) {
        var imageName: String = "play.fill"
        var tintColor: UIColor = .label
        
        switch state {
        case .playing:
            imageName = "pause.fill"
            tintColor = .tintSelectColor
        case .paused:
            imageName = "play.fill"
            tintColor = .label
        case .stopped:
            imageName = "play.fill"
            tintColor = .label
        case .buffering:
            print("buffering")
        default:
            print(state.rawValue)
        }
        
        statePlaying = state
        
        //if let selecedIndexPath = selecedIndexPath, let cell = collectionView.cellForItem(at: selecedIndexPath) as? RadioStationCell {
        if let selecedRadioItem = selecedRadioItem, let cell = searchCellSelected(radio: selecedRadioItem) {
            state == .playing ? !gifAnimationView.isAnimating ? gifAnimationView.startAnimating() : nil : gifAnimationView.stopAnimating()
            
            //??
            if state == .buffering {
                cellLoadingIndicatorStart(cell)
            }

            if state == .playing {
                cellLoadingIndicatorStop(cell)
                self.playedIndicatorViewWidthAnchor?.constant = 34
                gifAnimationLeadingAnchor?.constant = 0//-16
                
                if animation {
                    UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.75, initialSpringVelocity: 0.75, options: .curveEaseIn, animations: {
                        self.view.layoutIfNeeded()
                    })
                }
            }
            
            let config = UIImage.SymbolConfiguration(pointSize: 24, weight: .bold, scale: .small)
            //cell.playButton.setImage(UIImage(systemName: "pause.fill", withConfiguration: config)?.withRenderingMode(.alwaysTemplate), for: .normal)
            
            if animation {
                UIView.transition(with: cell.playButton as UIView, duration: 0.2, options: .transitionCrossDissolve, animations: {
                    cell.playButton.setImage(UIImage(systemName: imageName, withConfiguration: config)?.withRenderingMode(.alwaysTemplate), for: .normal)
                    cell.playButton.tintColor = tintColor
                }, completion: { (_ )in })
            }else{
                cell.playButton.setImage(UIImage(systemName: imageName, withConfiguration: config)?.withRenderingMode(.alwaysTemplate), for: .normal)
                cell.playButton.tintColor = tintColor
                
                collectionView.visibleCells.forEach { visibleCell in
                    if let visibleCell = visibleCell as? RadioStationCell {
                        if visibleCell.radioItem != cell.radioItem {
                            visibleCell.playButton.alpha = 1
                            visibleCell.playButton.setImage(UIImage(systemName: "play.fill", withConfiguration: config)?.withRenderingMode(.alwaysTemplate), for: .normal)
                            visibleCell.playButton.tintColor = .label
                        }else{
                            if state == .playing {
                                visibleCell.playButton.alpha = 1
                            }
                        }
                    }
                }
            }
        }
    }
    
    private func cellLoadingIndicatorStart(_ cell :RadioStationCell) {
        if !cell.loadingActivityIndicatorView.isAnimating {
            cell.loadingActivityIndicatorView.startAnimating()
            cell.playButton.alpha = 0
        }
    }
    
    private func cellLoadingIndicatorStop(_ cell :RadioStationCell) {
        if cell.loadingActivityIndicatorView.isAnimating {
            cell.loadingActivityIndicatorView.stopAnimating()
            cell.playButton.alpha = 1
        }
    }
    
    private func playPauseCellButtonHandlre(radio: RadioStation, indexPath: IndexPath) {
        if indexPath != selecedIndexPath {
            didSelectItemAtRadioStation(radio: radio, indexPath: indexPath)
        }else{
            delegate?.playPauseCell()
        }
    }
    
    var hideStatusBar: Bool = true {
        didSet {
            setNeedsStatusBarAppearanceUpdate()
        }
    }
    

    override var prefersStatusBarHidden: Bool {
        return hideStatusBar
    }
    
    override var preferredStatusBarUpdateAnimation: UIStatusBarAnimation{
        return .slide
    }
    
    
    var initSearchController = false
    var dismissSearchController = false
    
    private func testFunc(_ animation: Bool = true){
        if let selecedRadioItem = self.selecedRadioItem {
            if let cell = self.searchCellSelected(radio: selecedRadioItem) {
                
                self.playedIndicatorView.isHidden = self.tileStyle ? true : false
                self.gifAnimationView.isHidden = false
                self.playedIndicatorViewTopAnchor?.isActive = false
                self.playedIndicatorViewTopAnchor = self.playedIndicatorView.topAnchor.constraint(equalTo: cell.topAnchor)
                self.playedIndicatorViewTopAnchor?.isActive = true
                
                if let selecedIndexPath = self.selecedIndexPath {
                    self.collectionView.deselectItem(at: selecedIndexPath, animated: false)
                    self.didDeselectItemAtRadioStation(indexPath: selecedIndexPath)
                }
                
                if self.tileStyle {
                    gifAnimationViewBottomAnchor?.isActive = false
                    gifAnimationViewBottomAnchor = gifAnimationView.bottomAnchor.constraint(equalTo: cell.bottomAnchor, constant: 0)
                    gifAnimationViewBottomAnchor?.isActive = true
                    
                    gifAnimationViewHeightAnchor?.isActive = false
                    gifAnimationViewHeightAnchor = gifAnimationView.heightAnchor.constraint(equalTo: cell.imageView.heightAnchor, multiplier: 0.75)
                    gifAnimationViewHeightAnchor?.isActive = true

                    
                    gifAnimationLeadingAnchor?.isActive = false
                    gifAnimationViewCenterXAnchor?.isActive = false
                    gifAnimationViewCenterXAnchor = gifAnimationView.centerXAnchor.constraint(equalTo: cell.centerXAnchor)
                    gifAnimationViewCenterXAnchor?.isActive = true
                    

                    UIView.animate(withDuration: animation ? 0.25 : 0) {
                        //cell.contentView.layer.opacity = 0.4
                        cell.darkView.alpha = 1
                    }
                    
                }else{
                    gifAnimationViewBottomAnchor?.isActive = false
                    gifAnimationViewBottomAnchor = gifAnimationView.bottomAnchor.constraint(equalTo: playedIndicatorView.bottomAnchor, constant: 0)
                    gifAnimationViewBottomAnchor?.isActive = true
                    
                    gifAnimationViewHeightAnchor?.isActive = false
                    gifAnimationViewHeightAnchor = gifAnimationView.heightAnchor.constraint(equalTo: playedIndicatorView.heightAnchor, multiplier: 0.75)
                    gifAnimationViewHeightAnchor?.isActive = true
                    
                    gifAnimationViewCenterXAnchor?.isActive = false
                    gifAnimationLeadingAnchor?.isActive = true
                    
                    //cell.contentView.layer.opacity = 1
                    UIView.animate(withDuration: animation ? 0.25 : 0) {
                        cell.darkView.alpha = 0
                    }
                }

                if let colorHex = cell.radioItem?.colorHex{
                    self.playedIndicatorView.backgroundColor = UIColor(hex: colorHex)
                }
                
                let indexPath = self.collectionView.indexPath(for: cell)
                self.collectionView.selectItem(at: indexPath, animated: false, scrollPosition: .init())
                self.selecedIndexPath = indexPath
                
                self.changeCellRadioStatusPlaying(state: self.statePlaying, animation: false)
            }else{
                self.playedIndicatorView.isHidden = true
                self.gifAnimationView.isHidden = true
            }
        }
    }
}

extension RadioStationViewController: UISearchControllerDelegate {
    func willDismissSearchController(_ searchController: UISearchController) {
        dismissSearchController = true
    }
    
    
    
    func didDismissSearchController(_ searchController: UISearchController) {
        initSearchController = false
        dismissSearchController = false
    }
    
    func didPresentSearchController(_ searchController: UISearchController) {
        
    }
    
    func willPresentSearchController(_ searchController: UISearchController) {
        initSearchController = true
    }
}


extension RadioStationViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        guard let text = searchController.searchBar.text else { return }
//        print(text)
        if initSearchController {
            initSearchController = false
            return
        }
        
        if dismissSearchController {
            dismissSearchController = false
            return
        }
        
        if text.isEmpty {
            stationArraySearch = stationArray
            return
        }
        stationArraySearch = stationArray.filter { (radio: RadioStation) -> Bool in
            return (radio.title?.lowercased().contains(text.lowercased()) ?? false)
        }
    }
}





