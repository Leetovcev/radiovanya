//
//  SecondViewController.swift
//  RadioVanya
//
//  Created by Александр Краснощеков on 18.05.2021.
//  Copyright © 2021 Александр Краснощеков. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = #colorLiteral(red: 0.9990238547, green: 0.3811987638, blue: 0.006764368154, alpha: 1)
        
        setupDismissButton()
        
        setGradientBackground()
    }
    
    func setGradientBackground() {
        let colorTop =  #colorLiteral(red: 0.999315083, green: 0.3765257597, blue: 0.009756078944, alpha: 1).cgColor
        let colorBottom = #colorLiteral(red: 0.9989798665, green: 0.1903337836, blue: 0.001962964656, alpha: 1).cgColor
                    
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop, colorBottom]
        gradientLayer.locations = [0.0, 1.0]
        gradientLayer.frame = self.view.bounds
                
        self.view.layer.insertSublayer(gradientLayer, at:0)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
    }
    
    
    let dismissButton: UIButton = {
        let button = UIButton()
        button.setTitle("dismiss", for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private func setupDismissButton() {
        view.addSubview(dismissButton)
        NSLayoutConstraint.activate([
            dismissButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16),
            dismissButton.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 16)
        ])
        
        dismissButton.addTarget(self, action: #selector(dismissHandler), for: .touchUpInside)
    }
    
    @objc private func dismissHandler(){
        
        dismiss(animated: true) {
//            self.view.layer.removeFromSuperlayer()
        }
    }
}
