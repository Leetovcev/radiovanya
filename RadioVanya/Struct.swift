//
//  Struct.swift
//  RadioVanya
//
//  Created by Александр Краснощеков on 07.10.2020.
//  Copyright © 2020 Александр Краснощеков. All rights reserved.
//

import UIKit
import Realm
import RealmSwift

@objcMembers class RadioStations: Object, Decodable {
    @objc dynamic var id = 0
    let result = RealmSwift.List<RadioStation>()
    
    enum CodingKeys: String, CodingKey {
        case result
    }
    
    required init(from decoder: Decoder) throws
    {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        let resultList = try container.decode([RadioStation].self, forKey: .result)
        result.append(objectsIn: resultList)
        
        super.init()
    }
    
    override class func primaryKey() -> String? {
        return "id"
    }
    
    required override init()
    {
        super.init()
    }
    
    required init(value: Any, schema: RLMSchema)
    {
        super.init()
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema)
    {
        super.init()
    }
}

@objcMembers class RadioStation: Object, Decodable {
    @objc dynamic var streamId: Int = 0
    @objc dynamic var title: String?
    @objc dynamic var trackJson: String?
    @objc dynamic var image300: String?
    @objc dynamic var streamUrl: String?
    @objc dynamic var colorHex: String?
    
    enum CodingKeys: String, CodingKey {
        case streamId
        case title
        case trackJson
        case image300
        case streamUrl
        case colorHex
    }
    
    required init(from decoder: Decoder) throws
    {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        streamId = try container.decode(Int.self, forKey: .streamId)
        title = try container.decode(String.self, forKey: .title)
        trackJson = try? container.decode(String.self, forKey: .trackJson)
        image300 = try container.decode(String.self, forKey: .image300)
        streamUrl = try container.decode(String.self, forKey: .streamUrl)
        colorHex = try container.decode(String.self, forKey: .colorHex)
                
        super.init()
    }
    
    override static func primaryKey() -> String?
    {
        return "streamId"
    }
    
    required override init()
    {
        super.init()
    }
    
    required init(value: Any, schema: RLMSchema)
    {
        super.init()
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema)
    {
        super.init()
    }
}


struct Playlist: Codable {
    let items: [itemsTrack]
}

struct itemsTrack: Codable {
    let track: String?
    let artist: String?
    let duration: String?
    let startdate: String?
    let starttime: String?
    let image100: String?
    let image600: String?
    let service: String?
    let itunesId: Int?
    let itunesUrl: String?
    let listenUrl: String?
    let trackPrice: String?
}

struct itemsTrackAndMenu {
    let itemsTrack: itemsTrack?
    let menu: [[moreTableItem]]?
}

@objcMembers class Videos: Object, Decodable {
    @objc dynamic var id: Int = 0
    @objc dynamic var name: String?
    @objc dynamic var title: String?
    @objc dynamic var youtube_link: String?
    @objc dynamic var body: String?
    @objc dynamic var preview_img: String?
    @objc dynamic var created_at: String?
    @objc dynamic var updated_at: String?
    @objc dynamic var youtube_embed: String?
    
    enum CodingKeys : String, CodingKey {
        case id
        case name
        case title
        case youtube_link
        case body
        case preview_img
        case created_at
        case updated_at
        case youtube_embed
    }
    
    
    required init(from decoder: Decoder) throws
    {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        id = try container.decode(Int.self, forKey: .id)
        name = try? container.decode(String.self, forKey: .name)
        title = try? container.decode(String.self, forKey: .title)
        youtube_link = try? container.decode(String.self, forKey: .youtube_link)
        body = try? container.decode(String.self, forKey: .body)
        preview_img = try? container.decode(String.self, forKey: .preview_img)
        created_at = try? container.decode(String.self, forKey: .created_at)
        updated_at = try? container.decode(String.self, forKey: .updated_at)
        youtube_embed = try? container.decode(String.self, forKey: .youtube_embed)
                
        super.init()
    }
    
    override static func primaryKey() -> String?
    {
        return "id"
    }
    
    func incrementID() -> Int {
        let realm = try! Realm()
        return (realm.objects(Videos.self).max(ofProperty: "id") as Int? ?? 0) + 1
    }
    
    required override init()
    {
        super.init()
    }
    
    required init(value: Any, schema: RLMSchema)
    {
        super.init()
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema)
    {
        super.init()
    }
}

struct News: Codable {
    let id: Int?
    let name: String?
    let annotation_text: String?
    let annotation_image: String?
    let body: String?
    let status: Int?
    let seo_tags: String?
    let seo_title: String?
    let seo_slug: String?
    let seo_keywords: String?
    let post_start: String?
    let post_end: String?
    let ip_current: String?
    let ip_last: String?
    let user_id: Int?
    let created_at: String?
    let updated_at: String?
    
    enum CodingKeys : String, CodingKey {
        case id
        case name
        case annotation_text
        case annotation_image
        case body
        case status
        case seo_tags
        case seo_title
        case seo_slug
        case seo_keywords
        case post_start
        case post_end
        case ip_current
        case ip_last
        case user_id
        case created_at
        case updated_at
    }
}

struct Citys: Codable {
    let result: [cityResult]
}

struct cityResult: Codable {
    let city: String?
    let freq: String?
}

struct SectionCity {
    let letter : String
    let citys : [cityResult]
}

struct Contacts {
    let title: String?
    let contact: [Contact]
}

struct Contact {
    let img: UIImage?
    let title: String?
    let detailTitle: String?
    let type: CellType?
}

enum CellType: String {
    case telephone = "tel://"
    case email = "mailto:"
    case adress
}

struct Program {
    let title: String?
    let textProgram: [String]?
    let phoneArray: [PhoneArray]?
}

struct PhoneArray {
    let city: String
    let phone: String
}


struct moreTableItem {
    let image: UIImage?
    let title: String?
    let artist: String?
    let song: String?
    let itunesUrl: String?
    let type: typeItemCell?
}

enum typeItemCell: String, CaseIterable {
    case vk = "vk"
    case spotify = "spotify"
    case youtube = "youtube"
    case appleMusic = "appleMusic"
    case yandexmusic = "yandexmusic"
    case share
    case copy
}

//enum Social: String {
//    case vk = "https://vk.com/radiovanya"
//    case instagram = "https://www.instagram.com/vanya_radio/"
//}

struct Social {
    static let vk  = "https://vk.com/radiovanya"
    static let instagram = "https://www.instagram.com/vanya_radio/"
    static let AppStore = "https://www.instagram.com/vanya_radio/"
    static let ReviewAppStore = "https://www.instagram.com/vanya_radio/"
}

extension String{
    func replace(_ dictionary: [String: String]) -> String{
        var result = String()
        var i = -1
        for (of , with): (String, String)in dictionary{
            i += 1
            if i<1{
                result = self.replacingOccurrences(of: of, with: with)
            }else{
                result = result.replacingOccurrences(of: of, with: with)
            }
        }
        return result
    }
}
