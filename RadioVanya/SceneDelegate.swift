//
//  SceneDelegate.swift
//  RadioVanya
//
//  Created by Александр Краснощеков on 07.10.2020.
//  Copyright © 2020 Александр Краснощеков. All rights reserved.
//

import UIKit

extension SceneDelegate: NotificationViewDelegate {
    func showNotificationView(image: UIImage?, title: String) {
        guard let view = self.window else { return }
        imageNotification.image = image?.withRenderingMode(.alwaysTemplate)
        imageNotification.tintColor = .secondaryLabel
        titleNotification.text = title
        if isNotificationViewOpen { return }
        notificationViewBottomAnchor?.constant = notificationViewHeight
        UIView.animate(withDuration: 0.25, delay: 0) {
            view.layoutIfNeeded()
        } completion: { (_ ) in
            self.isNotificationViewOpen = true
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
            self.notificationViewBottomAnchor?.constant = 0
            UIView.animate(withDuration: 0.25, delay: 0) {
                view.layoutIfNeeded()
            } completion: { (_ ) in
                self.isNotificationViewOpen = false
            }
        }
    }
}

class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    
    var window: UIWindow?
    
    var launchedStationURL: String?
    
    //Notification View
    var isNotificationViewOpen: Bool = false
    var notificationViewHeight: CGFloat = 64
    var notificationViewBottomAnchor: NSLayoutConstraint?
    
    let wrapperNotificationView: UIView = {
        let view = UIView()
        let blurEffect = UIBlurEffect(style: .systemMaterial)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.isUserInteractionEnabled = false
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.addSubview(blurEffectView)
        blurEffectView.translatesAutoresizingMaskIntoConstraints = false
        blurEffectView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        blurEffectView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        blurEffectView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        blurEffectView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let notificationStackView: UIStackView = {
        let sv = UIStackView()
        sv.spacing = 8
        sv.translatesAutoresizingMaskIntoConstraints = false
        return sv
    }()
    
    let imageNotification: UIImageView = {
        let iv = UIImageView()
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    let titleNotification: UILabel = {
        let label = UILabel()
        label.textColor = .label
        label.font = UIFont.systemFont(ofSize: 16)//UIFont.boldSystemFont(ofSize: 16)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    //Notification View

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        // Use this method to optionally configure and attach the UIWindow `window` to the provided UIWindowScene `scene`.
        // If using a storyboard, the `window` property will automatically be initialized and attached to the scene.
        // This delegate does not imply the connecting scene or session are new (see `application:configurationForConnectingSceneSession` instead).
        guard let windowScene = (scene as? UIWindowScene) else { return }

        window = UIWindow(frame: UIScreen.main.bounds)
        window?.windowScene = windowScene
        let tabBar = TabBarController()
        window?.rootViewController = tabBar//UINavigationController(rootViewController: TabBarController())//RadioStationViewController //PlayerViewController
        window?.makeKeyAndVisible()
        setupNotificationView(tabBar)
        tabBar.delegateNV = self
        
        
//        if let shortcutItem = connectionOptions.shortcutItem?.type {
//
//        }
        
        if connectionOptions.shortcutItem?.type == "station", let parameters = connectionOptions.shortcutItem?.userInfo as? [String: String] {
            if parameters.keys.first == "play" {
                openStationLink(parameters.values.first)
            }
        }
        
        
        let urlinfo = connectionOptions.urlContexts
        if let url = urlinfo.first?.url {
            urlScheme(url)
        }
    }
    

    func scene(_ scene: UIScene, openURLContexts URLContexts: Set<UIOpenURLContext>) {
        if let url = URLContexts.first?.url {
            urlScheme(url)
        }
    }
    
    func urlScheme(_ url: URL){
        guard let components = NSURLComponents(url: url, resolvingAgainstBaseURL: true), let host = components.host, let params = components.queryItems else { return }
        
        if host == "station" {
            if let prefix = params.first(where: { $0.name == "streamId" })?.value {
                openStationLink(prefix)
            }
        }
    }
    
    func handleShortCutItem(shortcutItem: UIApplicationShortcutItem) -> Bool {
        if shortcutItem.type == "station", let parameters = shortcutItem.userInfo as? [String: String] {
            if parameters.keys.first == "play" {
                openStationLink(parameters.values.first)
                return true
            }else{
                return false
            }
        }else{
            return false
        }
    }
    
    func windowScene(_ windowScene: UIWindowScene, performActionFor shortcutItem: UIApplicationShortcutItem, completionHandler: @escaping (Bool) -> Void) {
        let handled = handleShortCutItem(shortcutItem: shortcutItem)
        completionHandler(handled)
    }
    
    func openStationLink(_ prefix: String?){
        if let prefix = prefix {
            if let mainTabBarController = window?.rootViewController as? TabBarController{
                mainTabBarController.radioStationViewController.stationOnLink = Int(prefix)
                //mainTabBarController.delegateNV?.showNotificationView(image: UIImage(systemName: "checkmark.circle.fill"), title: prefix)
            }
        }
    }
    
    private func setupNotificationView(_ tabBar: TabBarController){
        guard let view = window else { return }
        view.addSubview(wrapperNotificationView)
        NSLayoutConstraint.activate([
            wrapperNotificationView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            wrapperNotificationView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            
        ])
        notificationViewBottomAnchor = wrapperNotificationView.bottomAnchor.constraint(equalTo: view.topAnchor, constant: 0)
        notificationViewBottomAnchor?.isActive = true
        
        var topSafeArea: CGFloat
        if #available(iOS 11.0, *) {
            topSafeArea = tabBar.view.safeAreaInsets.top
        } else {
            topSafeArea = tabBar.topLayoutGuide.length
        }
        notificationViewHeight += topSafeArea == 0 ? 20 : topSafeArea
        wrapperNotificationView.heightAnchor.constraint(equalToConstant: notificationViewHeight).isActive = true

        wrapperNotificationView.addSubview(notificationStackView)
        NSLayoutConstraint.activate([
            notificationStackView.leadingAnchor.constraint(equalTo: wrapperNotificationView.leadingAnchor, constant: 16),
            notificationStackView.trailingAnchor.constraint(equalTo: wrapperNotificationView.trailingAnchor, constant: -16),
            notificationStackView.topAnchor.constraint(equalTo: wrapperNotificationView.safeAreaLayoutGuide.topAnchor, constant: 16),
            notificationStackView.bottomAnchor.constraint(equalTo: wrapperNotificationView.bottomAnchor, constant: -24),
        ])
        [imageNotification, titleNotification].forEach {notificationStackView.addArrangedSubview($0)}
        imageNotification.widthAnchor.constraint(equalTo: imageNotification.heightAnchor, multiplier: 1).isActive = true
        
        let bottomView = UIView()
//        bottomView.backgroundColor = .systemGray4
        
        let blurEffect = UIBlurEffect(style: .systemMaterial)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.isUserInteractionEnabled = false
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        bottomView.addSubview(blurEffectView)
        blurEffectView.translatesAutoresizingMaskIntoConstraints = false
        blurEffectView.topAnchor.constraint(equalTo: bottomView.topAnchor).isActive = true
        blurEffectView.leadingAnchor.constraint(equalTo: bottomView.leadingAnchor).isActive = true
        blurEffectView.trailingAnchor.constraint(equalTo: bottomView.trailingAnchor).isActive = true
        blurEffectView.bottomAnchor.constraint(equalTo: bottomView.bottomAnchor).isActive = true
        
        bottomView.layer.cornerRadius = 2.5
        bottomView.layer.masksToBounds = true
        bottomView.translatesAutoresizingMaskIntoConstraints = false
        
        wrapperNotificationView.addSubview(bottomView)
        NSLayoutConstraint.activate([
            bottomView.centerXAnchor.constraint(equalTo: wrapperNotificationView.centerXAnchor),
            bottomView.bottomAnchor.constraint(equalTo: wrapperNotificationView.bottomAnchor, constant: -8),
            bottomView.widthAnchor.constraint(equalToConstant: 44),
            bottomView.heightAnchor.constraint(equalToConstant: 5)
        ])
        
        let separatorView = UIView()
        separatorView.backgroundColor = .separator
        separatorView.translatesAutoresizingMaskIntoConstraints = false
        wrapperNotificationView.addSubview(separatorView)
        NSLayoutConstraint.activate([
            separatorView.leadingAnchor.constraint(equalTo: wrapperNotificationView.leadingAnchor),
            separatorView.trailingAnchor.constraint(equalTo: wrapperNotificationView.trailingAnchor),
            separatorView.bottomAnchor.constraint(equalTo: wrapperNotificationView.bottomAnchor),
            separatorView.heightAnchor.constraint(equalToConstant: 0.5)
        ])
    }

    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not neccessarily discarded (see `application:didDiscardSceneSessions` instead).
    }

    func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
        if launchedStationURL != nil {
            openStationLink(launchedStationURL)
            launchedStationURL = nil
        }
    }

    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }

    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
    }

    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.

        // Save changes in the application's managed object context when the application transitions to the background.
        (UIApplication.shared.delegate as? AppDelegate)?.saveContext()
    }


    
}

