//
//  PlayerViewController.swift
//  RadioVanya
//
//  Created by Александр Краснощеков on 07.10.2020.
//  Copyright © 2020 Александр Краснощеков. All rights reserved.
//

import UIKit
import MarqueeLabel
import Foundation
import SDWebImage
import StreamingKit
import MediaPlayer
import AVFoundation

protocol RaadioPlayerControlDelegate {
    func controlsPlayer (button: UIButton)
    
    func closePlayerView ()
    
    func statePlaying (state: STKAudioPlayerState)
    
    func handlerMoreView (item: itemsTrack)
}

extension PlayerViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        heightPlaylistTableView?.constant = CGFloat(playlistArray.count) * heightCell
        separatorPlaylistView.isHidden = playlistArray.isEmpty
        titlePlaylistLabel.isHidden = playlistArray.isEmpty
        wrapperScrollView.isScrollEnabled = !playlistArray.isEmpty
        return playlistArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! PlaylistTableCell
        let track = playlistArray[indexPath.item]
        cell.cellItemsTrack = track

        cell.moreButtonDelegate = {
            self.handlerMoreView(track)
        }

        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return heightCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) as? PlaylistTableCell {
            selectedCellHandler(cell)
        }
    }
    
//    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
//        if let cell = tableView.cellForRow(at: indexPath) as? PlaylistTableCell {
//            deselectCellHandler(cell)
//        }
//    }
}

extension PlayerViewController: STKAudioPlayerDelegate {
    func audioPlayer(_ audioPlayer: STKAudioPlayer, didStartPlayingQueueItemId queueItemId: NSObject) {
        
    }
    
    func audioPlayer(_ audioPlayer: STKAudioPlayer, didFinishBufferingSourceWithQueueItemId queueItemId: NSObject) {
        
    }
    
    func audioPlayer(_ audioPlayer: STKAudioPlayer, stateChanged state: STKAudioPlayerState, previousState: STKAudioPlayerState) {
        delegate?.statePlaying(state: state)
        changePlayPauseButton(state: state)
    }
    
    func audioPlayer(_ audioPlayer: STKAudioPlayer, didFinishPlayingQueueItemId queueItemId: NSObject, with stopReason: STKAudioPlayerStopReason, andProgress progress: Double, andDuration duration: Double) {
        
    }
    
    func audioPlayer(_ audioPlayer: STKAudioPlayer, unexpectedError errorCode: STKAudioPlayerErrorCode) {
        
    }
}

class PlayerViewController: UIViewController {
    
    var playlistArray = [itemsTrack]()
    var delegate: RaadioPlayerControlDelegate?
    
    let heightCell: CGFloat = 64
    
    lazy var wrapperScrollView: UIScrollView = {
        let scrView = UIScrollView()
        //scrView.contentSize = CGSize(width: view.frame.width, height: 1000)
        scrView.verticalScrollIndicatorInsets.top = -0.1
        scrView.translatesAutoresizingMaskIntoConstraints = false
        return scrView
    }()
    
    let wrapperStackView: UIStackView = {
        let sv = UIStackView()
        sv.axis = .vertical
        //sv.spacing = 16
        sv.translatesAutoresizingMaskIntoConstraints = false
        return sv
    }()
    
    let wrapperNowPlayStackView: UIStackView = {
        let sv = UIStackView()
        sv.axis = .vertical
        sv.spacing = 16
        sv.layoutMargins = UIEdgeInsets(top: 16, left: 32, bottom: 0, right: 32)
        sv.isLayoutMarginsRelativeArrangement = true
        sv.translatesAutoresizingMaskIntoConstraints = false
        return sv
    }()
    
    let titleStackView: UIStackView = {
        let sv = UIStackView()
        sv.spacing = 8
        sv.translatesAutoresizingMaskIntoConstraints = false
        return sv
    }()
    
    let titleImageViewWrapper: UIView = {
        let view = UIView()
        view.backgroundColor = .tintSelectColor
        view.layer.cornerRadius = 12
        view.layer.masksToBounds = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let titleImageView: UIImageView = {
        let iv = UIImageView()
        iv.backgroundColor = .tintSelectColor
        iv.contentMode = .scaleAspectFit
        iv.image = #imageLiteral(resourceName: "note_icon").withRenderingMode(.alwaysTemplate)
        iv.tintColor = .systemBackground//.white
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    let titleLabelWrapperView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.text = "СЕЙЧАС В ЭФИРЕ"
        label.font = UIFont.init(name: "BebasNeueBold", size: 24)
        label.textColor = .label//.black
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let trackImageView: UIImageView = {
        let iv = UIImageView()
        iv.image = #imageLiteral(resourceName: "600x600bb")
        iv.layer.cornerRadius = 16
        iv.layer.masksToBounds = true
        iv.contentMode = .scaleAspectFill
        iv.transform = CGAffineTransform(scaleX: 0.85, y: 0.85)
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    let trackImageViewShadow: UIView = {
        let view = UIView()
        view.layer.shadowColor = UIColor(named: "shadow")?.cgColor//lightGray //tertiarySystemBackground //secondarySystemBackground
        view.layer.shadowOffset = CGSize(width: 0, height: 6.0)
        view.layer.shadowRadius = 12.0//6.0
        view.layer.shadowOpacity = 1//0.7
        view.layer.masksToBounds = false
        view.layer.shouldRasterize = true
        view.layer.rasterizationScale = UIScreen.main.scale
        view.alpha = 0
        view.transform = CGAffineTransform(scaleX: 0.85, y: 0.85)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let artistSongWrapperStackView: UIStackView = {
        let sv = UIStackView()
        sv.layoutMargins = UIEdgeInsets(top: 0, left: -24, bottom: 0, right: 0)
        sv.isLayoutMarginsRelativeArrangement = true
        sv.spacing = 8
        return sv
    }()
    
    let artistSongStackView: UIStackView = {
        let sv = UIStackView()
        sv.axis = .vertical
        //sv.spacing = 4
        return sv
    }()
    
    let artistLabel: MarqueeLabel = {
        let label = MarqueeLabel()
        label.text = "ВАНЯ"
        //label.font = UIFont.init(name: "BebasNeueBold", size: 24)
        label.font = UIFont.systemFont(ofSize: 24, weight: .semibold)
        label.textColor = .tintSelectColor//#colorLiteral(red: 0.5685759783, green: 0.5686605573, blue: 0.5685573816, alpha: 1)//.lightGray
        label.type = .continuous
        label.speed = .duration(21) //Speed
        label.fadeLength = 10.0 //Mask Fade
        label.trailingBuffer = 30.0 // XBOCT
        label.leadingBuffer = 24
        label.animationCurve = .easeInOut //slow in out
        return label
    }()
    
    let songLabel: MarqueeLabel = {
        let label = MarqueeLabel()
        label.text = "РАДИО"
        //label.font = UIFont.init(name: "BebasNeueBold", size: 24)
        label.font = UIFont.systemFont(ofSize: 24, weight: .semibold)
        label.textColor = .label//.black
        label.type = .continuous
        label.speed = .duration(21) //Speed
        label.fadeLength = 10.0 //Mask Fade
        label.trailingBuffer = 30.0 // XBOCT
        label.leadingBuffer = 24
        label.animationCurve = .easeInOut //slow in out
        return label
    }()
    
    let audioControlsStackView: UIStackView = {
        let sv = UIStackView()
        sv.distribution = .equalSpacing
        return sv
    }()
    
    let backwardButton: UIButton = {
        let button = UIButton()
        let config = UIImage.SymbolConfiguration(pointSize: 30, weight: .bold, scale: .medium)
        button.setImage(UIImage(systemName: "backward.fill", withConfiguration: config)?.withRenderingMode(.alwaysTemplate), for: .normal)
        button.tintColor = .label//.black
        button.tag = 1
        button.addTarget(self, action: #selector(handlerPlayControl), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    let backwardButtonRadius: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 32
        view.layer.masksToBounds = true
        view.alpha = 0
        view.backgroundColor = UIColor.init(white: 0.7, alpha: 0.3)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let playPauseButton: UIButton = {
        let button = UIButton()
        let config = UIImage.SymbolConfiguration(pointSize: 44, weight: .bold, scale: .large)
        button.setImage(UIImage(systemName: "play.fill", withConfiguration: config)?.withRenderingMode(.alwaysTemplate), for: .normal)//pause.fill
        button.tintColor = .label//.black
        button.tag = 2
        button.addTarget(self, action: #selector(handlerPlayControl), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    let playPauseButtonActivityIndicatorView: UIActivityIndicatorView = {
        let ai = UIActivityIndicatorView()
        ai.color = .secondaryLabel//.gray
        ai.style = .large
        ai.translatesAutoresizingMaskIntoConstraints = false
        return ai
    }()
    
    let playPauseButtonRadius: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 40
        view.layer.masksToBounds = true
        view.alpha = 0
        view.backgroundColor = UIColor.init(white: 0.7, alpha: 0.3)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let forwardButton: UIButton = {
        let button = UIButton()
        let config = UIImage.SymbolConfiguration(pointSize: 30, weight: .bold, scale: .medium)
        button.setImage(UIImage(systemName: "forward.fill", withConfiguration: config)?.withRenderingMode(.alwaysTemplate), for: .normal)
        button.tintColor = .label//.black
        button.tag = 3
        button.addTarget(self, action: #selector(handlerPlayControl), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    let forwardButtonRadius: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 32
        view.layer.masksToBounds = true
        view.alpha = 0
        view.backgroundColor = UIColor.init(white: 0.7, alpha: 0.3)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var popupButtonPlayPause: UIButton = {
        let button = UIButton()//frame: CGRect(x: 0, y: 0, width: 44, height: 44)
        let config = UIImage.SymbolConfiguration(pointSize: 30, weight: .bold, scale: .small)
        button.setImage(UIImage(systemName: "play.fill", withConfiguration: config)?.withRenderingMode(.alwaysTemplate), for: .normal)
        button.tintColor = .label//.black
//        button.layer.cornerRadius = 22
//        button.layer.masksToBounds = true
        button.tag = 2
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    let popupLoadingActivityIndicatorView: UIActivityIndicatorView = {
        let ai = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 44, height: 44))
        ai.color = .secondaryLabel//.gray
        ai.style = .medium
        ai.startAnimating()
        return ai
    }()
    
    var popupPlayPauseButton: UIBarButtonItem = {
        let BarButton = UIBarButtonItem()//(customView: popupButtonPlayPause)
        BarButton.width = 44
        return BarButton
    }()
    
    let popupButtonNext: UIButton = {
        let button = UIButton()//frame: CGRect(x: 0, y: 0, width: 44, height: 44)
        let config = UIImage.SymbolConfiguration(pointSize: 30, weight: .bold, scale: .small)
        button.setImage(UIImage(systemName: "forward.fill", withConfiguration: config)?.withRenderingMode(.alwaysTemplate), for: .normal)
        button.tintColor = .label//.black
//        button.layer.cornerRadius = 22
//        button.layer.masksToBounds = true
        button.tag = 3
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    let popupNextButton: UIBarButtonItem = {
        let BarButton = UIBarButtonItem()//(customView: popupButtonPlayPause)
        BarButton.width = 44
        return BarButton
    }()
    
    let moreNowPlayView: UIView = {
        let view = UIView()
        
        view.backgroundColor = .tintSelectColor
        
        let blurEffect = UIBlurEffect(style: .systemThickMaterial)//.light
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
//        blurEffectView.isUserInteractionEnabled = false
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.addSubview(blurEffectView)
        blurEffectView.translatesAutoresizingMaskIntoConstraints = false
        blurEffectView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        blurEffectView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        blurEffectView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        blurEffectView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
        view.layer.cornerRadius = 16
        view.layer.masksToBounds = true
        view.isUserInteractionEnabled = false
        
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let moreNowPlayImageView: UIImageView = {
        let iv = UIImageView()
        let config = UIImage.SymbolConfiguration(pointSize: 18, weight: .semibold, scale: .medium)
        iv.image = UIImage(systemName: "ellipsis", withConfiguration: config)?.withRenderingMode(.alwaysTemplate)
        iv.tintColor = .tintSelectColor
        iv.contentMode = .scaleAspectFit
        iv.isUserInteractionEnabled = false
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
        
    //РАНЕЕ В ЭФИРЕ
    let separatorPlaylistView: UIView = {
        let view = UIView()
        view.backgroundColor = .separator//.secondaryLabel//.lightGray
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let titlePlaylistLabel: UILabel = {
        let label = UILabel()
        label.text = "РАНЕЕ В ЭФИРЕ"
        label.font = UIFont.init(name: "BebasNeueBold", size: 24)
        label.textColor = .label//.black
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    //CLOSE BUTTON DISMISS
    let closeButton: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 16
        view.layer.masksToBounds = true
        view.isUserInteractionEnabled = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let closeButtonImageView: UIImageView = {
        let iv = UIImageView()
        let config = UIImage.SymbolConfiguration(pointSize: 18, weight: .heavy, scale: .medium)
        iv.image = UIImage(systemName: "xmark", withConfiguration: config)?.withRenderingMode(.alwaysTemplate)
        iv.tintColor = .label//.black
        iv.contentMode = .scaleAspectFit
        iv.isUserInteractionEnabled = false
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    private let cellId = "cellId"
    
    let playlistTableView: UITableView = {
        let tableView = UITableView()
        tableView.backgroundColor = .clear
        tableView.translatesAutoresizingMaskIntoConstraints = false
        return tableView
    }()
    
    var heightPlaylistTableView: NSLayoutConstraint?
    
    
    var timerJsonTrackRadio: Timer?
    var trackJson: String?
    var radioStationSelected: RadioStation?
    
    var radioStation: RadioStation? {
        didSet {
            if let audioPlayer = audioPlayer {
                if radioStationSelected?.streamId != radioStation?.streamId {
                    if radioStation != nil {
                        audioPlayer.stop()
                    }
                    
                    if let streamUrl = radioStation?.streamUrl, let url = URL(string: streamUrl) {
                        let dataSource = STKAudioPlayer.dataSource(from: url)
                        audioPlayer.setDataSource(dataSource, withQueueItemId: SampleQueueId(url: url, andCount: 0))
                    }
                    
                }else{
                    switch audioPlayer.state {
                    case .playing:
                        audioPlayer.stop()
                    case .paused:
                        audioPlayer.resume()
                    case .stopped:
//                        if let streamUrl = radioStation?.streamUrl {
//                            audioPlayer.queue(streamUrl)
//                        }
                        if let streamUrl = radioStation?.streamUrl, let url = URL(string: streamUrl) {
                            let dataSource = STKAudioPlayer.dataSource(from: url)
                            audioPlayer.setDataSource(dataSource, withQueueItemId: SampleQueueId(url: url, andCount: 0))
                        }
                    default:
                        print(audioPlayer.state.rawValue)
                    }
                }
            }
            
            radioStationSelected = radioStation
            
            trackJson = radioStation?.trackJson
            jsonPlaylistLoad()
            if timerJsonTrackRadio == nil {
                timerJsonTrackRadio = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(jsonPlaylistLoad), userInfo: nil, repeats: true)
            }else{
                timerJsonTrackRadio?.invalidate()
                timerJsonTrackRadio = nil
                timerJsonTrackRadio = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(jsonPlaylistLoad), userInfo: nil, repeats: true)
            }
            
            
        }
    }
    
    var audioPlayer: STKAudioPlayer?

    //MARK:- Progress View play Item Track
    let downloadProgressView: UIView = {
        let view = UIView()
        view.backgroundColor = .tintSelectColor
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let progressView: UIView = {
        let view = UIView()
        view.isHidden = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var downloadProgressViewHeightAnchor: NSLayoutConstraint?
    
    var progressViewWidthAnchor: NSLayoutConstraint?
    var progressViewTopAnchor: NSLayoutConstraint?
    
    var progressViewWidth: CGFloat = 1 {
        didSet {
            setProgressViewWidthAnchor(value: progressViewWidth, animation: true)
        }
    }

    private func setProgressViewWidthAnchor(value: CGFloat, animation: Bool) {
        progressViewWidthAnchor?.isActive = false
        progressViewWidthAnchor = progressView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: value)
        progressViewWidthAnchor?.isActive = true
        
        if animation {
            UIView.animate(withDuration: 0.25) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    var downloadProgressViewHeight: CGFloat = 0 {
        didSet {
            setDownloadProgressViewHeightAnchor(value: downloadProgressViewHeight, animation: true)
        }
    }
    
    private func setDownloadProgressViewHeightAnchor(value: CGFloat, animation: Bool) {
        downloadProgressViewHeightAnchor?.isActive = false
        downloadProgressViewHeightAnchor = downloadProgressView.heightAnchor.constraint(equalTo: progressView.heightAnchor, multiplier: value)
        downloadProgressViewHeightAnchor?.isActive = true
        
        if animation {
            UIView.animate(withDuration: 0.25) {
                self.view.layoutIfNeeded()
            }
        }
    }
    //MARK:-
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemBackground//.white
        
        setGradientBackground()
        
        addBlurView()
        
        setupAudioPlayer()
        
        setupPopUpRightBarButtonItems()
        
        setupPlayerProgressView()
        
        setupPlaylistTableView()
        setupViews()
    }
    
    private func addBlurView(){
        let blurEffect = UIBlurEffect(style: .systemMaterial)//.light systemThickMaterial
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.isUserInteractionEnabled = false
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.addSubview(blurEffectView)
        blurEffectView.translatesAutoresizingMaskIntoConstraints = false
        blurEffectView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        blurEffectView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        blurEffectView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        blurEffectView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }
    
    func setGradientBackground() {
        let colorTop =  UIColor.clear.cgColor
        let colorBottom = UIColor.init(white: 0, alpha: 0.7).cgColor
                    
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop, colorBottom]
        gradientLayer.startPoint = CGPoint(x:0.0, y:0.5)
        gradientLayer.endPoint = CGPoint(x:0.25, y:1)
        //gradientLayer.locations = [0.6, 1.0]
        gradientLayer.frame = self.view.bounds
                
        self.view.layer.insertSublayer(gradientLayer, at:0)
    }
    
    private func setupPlayerProgressView(){
        view.addSubview(progressView)
        progressViewTopAnchor = progressView.topAnchor.constraint(equalTo: view.topAnchor, constant: -heightCell)
        progressViewTopAnchor?.isActive = true
        progressView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        progressView.heightAnchor.constraint(equalToConstant: heightCell).isActive = true
        progressViewWidthAnchor = progressView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: progressViewWidth)
        progressViewWidthAnchor?.isActive = true
        
        progressView.addSubview(downloadProgressView)
        downloadProgressView.bottomAnchor.constraint(equalTo: progressView.bottomAnchor).isActive = true
        downloadProgressView.leadingAnchor.constraint(equalTo: progressView.leadingAnchor).isActive = true
        downloadProgressView.trailingAnchor.constraint(equalTo: progressView.trailingAnchor).isActive = true
        downloadProgressViewHeightAnchor = downloadProgressView.heightAnchor.constraint(equalTo: progressView.heightAnchor, multiplier: downloadProgressViewHeight)
        downloadProgressViewHeightAnchor?.isActive = true
        
        let blurEffect = UIBlurEffect(style: .systemThickMaterial)//.light
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
//        blurEffectView.isUserInteractionEnabled = false
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        progressView.addSubview(blurEffectView)
        blurEffectView.translatesAutoresizingMaskIntoConstraints = false
        blurEffectView.topAnchor.constraint(equalTo: progressView.topAnchor).isActive = true
        blurEffectView.leadingAnchor.constraint(equalTo: progressView.leadingAnchor, constant: 2).isActive = true
        blurEffectView.trailingAnchor.constraint(equalTo: progressView.trailingAnchor).isActive = true
        blurEffectView.bottomAnchor.constraint(equalTo: progressView.bottomAnchor).isActive = true
    }
    
    private func setupPopUpRightBarButtonItems() {
        popupPlayPauseButton.customView = popupButtonPlayPause
        popupNextButton.customView = popupButtonNext
        popupItem.rightBarButtonItems = [popupPlayPauseButton, popupNextButton]
        popupButtonPlayPause.addTarget(self, action: #selector(handlerPlayControl), for: .touchUpInside)
        popupButtonPlayPause.heightAnchor.constraint(equalToConstant: 44).isActive = true
        
        popupButtonNext.addTarget(self, action: #selector(handlerPlayControl), for: .touchUpInside)
        popupButtonNext.heightAnchor.constraint(equalToConstant: 44).isActive = true
    }
    
    private func setupAudioPlayer() {
        var options = STKAudioPlayerOptions()
        options.equalizerBandFrequencies = (32, 64, 125, 250, 500, 1000, 2000, 4000, 8000, 16000, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
        options.enableVolumeMixer = false//true
        options.flushQueueOnSeek = true
        options.bufferSizeInSeconds = 15
        
        audioPlayer = STKAudioPlayer.init(options: options)
        
        if (audioPlayer != nil) {
            audioPlayer?.delegate = nil
        }
        audioPlayer?.delegate = self
        
        let bufferLength: Float32 = 0.1

        do{ try AVAudioSession.sharedInstance().setCategory(.playback)} catch {}
        do{ try AVAudioSession.sharedInstance().setPreferredIOBufferDuration(TimeInterval(bufferLength))} catch {}
        do{ try AVAudioSession.sharedInstance().setActive(true, options: [])} catch {}
        
        
        NotificationCenter.default.addObserver(self,selector: #selector(audioInterrupted), name: AVAudioSession.interruptionNotification, object: AVAudioSession.sharedInstance())
        
        setupEQaudioPlayer()
    }
    
    var playAfterPhoneCall: Bool = false
    
    @objc func audioInterrupted(noti: NSNotification) {
        guard noti.name == AVAudioSession.interruptionNotification && noti.userInfo != nil else {
            return
        }

        if let typenumber = (noti.userInfo?[AVAudioSessionInterruptionTypeKey] as AnyObject).uintValue {
            switch typenumber {
            case AVAudioSession.InterruptionType.began.rawValue:
                print("interrupted: began")
                if let audioPlayer = audioPlayer {
                    if audioPlayer.state == .playing {
                        audioPlayer.stop()
                        playAfterPhoneCall = true
                    }
                }
            case AVAudioSession.InterruptionType.ended.rawValue:
                print("interrupted: end")
                if playAfterPhoneCall {
                    playAfterPhoneCall = false
                    if let audioPlayer = audioPlayer {
                        if let streamUrl = radioStationSelected?.streamUrl, let url = URL(string: streamUrl) {
                            let dataSource = STKAudioPlayer.dataSource(from: url)
                            audioPlayer.setDataSource(dataSource, withQueueItemId: SampleQueueId(url: url, andCount: 0))
                        }
                    }
                }
            default:
                break
            }
        }
    }
    
    func setupEQaudioPlayer(){
        if let isEnabled = UserDefaults.standard.value(forKey: "EQisEnabled") as? Bool{
            audioPlayer?.equalizerEnabled = isEnabled
        }else{
            audioPlayer?.equalizerEnabled = false
        }
        
        if let EQArrayUserDefaults = UserDefaults.standard.value(forKey: "eqValueArray") as? Array<Float>{
            for (index, value) in EQArrayUserDefaults.enumerated() {
                audioPlayer?.setGain(value, forEqualizerBand: Int32(index))
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

    }
    
    private func setupPlaylistTableView(){
        playlistTableView.register(PlaylistTableCell.self, forCellReuseIdentifier: cellId)
        
        playlistTableView.delegate = self
        playlistTableView.dataSource = self
        
        playlistTableView.tableFooterView = UIView()
    }
    
    private func setupViews(){
        view.addSubview(wrapperScrollView)
        wrapperScrollView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        wrapperScrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        wrapperScrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        wrapperScrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
        wrapperScrollView.addSubview(wrapperStackView)
        wrapperStackView.topAnchor.constraint(equalTo: wrapperScrollView.topAnchor).isActive = true
        wrapperStackView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        wrapperStackView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        wrapperStackView.bottomAnchor.constraint(equalTo: wrapperScrollView.bottomAnchor).isActive = true
        
        let heightAnchorScrollView: NSLayoutConstraint = wrapperScrollView.heightAnchor.constraint(equalTo: wrapperStackView.heightAnchor)
        heightAnchorScrollView.priority = UILayoutPriority(rawValue: 1)
        heightAnchorScrollView.isActive = true
        
        [titleImageViewWrapper, titleLabelWrapperView].forEach {titleStackView.addArrangedSubview($0)}
        titleLabelWrapperView.addSubview(titleLabel)
        titleLabel.centerYAnchor.constraint(equalTo: titleLabelWrapperView.centerYAnchor, constant: 2.5).isActive = true
        //titleLabel.leadingAnchor.constraint(equalTo: titleLabelWrapperView.leadingAnchor).isActive = true
        //titleLabel.trailingAnchor.constraint(equalTo: titleLabelWrapperView.trailingAnchor).isActive = true
        
        titleImageViewWrapper.widthAnchor.constraint(equalToConstant: 24).isActive = true
        titleImageViewWrapper.heightAnchor.constraint(equalToConstant: 24).isActive = true
        
        titleImageViewWrapper.addSubview(titleImageView)
        titleImageView.topAnchor.constraint(equalTo: titleImageViewWrapper.topAnchor, constant: 4).isActive = true
        titleImageView.leadingAnchor.constraint(equalTo: titleImageViewWrapper.leadingAnchor, constant: 4).isActive = true
        titleImageView.trailingAnchor.constraint(equalTo: titleImageViewWrapper.trailingAnchor, constant: -4).isActive = true
        titleImageView.bottomAnchor.constraint(equalTo: titleImageViewWrapper.bottomAnchor, constant: -4).isActive = true

        [songLabel, artistLabel].forEach {artistSongStackView.addArrangedSubview($0)}
        
        let wrapperMoreNowPlayView = UIView()
        wrapperMoreNowPlayView.isUserInteractionEnabled = true
        wrapperMoreNowPlayView.translatesAutoresizingMaskIntoConstraints = false
        wrapperMoreNowPlayView.addSubview(moreNowPlayView)
        moreNowPlayView.widthAnchor.constraint(equalToConstant: 32).isActive = true
        moreNowPlayView.heightAnchor.constraint(equalToConstant: 32).isActive = true
        moreNowPlayView.trailingAnchor.constraint(equalTo: wrapperMoreNowPlayView.trailingAnchor).isActive = true
        moreNowPlayView.centerYAnchor.constraint(equalTo: wrapperMoreNowPlayView.centerYAnchor).isActive = true
        
        [artistSongStackView, wrapperMoreNowPlayView].forEach {artistSongWrapperStackView.addArrangedSubview($0)}
        wrapperMoreNowPlayView.widthAnchor.constraint(equalToConstant: 32).isActive = true
        
        let tapGestureMoreButton = UITapGestureRecognizer(target: self, action: #selector(moreButtonCurrentTrackHandler))
        wrapperMoreNowPlayView.addGestureRecognizer(tapGestureMoreButton)
        
        moreNowPlayView.addSubview(moreNowPlayImageView)
        moreNowPlayImageView.centerXAnchor.constraint(equalTo: moreNowPlayView.centerXAnchor).isActive = true
        moreNowPlayImageView.centerYAnchor.constraint(equalTo: moreNowPlayView.centerYAnchor).isActive = true
//        moreNowPlayImageView.widthAnchor.constraint(equalToConstant: 44).isActive = true
//        moreNowPlayImageView.heightAnchor.constraint(equalToConstant: 44).isActive = true
        
        [UIView(), backwardButton, UIView(), playPauseButton, UIView(), forwardButton, UIView()].forEach {audioControlsStackView.addArrangedSubview($0)}
        
        backwardButton.widthAnchor.constraint(equalToConstant: 44).isActive = true
        backwardButton.heightAnchor.constraint(equalToConstant: 44).isActive = true
        
        audioControlsStackView.insertSubview(backwardButtonRadius, belowSubview: backwardButton)
        backwardButtonRadius.centerXAnchor.constraint(equalTo: backwardButton.centerXAnchor).isActive = true
        backwardButtonRadius.centerYAnchor.constraint(equalTo: backwardButton.centerYAnchor).isActive = true
        backwardButtonRadius.widthAnchor.constraint(equalToConstant: 64).isActive = true
        backwardButtonRadius.heightAnchor.constraint(equalToConstant: 64).isActive = true
        backwardButtonRadius.transform = CGAffineTransform(scaleX: 0.3, y: 0.3)
        
        playPauseButton.widthAnchor.constraint(equalToConstant: 44).isActive = true
        playPauseButton.heightAnchor.constraint(equalToConstant: 44).isActive = true
        
        audioControlsStackView.insertSubview(playPauseButtonRadius, belowSubview: playPauseButton)
        playPauseButtonRadius.centerXAnchor.constraint(equalTo: playPauseButton.centerXAnchor).isActive = true
        playPauseButtonRadius.centerYAnchor.constraint(equalTo: playPauseButton.centerYAnchor).isActive = true
        playPauseButtonRadius.widthAnchor.constraint(equalToConstant: 80).isActive = true
        playPauseButtonRadius.heightAnchor.constraint(equalToConstant: 80).isActive = true
        playPauseButtonRadius.transform = CGAffineTransform(scaleX: 0.3, y: 0.3)
        
        audioControlsStackView.insertSubview(playPauseButtonActivityIndicatorView, aboveSubview: playPauseButton)
        playPauseButtonActivityIndicatorView.centerXAnchor.constraint(equalTo: playPauseButton.centerXAnchor).isActive = true
        playPauseButtonActivityIndicatorView.centerYAnchor.constraint(equalTo: playPauseButton.centerYAnchor).isActive = true
        
        forwardButton.widthAnchor.constraint(equalToConstant: 44).isActive = true
        forwardButton.heightAnchor.constraint(equalToConstant: 44).isActive = true
        
        audioControlsStackView.insertSubview(forwardButtonRadius, belowSubview: forwardButton)
        forwardButtonRadius.centerXAnchor.constraint(equalTo: forwardButton.centerXAnchor).isActive = true
        forwardButtonRadius.centerYAnchor.constraint(equalTo: forwardButton.centerYAnchor).isActive = true
        forwardButtonRadius.widthAnchor.constraint(equalToConstant: 64).isActive = true
        forwardButtonRadius.heightAnchor.constraint(equalToConstant: 64).isActive = true
        forwardButtonRadius.transform = CGAffineTransform(scaleX: 0.3, y: 0.3)
        //?
        [titleStackView, trackImageView, artistSongWrapperStackView, UIView(), audioControlsStackView, UIView(), separatorPlaylistView, UIView(), titlePlaylistLabel].forEach {wrapperNowPlayStackView.addArrangedSubview($0)}
        
        let paddingView = UIView()
        paddingView.translatesAutoresizingMaskIntoConstraints = false
        [wrapperNowPlayStackView, paddingView, playlistTableView, UIView()].forEach {wrapperStackView.addArrangedSubview($0)}
        paddingView.heightAnchor.constraint(equalToConstant: 16).isActive = true
        
        trackImageView.heightAnchor.constraint(equalTo: trackImageView.widthAnchor, multiplier: 1).isActive = true
        
        wrapperNowPlayStackView.insertSubview(trackImageViewShadow, belowSubview: trackImageView)//trackImageViewShadow
        trackImageViewShadow.centerXAnchor.constraint(equalTo: trackImageView.centerXAnchor).isActive = true
        trackImageViewShadow.centerYAnchor.constraint(equalTo: trackImageView.centerYAnchor).isActive = true
        trackImageViewShadow.widthAnchor.constraint(equalTo: trackImageView.widthAnchor, multiplier: 1).isActive = true
        trackImageViewShadow.heightAnchor.constraint(equalTo: trackImageView.heightAnchor, multiplier: 1).isActive = true

        separatorPlaylistView.heightAnchor.constraint(equalToConstant: 0.5).isActive = true
        
        heightPlaylistTableView = playlistTableView.heightAnchor.constraint(equalToConstant: 0)
        heightPlaylistTableView?.isActive = true
        
        view.addSubview(closeButton)
        closeButton.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 12).isActive = true
        closeButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -32).isActive = true
        closeButton.widthAnchor.constraint(equalToConstant: 32).isActive = true
        closeButton.heightAnchor.constraint(equalToConstant: 32).isActive = true
        
        let blurEffect = UIBlurEffect(style: .systemThickMaterial)//.light
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.isUserInteractionEnabled = false
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        closeButton.addSubview(blurEffectView)
        blurEffectView.translatesAutoresizingMaskIntoConstraints = false
        blurEffectView.topAnchor.constraint(equalTo: closeButton.topAnchor).isActive = true
        blurEffectView.leadingAnchor.constraint(equalTo: closeButton.leadingAnchor).isActive = true
        blurEffectView.trailingAnchor.constraint(equalTo: closeButton.trailingAnchor).isActive = true
        blurEffectView.bottomAnchor.constraint(equalTo: closeButton.bottomAnchor).isActive = true
        
        closeButton.addSubview(closeButtonImageView)
        closeButtonImageView.centerXAnchor.constraint(equalTo: closeButton.centerXAnchor).isActive = true
        closeButtonImageView.centerYAnchor.constraint(equalTo: closeButton.centerYAnchor).isActive = true
        //closeButtonImageView.widthAnchor.constraint(equalTo: closeButton.widthAnchor, multiplier: 1, constant: -12).isActive = true
        closeButtonImageView.heightAnchor.constraint(equalTo: closeButton.heightAnchor, multiplier: 1, constant: -12).isActive = true
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(closePlayerView))
        closeButton.addGestureRecognizer(tapGesture)
    }

    override func viewDidLayoutSubviews() {
        trackImageViewShadow.layer.shadowColor = UIColor(named: "shadow")?.cgColor
        trackImageViewShadow.layer.shadowPath = UIBezierPath(roundedRect: trackImageViewShadow.bounds, cornerRadius: 16).cgPath
    }
    
    @objc private func jsonPlaylistLoad(){
        let timestamp = NSDate().timeIntervalSince1970
        //let urlString = "https://dev.radiovanya.ru/api/v3/vast/0/?\(timestamp)"
        guard let urlString = trackJson, let url = URL(string: "\(urlString)?\(timestamp)") else { return }
        URLSession.shared.dataTask(with: url) { (data, _, err) in
            DispatchQueue.main.async {
                if let err = err {
                    print("Failed to get data from url:", err)
                    //self.notLoadTryAgain()
                    return
                }
                
                guard let data = data else { return } //self.notLoadTryAgain();
                
                do {
                    let tracks = try JSONDecoder().decode(Playlist.self, from: data)
                    DispatchQueue.main.async() {
                        var tracksPlaylist = tracks.items
                        if tracksPlaylist.count > 0 {
                            if self.playlistArray.count == 0 {
                                
                                let trackNow = tracksPlaylist.removeFirst()
                                self.changeTrackNowPlaying(trackNow)
                                
                                self.playlistArray = tracksPlaylist
                                self.playlistTableViewSetCompletionBlock()
                                
                                return
                            }
                            
                            let trackNow = tracksPlaylist.removeFirst()
                            if tracksPlaylist[0].starttime != self.playlistArray[0].starttime {
                                self.changeTrackNowPlaying(trackNow)
                                
                                self.playlistArray = tracksPlaylist
                                self.playlistTableViewSetCompletionBlock()
                            }
                        }
                    }
                } catch let jsonErr {
                    print("Failed to decode:", jsonErr)
                    //self.notLoadTryAgain()
                }
            }
        }.resume()
    }
    
    private func playlistTableViewSetCompletionBlock() {
        CATransaction.begin()
        CATransaction.setCompletionBlock {
            // Completion
            self.searchAndSelectPlayItemTrackCell()
        }
        self.playlistTableView.reloadData()
        CATransaction.commit()
    }
    
    private func searchAndSelectPlayItemTrackCell(){
        self.playItemCell?.wrapperPlaybackIndicatorView.isHidden = true
        var searchDone: Bool = false
        playlistTableView.visibleCells.forEach { (cell) in
            if let cell = cell as? PlaylistTableCell {
                if let playItemsTrack = playItemsTrack, let visibleCellItemsTrack = cell.cellItemsTrack, visibleCellItemsTrack.artist == playItemsTrack.artist {
                    progressView.isHidden = false
                    progressViewTopAnchor?.isActive = false
                    progressViewTopAnchor = progressView.topAnchor.constraint(equalTo: cell.topAnchor)
                    progressViewTopAnchor?.isActive = true
                    
                    wrapperPlaybackIndicatorViewhandler(cell)
                    
                    self.playItemCell = cell
                    
                    addImageViewTrackLayerMask()
                    searchDone = true
                }else{
                    if playlistTableView.visibleCells.last == cell && !searchDone{
                        progressView.isHidden = true
                    }
                    cell.wrapperPlaybackIndicatorView.isHidden = true
                    cell.imageViewTrack.layer.mask = nil
                }
            }
        }
    }
    
    private func wrapperPlaybackIndicatorViewhandler(_ cell: PlaylistTableCell) {
        if let player = player {
            cell.wrapperPlaybackIndicatorView.isHidden = false
            
            if player.isPlaying {
                cell.playbackIndicatorView.state = .playing
            }else{
                cell.playbackIndicatorView.state = .paused
            }
        }else{
            cell.playbackIndicatorView.state = .stopped
            cell.wrapperPlaybackIndicatorView.isHidden = true
        }
    }
    
    var currentTrackPlaying: itemsTrack?
    
    private func changeTrackNowPlaying(_ trackNow: itemsTrack) {
        self.currentTrackPlaying = trackNow
        self.artistLabel.text = trackNow.artist
        self.songLabel.text = trackNow.track
        
        if let song = trackNow.track, let artist = trackNow.artist {
            self.popupItem.title = "\(song) - \(artist)"
        }else if let song = trackNow.track{
            self.popupItem.title = song
        }else if let artist = trackNow.artist{
            self.popupItem.title = artist
        }else{
            self.popupItem.title = radioStation?.title
            self.popupItem.subtitle = nil
        }

        if let imgString = trackNow.image600, let imgUrl = URL(string: imgString) {
            self.trackImageView.sd_setImageWithURLWithFade(url: imgUrl, placeholderImage: trackImageView.image ?? #imageLiteral(resourceName: "600x600bb"), completion: { (image) in
                self.popupItem.image = image
                if let image = image {
                    UIView.animate(withDuration: 0.5) {
                        self.view.backgroundColor = image.averageColor
                    }
                }
                self.updateLockScreen()
            })
        }else{
            self.trackImageView.image = #imageLiteral(resourceName: "600x600bb")
            self.popupItem.image = #imageLiteral(resourceName: "600x600bb")
        }
        
        updateLockScreen()
    }
    
    @objc func handlerPlayControl(_ button: UIButton) {

        var radiusButton: UIView?
        let tag = button.tag
        if tag == 1 {
            if let audioPlayer = audioPlayer {
                audioPlayer.stop()
            }
            //animateImageTrackView(state: STKAudioPlayerState.init(rawValue: 0))
            radiusButton = backwardButtonRadius
        }else if tag == 2 {
            //??
            playToggle()
            radiusButton = playPauseButtonRadius
        }else if tag == 3 {
            if let audioPlayer = audioPlayer {
                audioPlayer.stop()
            }
            //animateImageTrackView(state: STKAudioPlayerState.init(rawValue: 0))
            radiusButton = forwardButtonRadius
        }
        
        UIView.animate(withDuration: 0.15, animations: {
            button.transform = CGAffineTransform(scaleX: 0.85, y: 0.85)
            if let radius = radiusButton {
                radius.transform = .identity
                radius.alpha = 1
            }
        }) { (_ ) in
            UIView.animate(withDuration: 0.15, animations: {
                button.transform = .identity
                if let radius = radiusButton {
                    radius.alpha = 0
                }
            }) { (_ ) in
                if let radius = radiusButton {
                    radius.transform = CGAffineTransform(scaleX: 0.3, y: 0.3)
                }
            }
        }
        
        delegate?.controlsPlayer(button: button)
    }
    
    func playToggle() {
        if let audioPlayer = audioPlayer {
            switch audioPlayer.state {
            case .playing:
                audioPlayer.stop()
            default:
                if let streamUrl = radioStationSelected?.streamUrl, let url = URL(string: streamUrl) {
                    let dataSource = STKAudioPlayer.dataSource(from: url)
                    audioPlayer.setDataSource(dataSource, withQueueItemId: SampleQueueId(url: url, andCount: 0))
                }
            }
        }
    }
    
    @objc func closePlayerView() {
        wrapperScrollView.setContentOffset(.zero, animated: true)
        delegate?.closePlayerView()
    }
    
    func changePlayPauseButton(state: STKAudioPlayerState) {
        var imageName: String = "play.fill"

        switch state {
        case .playing:
            print("DELEGATE - play")
            imageName = "pause.fill"
            pausePlayerIfPlayning()
            stopActivityIndicatorView()
        case .paused:
            print("DELEGATE - paused")
            imageName = "play.fill"
        case .stopped:
            print("DELEGATE - stoped")
            imageName = "play.fill"
        case .buffering:
            print("buffering")
            startActivityIndicatorView()
        default:
            print(state.rawValue)
        }
        
        let config = UIImage.SymbolConfiguration(pointSize: 44, weight: .bold, scale: .large)
        playPauseButton.setImage(UIImage(systemName: imageName, withConfiguration: config)?.withRenderingMode(.alwaysTemplate), for: .normal)
        
        
        let configPopUp = UIImage.SymbolConfiguration(pointSize: 30, weight: .bold, scale: .small)
        popupButtonPlayPause.setImage(UIImage(systemName: imageName, withConfiguration: configPopUp)?.withRenderingMode(.alwaysTemplate), for: .normal)
        
        animateImageTrackView(state: state)
    }
    
    private func stopMainPlayerIfPlayning() {
        if let audioPlayer = audioPlayer {
            if audioPlayer.state == .playing {
                audioPlayer.stop()
            }
        }
    }
    
    private func pausePlayerIfPlayning(){
        if let player = player, player.isPlaying {
            self.playItemCell?.playbackIndicatorView.state = .paused
            player.pause()
        }
    }
    
    func startActivityIndicatorView() {
        if !playPauseButtonActivityIndicatorView.isAnimating {
            playPauseButtonActivityIndicatorView.startAnimating()
            playPauseButton.alpha = 0
        }

        popupPlayPauseButton.customView = popupLoadingActivityIndicatorView
    }
    
    private func stopActivityIndicatorView() {
        if playPauseButtonActivityIndicatorView.isAnimating {
            playPauseButtonActivityIndicatorView.stopAnimating()
            playPauseButton.alpha = 1
        }

        popupPlayPauseButton.customView = popupButtonPlayPause
    }
    
    func animateImageTrackView (state: STKAudioPlayerState) {
        var transform: CGAffineTransform = CGAffineTransform(scaleX: 0.85, y: 0.85)
        var alpha: CGFloat = 0
        
        switch state {
        case .playing:
            transform = .identity
            alpha = 1
        default:
            transform = CGAffineTransform(scaleX: 0.85, y: 0.85)
            alpha = 0
        }
        
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1.0, initialSpringVelocity: 1.0, options: .curveEaseInOut, animations: {//
            self.trackImageView.transform = transform
            self.trackImageViewShadow.transform = transform
            self.trackImageViewShadow.alpha = alpha
        }) { (_ ) in
            
        }
    }
    
    func updateLockScreen() {
        // Define Now Playing Info
        var nowPlayingInfo = [String : Any]()
        
        if let image = trackImageView.image{
            nowPlayingInfo[MPMediaItemPropertyArtwork] = MPMediaItemArtwork.init(boundsSize: image.size, requestHandler: { (size) -> UIImage in
                    return image
            })//MPMediaItemArtwork(image: image)
        }
        nowPlayingInfo[MPMediaItemPropertyArtist] = artistLabel.text == nil ? radioStation?.title : artistLabel.text

        nowPlayingInfo[MPMediaItemPropertyTitle] = songLabel.text
        
        nowPlayingInfo[MPNowPlayingInfoPropertyIsLiveStream] = true

        MPNowPlayingInfoCenter.default().nowPlayingInfo = nowPlayingInfo
    }
    
    //MARD:- Selected PlayList Cell
    var player: AVAudioPlayer?
    
    var secondTimerItemTrack: Timer?
    var playItemCell: PlaylistTableCell?
    var playItemsTrack: itemsTrack?
    
    private func selectedCellHandler(_ cell: PlaylistTableCell) {
        
        if let cellItemsTrack = cell.cellItemsTrack {
            if let songPlayUrlString = cellItemsTrack.listenUrl {
                
                if self.downloadTask != nil {
                    self.downloadTask?.cancel()
                }

                print(songPlayUrlString)
                if cell == playItemCell && songPlayUrlString == self.playItemsTrack?.listenUrl && downloadTask == nil{
                    guard let player = player else { return }
                    if player.isPlaying {
                        player.pause()

                        cell.playbackIndicatorView.state = .paused
                        stopTimerItemTrack(complition: nil)
                    }else{
                        player.play()

                        ifNeedReturnPlayCurrentlyTrack(cell)
                        
                        cell.playbackIndicatorView.state = .playing
                        startTimerItemTrack()
                    }
                    return
                }
                playItemCellPlayIndicatorHide()
                
                playItemCell = cell
                playItemsTrack = cell.cellItemsTrack
                progressView.isHidden = false
                
                stopTimerItemTrack(complition: nil)
                
                setProgressViewWidthAnchor(value: 1, animation: false)
                setDownloadProgressViewHeightAnchor(value: 0, animation: false)
                
                progressViewTopAnchor?.isActive = false
                progressViewTopAnchor = progressView.topAnchor.constraint(equalTo: cell.topAnchor)
                progressViewTopAnchor?.isActive = true

                if let url = URL(string: songPlayUrlString) {
                    self.downloadFileFromURL(url)
                }
            }
        }
    }
    
    private func ifNeedReturnPlayCurrentlyTrack(_ cell: PlaylistTableCell){
        if self.progressViewWidth == 0 {
            setProgressViewWidthAnchor(value: 1, animation: false)
        }
        
        if self.downloadProgressViewHeight == 0 {
            setDownloadProgressViewHeightAnchor(value: 1, animation: false)
        }
        if cell.wrapperPlaybackIndicatorView.isHidden {
            cell.wrapperPlaybackIndicatorView.isHidden = false
        }
        if cell.imageViewTrack.layer.mask == nil {
            addImageViewTrackLayerMask()
        }
    }
    
    
    fileprivate var downloadTask: DownloadTask?
    
    @objc func downloadFileFromURL(_ url: URL){

        progressViewAnimationLoadStart()
                
        let request = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalCacheData, timeoutInterval: 30)
        downloadTask = DownloadService.shared.download(request: request)
        downloadTask?.completionHandler = { [weak self] in
            switch $0 {
            case .failure(let error):
                print(error)
            case .success(let data):
                print("Number of bytes: \(data.count)")
                self?.playItemTrack(data)
                self?.progressViewAnimationLoadStop()
                
            }
            self?.downloadTask = nil
        }
        
        downloadTask?.progressHandler = { [weak self] in
            print("Task1: \($0)")
            self?.downloadProgressViewHeight = CGFloat($0)
        }
        
        downloadTask?.resume()

    }
    
    private func progressViewAnimationLoadStart(){
        UIView.animate(withDuration: 0.5, delay: 0, options: [.repeat, .autoreverse], animations: {
            self.progressView.alpha = 0.5
        }, completion: nil)
    }
    
    private func progressViewAnimationLoadStop(){
            self.progressView.layer.removeAllAnimations()
//            self.observation?.invalidate()
            UIView.animate(withDuration: 0.5) {
                self.progressView.alpha = 1
                self.downloadProgressView.alpha = 1
        }
    }
    
//    private var someObservationContext = "something"
    
    func playItemTrack(_ data: Data) {
        do {
            self.player = try AVAudioPlayer(data: data)//(contentsOf: url)
            guard let player = player else { return }
//            player.addObserver(self, forKeyPath: "currentTime", options: [], context: &someObservationContext)
            player.prepareToPlay()
            player.volume = 1.0
            player.play()
            
            playItemCell?.wrapperPlaybackIndicatorView.isHidden = false
            playItemCell?.playbackIndicatorView.state = .playing
            
            addImageViewTrackLayerMask()
            
            startTimerItemTrack()
        } catch let error as NSError {
            //self.player = nil
            print(error.localizedDescription)
        } catch {
            print("AVAudioPlayer init failed")
        }
    }
    
    private func addImageViewTrackLayerMask() {
        if let playItemCell = playItemCell {
            let iv = playItemCell.imageViewTrack
            let maskLayer = CAShapeLayer()
            maskLayer.frame = iv.bounds
            // Create the frame for the circle.
            let radius: CGFloat = 33.0
            // Rectangle in which circle will be drawn
            let rect = CGRect(x: (iv.frame.width - radius) / 2, y: (iv.frame.height - radius) / 2, width: radius, height: radius)
            let circlePath = UIBezierPath(ovalIn: rect)
            // Create a path
            let path = UIBezierPath(rect: iv.bounds)
            // Append additional path which will create a circle
            path.append(circlePath)
            // Setup the fill rule to EvenOdd to properly mask the specified area and make a crater
            maskLayer.fillRule = CAShapeLayerFillRule.evenOdd
            // Append the circle to the path so that it is subtracted.
            maskLayer.path = path.cgPath
            // Mask our view with Blue background so that portion of red background is visible
            iv.layer.mask = maskLayer
        }
    }


    
    private func startTimerItemTrack(){
        stopMainPlayerIfPlayning()
        DispatchQueue.main.async {
            if self.secondTimerItemTrack == nil {
                self.secondViewHandler()
                self.secondTimerItemTrack = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.secondViewHandler), userInfo: nil, repeats: true)
                if let timer = self.secondTimerItemTrack {
                    RunLoop.main.add(timer, forMode: RunLoop.Mode.common)
                }
            }else{
                self.secondTimerItemTrack?.invalidate()
                self.secondTimerItemTrack = nil
                self.secondViewHandler()
                self.secondTimerItemTrack = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.secondViewHandler), userInfo: nil, repeats: true)
                if let timer = self.secondTimerItemTrack {
                    RunLoop.main.add(timer, forMode: RunLoop.Mode.common)
                }
            }
        }
    }
    
    private func stopTimerItemTrack(complition: ((Bool) -> ())?) {
        if let secondTimer = secondTimerItemTrack {
            secondTimer.invalidate()
            self.secondTimerItemTrack = nil
            complition?(true)
        }
    }
    
    @objc func secondViewHandler() {
        guard let player = player else { return }
        let total = player.duration
        let value: CGFloat = 1 - CGFloat((player.currentTime / total))
        
        if value < 1 && value > 0 {
            progressViewWidth = value
        }else{
            if !player.isPlaying {
                stopTimerItemTrack { (complition) in
                    if complition {
                        self.progressViewWidth = 0
                        self.downloadProgressViewHeight = 0
                        self.playItemCellPlayIndicatorHide()
                    }
                }
            }
        }
    }
    
    private func playItemCellPlayIndicatorHide(){
        self.playItemCell?.wrapperPlaybackIndicatorView.isHidden = true
        self.playItemCell?.imageViewTrack.layer.mask = nil
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if let player = player, let playItemCell = playItemCell {
            if player.isPlaying {
                playItemCell.playbackIndicatorView.state = .playing
            }
        }
    }
    
    @objc private func moreButtonCurrentTrackHandler() {
        if let track = currentTrackPlaying {
            handlerMoreView(track)
        }
    }
    
    private func handlerMoreView(_ track: itemsTrack) {
        delegate?.handlerMoreView(item: track)
    }
}
