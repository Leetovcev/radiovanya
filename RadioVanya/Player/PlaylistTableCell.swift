//
//  PlaylistTableCell.swift
//  RadioVanya
//
//  Created by Александр Краснощеков on 08.10.2020.
//  Copyright © 2020 Александр Краснощеков. All rights reserved.
//

import UIKit
import Foundation

import NAKPlaybackIndicatorView

class PlaylistTableCell: UITableViewCell {
    
    var cellItemsTrack: itemsTrack? {
        didSet {
            if let cellItemsTrack = cellItemsTrack {
                self.artistLabel.text = cellItemsTrack.artist
                self.songLabel.text = cellItemsTrack.track
                
                if let stringImgUrl = cellItemsTrack.image100, let imgUrl = URL(string: stringImgUrl) {
                    self.imageViewTrack.sd_setImageWithURLWithFade(url: imgUrl, placeholderImage: #imageLiteral(resourceName: "600x600bb"), completion: { (_ ) in})
                    self.contentView.layer.opacity = 1
                }else{
                    self.imageViewTrack.image = #imageLiteral(resourceName: "600x600bb")
                    self.contentView.layer.opacity = 0.4
                }
                
                if let stringStartTime = cellItemsTrack.starttime, stringStartTime.count > 3 {
                    self.timeLabel.text = stringStartTime.dropLast(3).string
                }
            }
        }
    }
    
    var moreButtonDelegate: (() -> ())?
    
    let wrapperStackView: UIStackView = {
        let sv = UIStackView()
        sv.spacing = 8
        sv.translatesAutoresizingMaskIntoConstraints = false
        return sv
    }()
    
    let imageViewTrack: UIImageView = {
        let iv = UIImageView()
        iv.image = #imageLiteral(resourceName: "600x600bb")
        iv.layer.cornerRadius = 8
        iv.layer.borderWidth = 0.5
        iv.layer.borderColor = UIColor.init(white: 0.7, alpha: 0.3).cgColor
        iv.layer.masksToBounds = true
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    let artistSongStackView: UIStackView = {
        let sv = UIStackView()
        sv.axis = .vertical
        sv.distribution = .fillEqually
        //sv.spacing = 1
        return sv
    }()
    
    let artistLabel: UILabel = {
        let label = UILabel()
        label.text = "ARTIST"
        label.textColor = #colorLiteral(red: 0.5685759783, green: 0.5686605573, blue: 0.5685573816, alpha: 1)
        label.font = UIFont.systemFont(ofSize: 16, weight: .semibold)
        return label
    }()
    
    let songLabel: UILabel = {
        let label = UILabel()
        label.text = "SONG"
        label.font = UIFont.systemFont(ofSize: 16, weight: .semibold)
        return label
    }()
    
    let timeLabel: UILabel = {
        let label = UILabel()
        label.text = "00:00"
        label.font = UIFont.systemFont(ofSize: 12)
        label.textColor = #colorLiteral(red: 0.5685759783, green: 0.5686605573, blue: 0.5685573816, alpha: 1)
//        label.textAlignment = .right
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let moreButton: UIButton = {
        let button = UIButton()
        let config = UIImage.SymbolConfiguration(pointSize: 18, weight: .semibold, scale: .medium)
        let image = UIImage(systemName: "ellipsis", withConfiguration: config)
        if let image = image, let rotateImage = image.rotate(radians: .pi/2) {
            let imageRotated: UIImage = rotateImage.withRenderingMode(.alwaysTemplate)
//            let imageRotated: UIImage = UIImage(cgImage: cgImage, scale: 0, orientation: UIImage.Orientation.left).withRenderingMode(.alwaysTemplate)
            button.setImage(imageRotated, for: .normal)
            button.tintColor = .secondaryLabel
        }
        button.layer.opacity = 0.7
        button.imageView?.contentMode = .scaleAspectFit
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    
    let wrapperPlaybackIndicatorView: UIView = {
        let view = UIView()
//        view.backgroundColor = .init(white: 0, alpha: 0.75)
        let blurEffect = UIBlurEffect(style: .systemThickMaterial)//.light
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.isUserInteractionEnabled = false
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        blurEffectView.alpha = 0.87
        view.addSubview(blurEffectView)
        blurEffectView.translatesAutoresizingMaskIntoConstraints = false
        blurEffectView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        blurEffectView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        blurEffectView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        blurEffectView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true

        view.isHidden = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let playbackIndicatorView: NAKPlaybackIndicatorView = {
        let style: NAKPlaybackIndicatorViewStyle = .iOS10()
        let view = NAKPlaybackIndicatorView(frame: .zero, style: style)
        view?.tintColor = .tintSelectColor
        view!.translatesAutoresizingMaskIntoConstraints = false
        return view!
    }()
    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        backgroundColor = .clear
        setupViews()
    }
    
    func setupViews(){
        
        contentView.addSubview(wrapperStackView)
        wrapperStackView.topAnchor.constraint(equalTo: topAnchor, constant: 8).isActive = true
        wrapperStackView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 32).isActive = true
        wrapperStackView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -32).isActive = true
        wrapperStackView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -8).isActive = true
        
        
        [timeLabel, songLabel, artistLabel].forEach {artistSongStackView.addArrangedSubview($0)}
        
        let artistSongStackViewWrapper = UIStackView(arrangedSubviews: [artistSongStackView])
        artistSongStackViewWrapper.alignment = .center
        
        let wrapperMoreButton = UIView()
        wrapperMoreButton.translatesAutoresizingMaskIntoConstraints = false
        [imageViewTrack, artistSongStackViewWrapper, wrapperMoreButton].forEach {wrapperStackView.addArrangedSubview($0)}
        wrapperMoreButton.widthAnchor.constraint(equalToConstant: 32).isActive = true
        
        addSubview(moreButton)
        moreButton.topAnchor.constraint(equalTo: wrapperMoreButton.topAnchor).isActive = true
        moreButton.leadingAnchor.constraint(equalTo: wrapperMoreButton.leadingAnchor).isActive = true
        moreButton.trailingAnchor.constraint(equalTo: wrapperMoreButton.trailingAnchor).isActive = true
        moreButton.bottomAnchor.constraint(equalTo: wrapperMoreButton.bottomAnchor).isActive = true
        
//        moreButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(moreButtonHandler)))
        moreButton.addTarget(self, action: #selector(moreButtonHandler), for: .touchUpInside)
        
        imageViewTrack.widthAnchor.constraint(equalTo: imageViewTrack.heightAnchor, multiplier: 1).isActive = true
        //timeLabel.widthAnchor.constraint(equalToConstant: 40).isActive = true
        
        addSubview(wrapperPlaybackIndicatorView)
        wrapperPlaybackIndicatorView.centerXAnchor.constraint(equalTo: imageViewTrack.centerXAnchor).isActive = true
        wrapperPlaybackIndicatorView.centerYAnchor.constraint(equalTo: imageViewTrack.centerYAnchor).isActive = true
        
        wrapperPlaybackIndicatorView.widthAnchor.constraint(equalToConstant: 34).isActive = true
        wrapperPlaybackIndicatorView.heightAnchor.constraint(equalToConstant: 34).isActive = true
        wrapperPlaybackIndicatorView.layer.cornerRadius = 17
        wrapperPlaybackIndicatorView.layer.masksToBounds = true
        
        wrapperPlaybackIndicatorView.addSubview(playbackIndicatorView)
        playbackIndicatorView.centerXAnchor.constraint(equalTo: wrapperPlaybackIndicatorView.centerXAnchor).isActive = true
        playbackIndicatorView.centerYAnchor.constraint(equalTo: wrapperPlaybackIndicatorView.centerYAnchor).isActive = true
        
        playbackIndicatorView.widthAnchor.constraint(equalToConstant: 44).isActive = true
        playbackIndicatorView.heightAnchor.constraint(equalToConstant: 44).isActive = true

        
        
        self.separatorInset.left = 84//52
        self.separatorInset.right = 32//32
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
//        let maskView = UIView(frame: imageViewTrack.frame)
//        maskView.backgroundColor = .blue
//        maskView.layer.cornerRadius = imageViewTrack.frame.width / 2
//        imageViewTrack.mask = maskView
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc fileprivate func moreButtonHandler(){
        moreButtonDelegate?()
    }
}


extension UIImage {
    func rotate(radians: Float) -> UIImage? {
        var newSize = CGRect(origin: CGPoint.zero, size: self.size).applying(CGAffineTransform(rotationAngle: CGFloat(radians))).size
        // Trim off the extremely small float value to prevent core graphics from rounding it up
        newSize.width = floor(newSize.width)
        newSize.height = floor(newSize.height)

        UIGraphicsBeginImageContextWithOptions(newSize, false, self.scale)
        let context = UIGraphicsGetCurrentContext()!

        // Move origin to middle
        context.translateBy(x: newSize.width/2, y: newSize.height/2)
        // Rotate around middle
        context.rotate(by: CGFloat(radians))
        // Draw the image at its center
        self.draw(in: CGRect(x: -self.size.width/2, y: -self.size.height/2, width: self.size.width, height: self.size.height))

        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return newImage
    }
}
