//
//  TimerCell.swift
//  RadioVanya
//
//  Created by Александр Краснощеков on 20.10.2020.
//  Copyright © 2020 Александр Краснощеков. All rights reserved.
//

import UIKit


class TimerCell: UITableViewCell {
    
    var senderDatePicker: ((_ sender: UIDatePicker) -> ())?
    
    let titleLabel: UILabel = {
        let label = UILabel()
//        label.textColor = .black
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let timerDatePicker: UIDatePicker = {
        let dp = UIDatePicker()
        dp.datePickerMode = .countDownTimer
        //picker.countDownDuration = 120
        //picker.isHidden = true
        dp.clipsToBounds = true
        dp.translatesAutoresizingMaskIntoConstraints = false
        return dp
    }()
    
    let timerLabel: UILabel = {
        let label = UILabel()
        label.textColor = .gray
        label.textAlignment = .right
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let timerSwitcher: UISwitch = {
        let sw = UISwitch()
        sw.isHidden = true
        sw.onTintColor = .tintSelectColor
        sw.translatesAutoresizingMaskIntoConstraints = false
        return sw
    }()
    
    var timerDatePickerHeigth: NSLayoutConstraint?
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupViews()
    }
    
    func setupViews() {
        contentView.addSubview(titleLabel)
        titleLabel.centerYAnchor.constraint(equalTo: topAnchor, constant: 22).isActive = true
        
        contentView.addSubview(timerSwitcher)
        timerSwitcher.centerYAnchor.constraint(equalTo: titleLabel.centerYAnchor).isActive = true
        
        if let textLabel = textLabel {
            titleLabel.leadingAnchor.constraint(equalTo: textLabel.leadingAnchor).isActive = true
            timerSwitcher.trailingAnchor.constraint(equalTo: textLabel.trailingAnchor, constant: -4).isActive = true
        }else{
            titleLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: separatorInset.left).isActive = true
            timerSwitcher.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16).isActive = true
        }
        
        
        contentView.addSubview(timerLabel)
        timerLabel.centerYAnchor.constraint(equalTo: titleLabel.centerYAnchor).isActive = true
        timerLabel.trailingAnchor.constraint(equalTo: timerSwitcher.leadingAnchor, constant: -8).isActive = true
        
        contentView.addSubview(timerDatePicker)
        timerDatePicker.topAnchor.constraint(equalTo: topAnchor, constant: 44).isActive = true
        timerDatePicker.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        timerDatePicker.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        
        DispatchQueue.main.async() {
            self.timerDatePicker.countDownDuration = 60
        }
        
        timerDatePicker.addTarget(self, action: #selector(handlerTimerDatePicker), for: .valueChanged)
        
        timerDatePickerHeigth = timerDatePicker.heightAnchor.constraint(equalToConstant: 0)
        timerDatePickerHeigth?.isActive = true
    }
    
    @objc func handlerTimerDatePicker(sender: UIDatePicker) {
        senderDatePicker?(sender)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
