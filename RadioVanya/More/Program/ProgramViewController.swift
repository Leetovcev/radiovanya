//
//  ProgramViewController.swift
//  RadioVanya
//
//  Created by Александр Краснощеков on 14.10.2020.
//  Copyright © 2020 Александр Краснощеков. All rights reserved.
//

import UIKit

extension ProgramViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return programArray[section].textProgram?.count ?? programArray[section].phoneArray?.count ?? 0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return programArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath)
        let cell = UITableViewCell(style: .value1, reuseIdentifier: cellId)
        
        if let itemText = programArray[indexPath.section].textProgram?[indexPath.row] {
            cell.textLabel?.text = itemText
            if indexPath.section == 3 {
                cell.textLabel?.textAlignment = .center
                cell.textLabel?.textColor = .tintSelectColor
                cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 18)
                cell.textLabel?.translatesAutoresizingMaskIntoConstraints = false
                cell.textLabel?.widthAnchor.constraint(equalTo: cell.widthAnchor, multiplier: 1).isActive = true
                cell.textLabel?.heightAnchor.constraint(equalTo: cell.heightAnchor, multiplier: 1).isActive = true
            }else{
                cell.textLabel?.textAlignment = .left
                cell.textLabel?.textColor = .label
            }
        }
        
        if let itemPhone = programArray[indexPath.section].phoneArray?[indexPath.row] {
            cell.textLabel?.text = itemPhone.city
            cell.detailTextLabel?.text = itemPhone.phone
        }
            
        //cell.textProgram.text = "1"//item
        
        cell.textLabel?.numberOfLines = 0
        cell.backgroundColor = .secondarySystemBackground//.init(white: 0.9, alpha: 0.3)
        
        switch indexPath.section {
        case 8:
            cell.selectionStyle = .default
        default:
            cell.selectionStyle = .none
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 46))
        
        
        let titleLabel = UILabel()
        titleLabel.text = programArray[section].title
        titleLabel.textColor = .label
        titleLabel.font = UIFont.init(name: "BebasNeueBold", size: 18)
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        
        headerView.addSubview(titleLabel)
        
        titleLabel.topAnchor.constraint(equalTo: headerView.topAnchor, constant: 8).isActive = true
        titleLabel.leadingAnchor.constraint(equalTo: headerView.leadingAnchor, constant: 20).isActive = true//lessThanOrEqualTo
        titleLabel.trailingAnchor.constraint(equalTo: headerView.trailingAnchor).isActive = true
        titleLabel.bottomAnchor.constraint(equalTo: headerView.bottomAnchor, constant: -8).isActive = true
        
        let headerViewTop = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: tableView.frame.width * 0.56933333))
        let imageHader = UIImageView()
        imageHader.image = #imageLiteral(resourceName: "request_program")
        imageHader.contentMode = .scaleToFill
        imageHader.layer.cornerRadius = 8
        imageHader.layer.masksToBounds = true
        
        headerViewTop.addSubview(imageHader)
        imageHader.translatesAutoresizingMaskIntoConstraints = false
        imageHader.topAnchor.constraint(equalTo: headerViewTop.topAnchor, constant: 16).isActive = true
        imageHader.leadingAnchor.constraint(equalTo: headerViewTop.leadingAnchor).isActive = true
        imageHader.trailingAnchor.constraint(equalTo: headerViewTop.trailingAnchor).isActive = true
        imageHader.bottomAnchor.constraint(equalTo: headerViewTop.bottomAnchor, constant: -16).isActive = true
        
        return section == 0 ? headerViewTop : headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        programArray[section].title == nil ? section == 0 ? tableView.frame.width * 0.56933333 : 0 : 46
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 8:
            if let phoneArray = programArray[indexPath.section].phoneArray {
                if phoneArray.count > 0 {
                    didSelectPhoneRow(phoneArray[indexPath.row].phone)
                }
            }
        default:
            print("default")
        }
    }
}

class ProgramViewController: UIViewController {
    
    let tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .insetGrouped)//grouped
        tableView.backgroundColor = .clear
        tableView.translatesAutoresizingMaskIntoConstraints = false
        return tableView
    }()
    
    private let cellId = "cellId"
    
    var programArray = [Program]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .systemBackground
        
        setupNavBar()
        
        programArraySetup()
        
        setupTableView()
    }
    
    func setupNavBar() {
        let titleLabel = UILabel()
        titleLabel.text = "Программа по заявкам"
        titleLabel.font = UIFont.init(name: "BebasNeueBold", size: 24)
        self.navigationItem.titleView = titleLabel
        self.navigationController?.navigationBar.tintColor = .tintSelectColor
    }
    
    private func setupTableView(){
        
        tableView.delegate = self
        tableView.dataSource = self
        
        view.addSubview(tableView)
        tableView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: cellId)
        
        //tableView.allowsSelection = false
    }
    
    private func programArraySetup() {
        
        let test1 = Program.init(title: nil, textProgram: ["Хотите услышать свой голос в эфире?"], phoneArray: nil)//"Хотите услышать свой голос в эфире?"
        let test2 = Program.init(title: nil, textProgram: ["Хотите подарить любимую песню друзьям и близким?"], phoneArray: nil)
        let test3 = Program.init(title: nil, textProgram: ["Хотите поздравить маму или признаться в любви дорогому человеку?\nА может просто передать всем ПРИВЕТ?"], phoneArray: nil)
        
        let test4 = Program.init(title: nil, textProgram: ["Тогда вы зашли по адресу!"], phoneArray: nil)
        
        let test5 = Program.init(title: "Когда выходит программа по заявкам?", textProgram: ["Каждый день с 13:00 до 14:00 и с 21:00 до 22:00 по московскому времени. Обращаем ваше внимание, что в некоторых городах программа по заявкам выходит по местному времени."], phoneArray: nil)
        
        let test6 = Program.init(title: "в какое время лучше звонить?", textProgram: ["Звонки в программу по заявкам “Всем привет на Радио Ваня” принимаются за несколько часов до выхода программы в эфир. Поэтому звонить лучше заранее, при этом, уточняя время, когда бы вы хотели услышать свой “Привет”. Например, дневные заявки принимаются вечером, а вечерние - в течение дня."], phoneArray: nil)
        
        let test7 = Program.init(title: "А чего говорить-то?", textProgram: ["Свою заявку лучше начать с фразы: “Всем привет на Радио Ваня!”. А далее ваш привет, поздравление друзьям и близким или просто заказ любимой песни, которая играет в эфире Радио Ваня. Закончить звонок желательно фразой: “Спасибо, Радио Ваня!”."], phoneArray: nil)
        
        let test8 = Program.init(title: "И все? Так просто?", textProgram: ["Да! Только следует помнить несколько несложных правил:\n\n• заказать можно только ту песню, которая играет в эфире Радио Ваня;\n• заявка должна быть простой и понятной, лаконичной, а главное, “приличной”. Иначе, увы, все ваше устное творчество останется за кадром, а в эфир пойдет только сама песня..."], phoneArray: nil)
        
        
        let phone1 = PhoneArray.init(city: "Арзамас", phone: "(3147) 7-66-47")
        let phone2 = PhoneArray.init(city: "Армавир", phone: "(6137) 3-03-00")
        let phone3 = PhoneArray.init(city: "Брянск", phone: "(4832) 30-39-70")
        let phone4 = PhoneArray.init(city: "Великие Луки", phone: "(1153) 5-70-32")
        let phone5 = PhoneArray.init(city: "Иваново", phone: "(4932) 59-10-49")
        let phone6 = PhoneArray.init(city: "Киров", phone: "(332) 24-92-49")
        let phone7 = PhoneArray.init(city: "Кинель-Черкассы", phone: "(846) 926-00-00")
        let phone8 = PhoneArray.init(city: "Ковров", phone: "(49232) 2-75-35")
        let phone9 = PhoneArray.init(city: "Самара", phone: "(846) 926-00-00")
        let phone10 = PhoneArray.init(city: "Саратов", phone: "(8452) 78-00-08")
        let phone11 = PhoneArray.init(city: "Смоленск", phone: "(4812) 32-42-20")
        let phone12 = PhoneArray.init(city: "Сызрань", phone: "(8482) 61-51-15")
        let phone13 = PhoneArray.init(city: "Тольятти", phone: "(8482) 61-51-15")
        
        
        let test9 = Program.init(title: "телефоны программы по заявкам “Всем привет”:", textProgram: nil, phoneArray: [phone1, phone2, phone3, phone4, phone5, phone6, phone7, phone8, phone9, phone10, phone11, phone12, phone13])
        
        
        [test1, test2, test3, test4, test5, test6, test7, test8, test9].forEach{programArray.append($0)}
    }
    
    private func deselectRow() {
        if let indexPath = tableView.indexPathForSelectedRow {
            tableView.deselectRow(at: indexPath, animated: true)
        }
    }
    
    private func didSelectPhoneRow(_ phone: String?){
        if let string = phone {
            let dictionary = ["+": "", " ": "", "(": "", ")": "", "-": "", "доб. 145": ""]
            let mobileResult = string.replace(dictionary)
            let urlString = "tel://\(mobileResult)"
            if let url = URL(string: urlString) {
                UIApplication.shared.open(url, options: [:]) { (_ ) in }
            }
        }
        deselectRow()
    }
}
