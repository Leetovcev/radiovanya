//
//  EQualizer.swift
//  RadioVanya
//
//  Created by Александр Краснощеков on 15.10.2020.
//  Copyright © 2020 Александр Краснощеков. All rights reserved.
//

import UIKit
import StreamingKit
import VerticalSlider

var textColorSet: UIColor = .label//.darkText//.white
//var tintSelectColor: UIColor = #colorLiteral(red: 0.06995903701, green: 0.8604989648, blue: 0.8549278378, alpha: 1)


class VerticalStackView: UIStackView {
    
    init(arrangedSubviews: [UIView], spacing: CGFloat = 0, distribution: UIStackView.Distribution = .fill, aligment: Alignment = .fill) {
        super.init(frame: .zero)
        
        arrangedSubviews.forEach({
            addArrangedSubview($0)
//            $0.translatesAutoresizingMaskIntoConstraints = false
//            $0.widthAnchor.constraint(equalToConstant: 20).isActive = true
        })
        
        self.spacing = spacing
        self.axis = .vertical
        self.distribution = distribution
        self.alignment = aligment
//        self.translatesAutoresizingMaskIntoConstraints = false
//        self.heightAnchor.constraint(equalToConstant: 20).isActive = true
    }
    
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

class HorizontalStackView: UIStackView {
    
    init(arrangedSubviews: [UIView]) {
        super.init(frame: .zero)
        
        arrangedSubviews.forEach({addArrangedSubview($0)})
        
        self.spacing = spacing
        self.axis = .horizontal
        self.distribution = .fillEqually
    }
    
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}


class vertStackView: UIStackView {
    
    init() {
        super.init(frame: .zero)
        
        for index in 1...25 {
            if index == 1{
                addArrangedSubview(UILabel(text: "1", font: .systemFont(ofSize: 12), textColor: .clear))
            }
            
            if index == 1 {
                addArrangedSubview(UIView(bg: .clear, heightAnchor: 0.5))
            }else if index == 13 {
                addArrangedSubview(UIStackView(arrangedSubviews: [UIView(bg: .systemBackground, heightAnchor: 0.5, widthAnchor: 15), UIView()]))//.init(white: 0.7, alpha: 0.7) #colorLiteral(red: 0.8705949187, green: 0.8705140948, blue: 0.8791016936, alpha: 1)
            }else if index == 25 {
                addArrangedSubview(UIView(bg: .clear, heightAnchor: 0.5))
            }else{
                addArrangedSubview(UIStackView(arrangedSubviews: [UIView(bg: .systemBackground, heightAnchor: 0.5, widthAnchor: 15), UIView()]))//.init(white: 0.5, alpha: 0.5) #colorLiteral(red: 0.8705949187, green: 0.8705140948, blue: 0.8791016936, alpha: 1)
            }
            
            if index == 25{
                addArrangedSubview(UIView(bg: .clear, heightAnchor: 0.5))
            }
            
        }
        self.axis = .vertical
        self.distribution = .equalCentering
        //self.layoutMargins = UIEdgeInsets(top: 15, left: 0, bottom: 5, right: 0)
        //self.isLayoutMarginsRelativeArrangement = true
    }
    
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

extension VerticalSlider {
    convenience init(tag: Int = 0) {
        self.init(frame: .zero)
        self.tag = tag
        self.minimumValue = -12
        self.maximumValue = 12
        self.value = 0
        //self.addTarget(self, action: #selector(handlerChangeSlider), for: .valueChanged)
    }
    
    
//    @objc func handlerChangeSlider(){
//        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "setGain"), object: nil, userInfo: ["value": slider.value, "forEqualizerBand": tag])
//    }
    
}



var arrayLabelTop = [
    UILabel(text: "32Hz", font: .systemFont(ofSize: 9), textColor: textColorSet),
    UILabel(text: "64", font: .systemFont(ofSize: 9), textColor: textColorSet),
    UILabel(text: "125", font: .systemFont(ofSize: 9), textColor: textColorSet),
    UILabel(text: "250", font: .systemFont(ofSize: 9), textColor: textColorSet),
    UILabel(text: "500", font: .systemFont(ofSize: 9), textColor: textColorSet),
    UILabel(text: "1K", font: .systemFont(ofSize: 9), textColor: textColorSet),
    UILabel(text: "2K", font: .systemFont(ofSize: 9), textColor: textColorSet),
    UILabel(text: "4K", font: .systemFont(ofSize: 9), textColor: textColorSet),
    UILabel(text: "8K", font: .systemFont(ofSize: 9), textColor: textColorSet),
    UILabel(text: "16KHz", font: .systemFont(ofSize: 9), textColor: textColorSet)
]

var timerLabel = [Timer(), Timer(), Timer(), Timer(), Timer(), Timer(), Timer(), Timer(), Timer(), Timer()]

var tableViewEQ: UITableView!
var selectedIndex: Int = 0
var arrayEQ = ["CUSTOM","ACOUSTIC","TREBLE","BASS","VOCAL","CLASSICAL","DANCE","SPOKEN WORD","ELECTRONIC","HIP-HOP","LOUDNESS","JAZZ","LATINO","LOUNGE","SMALL SPEAKERS","PIANO","FLAT","POP","DEEP","R&B","TREBLE REDUCER","BASS REDUCER","ROCK","PERFECT","BOOSTED"]

var eqValueArray: Array<Float> = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]

var resetEqualizer: UIButton! = {
    let button = UIButton(type: .system)
    button.setTitle("Сбросить", for: .normal)
    button.titleLabel?.font = UIFont.systemFont(ofSize: 16)
    button.setTitleColor(.gray, for: .disabled)
    button.isEnabled = false
    button.setTitleColor(.tintSelectColor, for: .normal)
    return button
}()

var closeEqualizer: UIButton! = {
    let button = UIButton(type: .system)
    button.setTitle("Закрыть", for: .normal)
    button.titleLabel?.font = UIFont.systemFont(ofSize: 16)
    button.setTitleColor(.gray, for: .disabled)
    button.isEnabled = false
    button.setTitleColor(.tintSelectColor, for: .normal)
    return button
}()

protocol EQualizerDelegate {
    func setGain (forEqualizerBand: Int32, value: Float)
    
    func eqIsOn (value: Bool)
}

class EQualizer: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var delegate: EQualizerDelegate?
    
    fileprivate let cellID = "cellID"
    
    let arraySliderText = ["32Hz", "64", "125", "250", "500", "1K", "2K", "4K", "8K", "16KHz"]
    
    let greaterToConstantWhidthEQ: CGFloat = 670
    
    lazy var sliderArray = [VerticalSlider(tag: 0), VerticalSlider(tag: 1), VerticalSlider(tag: 2), VerticalSlider(tag: 3), VerticalSlider(tag: 4), VerticalSlider(tag: 5), VerticalSlider(tag: 6), VerticalSlider(tag: 7), VerticalSlider(tag: 8), VerticalSlider(tag: 9)]
    
    let EQpresetArray: Array<Array<Float>> = [
        [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
        [5, 4.9, 3.95, 1.05, 2.15, 1.75, 3.5, 4.1, 3.55, 2.15],
        [0.0, 0.0, 0.0, 0.0, 0.0, 1.25, 2.5, 3.5, 4.25, 5.5],
        [5.5, 4.25, 3.5, 2.5, 1.25, 0.0, 0.0, 0.0, 0.0, 0.0],
        [-1.5, -3.0, -3.0, 1.5, 3.75, 3.75, 3.0, 1.5, 0.0, -1.5],
        [4.75, 3.75, 3.0, 2.5, -1.5, -1.5, 0.0, 2.25, 3.25, 3.75],
        [3.57, 6.55, 4.99, 0.0, 1.92, 3.65, 5.15, 4.54, 3.59, 0.0],
        [-3.46, -0.47, 0.0, 0.69, 3.46, 4.61, 4.84, 4.28, 2.54, 0.0],
        [4.25, 3.8, 1.2, 0.0, -2.15, 2.25, 0.85, 1.25, 3.95, 4.8],
        [5.0, 4.25, 1.5, 3.0, -1.0, -1.0, 1.5, -0.5, 2.0, 3.0],
        [6.0, 4.0, 0.0, 0.0, -2.0, 0.0, -1.0, -5.0, 5.0, 1.0],
        [4.0, 3.0, 1.5, 2.25, -1.5, -1.5, 0.0, 1.5, 3.0, 3.75],
        [4.5, 3.0, 0.0, 0.0, -1.5, -1.5, -1.5, 0.0, 3.0, 4.5],
        [-3.0, -1.5, -0.5, 1.5, 4.0, 2.5, 0.0, -1.5, 2.0, 1.0],
        [5.5, 4.25, 3.5, 2.5, 1.25, 0.0, -1.25, -2.5, -3.5, -4.25],
        [3.0, 2.0, 0.0, 2.5, 3.0, 1.5, 3.5, 4.5, 3.0, 3.5],
        [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
        [-1.5, -1.0, 0.0, 2.0, 4.0, 4.0, 2.0, 0.0, -1.0, -1.5],
        [4.95, 3.55, 1.75, 1.0, 2.85, 2.5, 1.45, -2.15, -3.55, -4.6],
        [2.62, 6.92, 5.65, 1.33, -2.19, -1.5, 2.32, 2.65, 3.0, 3.75],
        [0.0, 0.0, 0.0, 0.0, 0.0, -1.25, -2.5, -3.5, -4.25, -5.5],
        [-5.5, -4.25, -3.5, -2.5, -1.25, 0.0, 0.0, 0.0, 0.0, 0.0],
        [5.0, 4.0, 3.0, 1.5, -0.5, -1.0, 0.5, 2.5, 3.5, 4.5],
        [4.0, 2.27, 0.97, 0.32, -0.32, -1.17, 0.1, 1.62, 3.13, 3.35],
        [5.0, 4.9, 3.95, 0.54, 1.17, 1.17, 2.27, 4.1, 3.55, 2.15]
    ]
    
    let switchEQ: UISwitch! = {
        let sw = UISwitch()
        sw.translatesAutoresizingMaskIntoConstraints = false
        sw.addTarget(self, action: #selector(handlerSwitchEQ), for: .valueChanged)
        sw.onTintColor = .tintSelectColor
        return sw
    }()
    
    @objc fileprivate func handlerSwitchEQ(sw: UISwitch){
//        StreamKitPlayer.shared.handlerSwitchEQ(sw: sw)
        delegate?.eqIsOn(value: sw.isOn)
//        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "eqIsOn"), object: nil, userInfo: ["value": sw.isOn])
        UserDefaults.standard.set(sw.isOn, forKey: "EQisEnabled")
    }
    
    let sliderStackView = UIStackView()
    let stackViewTop = UIStackView()
    
    var tableViewEQTopAnchor: NSLayoutConstraint?
    var tableViewEQLeadingAnchor: NSLayoutConstraint?
    
    var sliderStackViewWidthAnchor: NSLayoutConstraint?
    var sliderStackViewHeightAnchor: NSLayoutConstraint?
    var varsliderStackViewCenterXAnchor: NSLayoutConstraint?
    var sliderStackViewLeadingAnchor: NSLayoutConstraint?
    var sliderStackViewTrailingAnchor: NSLayoutConstraint?
    var sliderStackViewTrailingBottomAnchor: NSLayoutConstraint?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNavBar()
        
        view.backgroundColor = .systemBackground//#colorLiteral(red: 0.9685427547, green: 0.9686817527, blue: 0.9685124755, alpha: 1)//.white

        
        resetEqualizer.addTarget(self, action: #selector(resetEQ), for: .touchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(customView: resetEqualizer)
                
        let topLabel = UILabel()
        topLabel.text = "Использовать эквалайзер"
        topLabel.textColor = textColorSet
        topLabel.font = .systemFont(ofSize: 18)
        

        stackViewTop.addArrangedSubview(topLabel)
        stackViewTop.addArrangedSubview(switchEQ)
        view.addSubview(stackViewTop)
        stackViewTop.translatesAutoresizingMaskIntoConstraints = false
        stackViewTop.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 16).isActive = true
        //stackViewTop.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16).isActive = true
        //stackViewTop.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16).isActive = true
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            //iPad
            stackViewTop.widthAnchor.constraint(greaterThanOrEqualToConstant: greaterToConstantWhidthEQ).isActive = true
            stackViewTop.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
            stackViewTop.leadingAnchor.constraint(greaterThanOrEqualTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 16).isActive = true
            stackViewTop.trailingAnchor.constraint(lessThanOrEqualTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -16).isActive = true
        }else{
            //iPhone
            stackViewTop.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 16).isActive = true
            stackViewTop.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -16).isActive = true
        }

        for index in 0...9{

            
            sliderStackView.addArrangedSubview(
                VerticalStackView(arrangedSubviews: [
                    arrayLabelTop[index],
                    sliderArray[index]
                    ])
            )


        }
        
        
        
        sliderStackView.distribution = .fillEqually
        //verticalStackView.spacing = 0
        
        view.addSubview(sliderStackView)
        sliderStackView.translatesAutoresizingMaskIntoConstraints = false
        sliderStackView.topAnchor.constraint(equalTo: stackViewTop.bottomAnchor, constant: 16).isActive = true
        //verticalStackView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16).isActive = true
        //verticalStackView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16).isActive = true
        //verticalStackView.heightAnchor.constraint(equalToConstant: 200).isActive = true
        sliderStackViewWidthAnchor = sliderStackView.widthAnchor.constraint(greaterThanOrEqualToConstant: greaterToConstantWhidthEQ)
        sliderStackViewWidthAnchor?.isActive = true
        sliderStackViewHeightAnchor = sliderStackView.heightAnchor.constraint(equalToConstant: 200)
        sliderStackViewHeightAnchor?.isActive = true
        varsliderStackViewCenterXAnchor = sliderStackView.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        varsliderStackViewCenterXAnchor?.isActive = true
        sliderStackViewLeadingAnchor = sliderStackView.leadingAnchor.constraint(greaterThanOrEqualTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 16)
        sliderStackViewLeadingAnchor?.isActive = true
        sliderStackViewTrailingAnchor = sliderStackView.trailingAnchor.constraint(lessThanOrEqualTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -16)
        sliderStackViewTrailingAnchor?.isActive = true
        sliderStackViewTrailingBottomAnchor = sliderStackView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -16)
        sliderStackViewTrailingBottomAnchor?.isActive = false
        
        
        let lineStackView = UIStackView()
        view.insertSubview(lineStackView, belowSubview: sliderStackView)
        
        for index in 1...25 {
            if index == 1{
                lineStackView.addArrangedSubview(UILabel(text: "1", font: .systemFont(ofSize: 12), textColor: .clear))
            }
            
            if index == 1 {
                lineStackView.addArrangedSubview(UIView(bg: .clear))
            }else if index == 13 {
                lineStackView.addArrangedSubview(UIView(bg: .systemGray4))//.init(white: 0.7, alpha: 0.7) #colorLiteral(red: 0.8705531955, green: 0.8705758452, blue: 0.8747941256, alpha: 1)
            }else if index == 25 {
                lineStackView.addArrangedSubview(UIView(bg: .clear))
            }else{
                lineStackView.addArrangedSubview(UIView(bg: .systemGray4))//.init(white: 0.5, alpha: 0.5) #colorLiteral(red: 0.8705531955, green: 0.8705758452, blue: 0.8747941256, alpha: 1)
            }
            
            if index == 25{
                lineStackView.addArrangedSubview(UIView(bg: .clear))
            }
            
        }
        lineStackView.axis = .vertical
        lineStackView.distribution = .equalCentering
        
        lineStackView.translatesAutoresizingMaskIntoConstraints = false
        lineStackView.topAnchor.constraint(equalTo: sliderStackView.topAnchor).isActive = true
        lineStackView.leadingAnchor.constraint(equalTo: sliderStackView.leadingAnchor).isActive = true
        lineStackView.trailingAnchor.constraint(equalTo: sliderStackView.trailingAnchor).isActive = true
        lineStackView.bottomAnchor.constraint(equalTo: sliderStackView.bottomAnchor).isActive = true
        
        
        let labelLeftStackView = UIStackView(arrangedSubviews: [
            UILabel(text: "12", font: .systemFont(ofSize: 6), textColor: textColorSet, textAlignment: .left),
            UILabel(text: "0", font: .systemFont(ofSize: 6), textColor: textColorSet, textAlignment: .left),
            UILabel(text: "-12", font: .systemFont(ofSize: 6), textColor: textColorSet, textAlignment: .left)
            ])
        labelLeftStackView.axis = .vertical
        labelLeftStackView.distribution = .equalCentering
        labelLeftStackView.layoutMargins = UIEdgeInsets(top: 19, left: 0, bottom: 9, right: 3)
        labelLeftStackView.isLayoutMarginsRelativeArrangement = true
        
        view.addSubview(labelLeftStackView)
        labelLeftStackView.translatesAutoresizingMaskIntoConstraints = false
        labelLeftStackView.topAnchor.constraint(equalTo: sliderStackView.topAnchor).isActive = true
        labelLeftStackView.leadingAnchor.constraint(equalTo: sliderStackView.leadingAnchor).isActive = true
        labelLeftStackView.bottomAnchor.constraint(equalTo: sliderStackView.bottomAnchor).isActive = true
        
        
        
        //MARK:- Table View
        tableViewEQ = UITableView(frame: .zero)
        tableViewEQ.backgroundColor = .clear
        tableViewEQ.register(UITableViewCell.self, forCellReuseIdentifier: cellID)
        tableViewEQ.delegate = self
        tableViewEQ.dataSource = self
        tableViewEQ.tableFooterView = UIView()
        
        
        view.addSubview(tableViewEQ)
        tableViewEQ.translatesAutoresizingMaskIntoConstraints = false
        tableViewEQTopAnchor = tableViewEQ.topAnchor.constraint(equalTo: sliderStackView.bottomAnchor, constant: 16)
        tableViewEQTopAnchor?.isActive = true
        tableViewEQLeadingAnchor = tableViewEQ.leadingAnchor.constraint(equalTo: view.leadingAnchor)
        tableViewEQLeadingAnchor?.isActive = true
        tableViewEQ.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        tableViewEQ.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        
        checkPresetUserDefaults()
        
        //        Timer.scheduledTimer(withTimeInterval: 0.001, repeats: true) {_ in
        //            print(320 * ((StreamKitPlayer.shared.audioPlayerSTK.averagePowerInDecibels(forChannel: 1) + 60) / 60))
        //        }
        
//        NotificationCenter.default.addObserver(self, selector: #selector(changeTextLabel), name: NSNotification.Name(rawValue: "setGain"), object: nil)
    }
        
    func setupNavBar() {
        let titleLabel = UILabel()
        titleLabel.text = "Эквалайзер"
        titleLabel.font = UIFont.init(name: "BebasNeueBold", size: 24)
        self.navigationItem.titleView = titleLabel
        self.navigationController?.navigationBar.tintColor = .tintSelectColor
    }
    
    @objc fileprivate func closeEQ(){
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc fileprivate func resetEQ(){
        for index in 0...9{
            //sliderArray[index].setValue(0.0)
            sliderArray[index].value = 0
            eqValueArray[index] = 0
//            StreamKitPlayer.shared.audioPlayerSTK.setGain(0, forEqualizerBand: Int32(index))
//            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "setGain"), object: nil, userInfo: ["value": Float(0), "forEqualizerBand": Int32(index)])
            delegate?.setGain(forEqualizerBand: Int32(index), value: Float(0))
        }
        selectedIndex = 0
        UserDefaults.standard.set(arrayEQ[selectedIndex], forKey: "eqPreset")
        UserDefaults.standard.set(eqValueArray, forKey: "eqValueArray")
        UserDefaults.standard.set(eqValueArray, forKey: "CUSTOMeqValueArray")
        DispatchQueue.main.async {
            tableViewEQ.reloadData()
            self.selectedPresetCell()
        }
    }
    
    
    func checkPresetUserDefaults(){
        if let isEnabled = UserDefaults.standard.value(forKey: "EQisEnabled") as? Bool{
            switchEQ.isOn = isEnabled
        }else{
            switchEQ.isOn = false
        }
        
        if let preset = UserDefaults.standard.value(forKey: "eqPreset") as? String{
            print("preset", preset)
            var row = 0
            for eqPreset in arrayEQ{
                if eqPreset == preset{
                    selectedIndex = row
                }
                row += 1
            }
        }
        
        if let EQArrayUserDefaults = UserDefaults.standard.value(forKey: "eqValueArray") as? Array<Float>{
            eqValueArray = EQArrayUserDefaults
            sendSliderEQ()
            print("SET: eqValueArray")
        }
        
    }
    
    fileprivate func sendSliderEQ(){
        for (index, value) in eqValueArray.enumerated() {
            sliderArray[index].value = value
            //StreamKitPlayer.shared.audioPlayerSTK.setGain(value, forEqualizerBand: Int32(index))
        }
    }
    
    
    //MARK:- override EQualizer
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        tableViewEQ.reloadData()
        selectedPresetCell()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.setValue(false, forKey: "hidesShadow")
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        currentlyOrientation()
    }
    
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        currentlyOrientation()
    }
    
    func currentlyOrientation(){
        if UIDevice.current.orientation.isLandscape {
            if UIDevice.current.userInterfaceIdiom == .pad {
                //iPad
                tableViewEQAnchorBottom()
                sliderStackView200()
            }else{
                //iPhone
                tableViewEQAnchorTop()
                sliderStackViewLeft()
            }
        }else{//portret
            if UIDevice.current.userInterfaceIdiom == .pad {
                //iPad
                tableViewEQAnchorBottom()
                sliderStackView200()
            }else{
                //iPhone
                tableViewEQAnchorBottom()
                sliderStackView200()
            }
        }
    }
    
    func tableViewEQAnchorBottom(){
        tableViewEQTopAnchor?.isActive = false
        tableViewEQTopAnchor = tableViewEQ.topAnchor.constraint(equalTo: sliderStackView.bottomAnchor, constant: 16)
        tableViewEQTopAnchor?.isActive = true
        
        tableViewEQLeadingAnchor?.isActive = false
        tableViewEQLeadingAnchor = tableViewEQ.leadingAnchor.constraint(equalTo: view.leadingAnchor)
        tableViewEQLeadingAnchor?.isActive = true
    }
    
    func tableViewEQAnchorTop(){
        tableViewEQTopAnchor?.isActive = false
        tableViewEQTopAnchor = tableViewEQ.topAnchor.constraint(equalTo: stackViewTop.bottomAnchor, constant: 16)
        tableViewEQTopAnchor?.isActive = true
        
        tableViewEQLeadingAnchor?.isActive = false
        tableViewEQLeadingAnchor = tableViewEQ.leadingAnchor.constraint(equalTo: sliderStackView.trailingAnchor)
        tableViewEQLeadingAnchor?.isActive = true
    }
    
    func sliderStackView200(){
        sliderStackViewWidthAnchor?.isActive = false
        sliderStackViewWidthAnchor = sliderStackView.widthAnchor.constraint(greaterThanOrEqualToConstant: greaterToConstantWhidthEQ)
        sliderStackViewWidthAnchor?.isActive = true
        
        sliderStackViewHeightAnchor?.isActive = false
        sliderStackViewHeightAnchor = sliderStackView.heightAnchor.constraint(equalToConstant: 200)
        sliderStackViewHeightAnchor?.isActive = true
        
        varsliderStackViewCenterXAnchor?.isActive = false
        varsliderStackViewCenterXAnchor = sliderStackView.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        varsliderStackViewCenterXAnchor?.isActive = true
        
        sliderStackViewLeadingAnchor?.isActive = false
        sliderStackViewLeadingAnchor = sliderStackView.leadingAnchor.constraint(greaterThanOrEqualTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 16)
        sliderStackViewLeadingAnchor?.isActive = true
        
        sliderStackViewTrailingAnchor?.isActive = false
        sliderStackViewTrailingAnchor = sliderStackView.trailingAnchor.constraint(lessThanOrEqualTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -16)
        sliderStackViewTrailingAnchor?.isActive = true
        
        sliderStackViewTrailingBottomAnchor?.isActive = false
    }
    
    func sliderStackViewLeft(){
        sliderStackViewWidthAnchor?.isActive = false
        sliderStackViewWidthAnchor = sliderStackView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.65)
        sliderStackViewWidthAnchor?.isActive = true
        
        sliderStackViewHeightAnchor?.isActive = false
        
        varsliderStackViewCenterXAnchor?.isActive = false
        
        sliderStackViewLeadingAnchor?.isActive = false
        sliderStackViewLeadingAnchor = sliderStackView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 16)
        sliderStackViewLeadingAnchor?.isActive = true
        
        sliderStackViewTrailingAnchor?.isActive = false
        
        sliderStackViewTrailingBottomAnchor?.isActive = false
        sliderStackViewTrailingBottomAnchor = sliderStackView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -16)
        sliderStackViewTrailingBottomAnchor?.isActive = true
    }
    
    
    
    //MARK:- Table Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayEQ.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath as IndexPath)
        cell.textLabel?.text = arrayEQ[indexPath.row]
        
        
        cell.accessoryType = .checkmark
        cell.tintColor = .tintSelectColor
        
        //tableView.separatorColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.2)
        cell.backgroundColor = .clear
        //cell.textLabel?.textColor = .lightGray
        
        cell.selectionStyle = .none
        
        if selectedIndex == indexPath.row{
            cell.accessoryType = .checkmark
            cell.textLabel?.textColor = .tintSelectColor
            //cell.imageView?.tintColor = .systemBlue
        }else{
            cell.accessoryType = .none
            cell.textLabel?.textColor = textColorSet//.lightGray
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
        selectedIndex = indexPath.row
        tableView.cellForRow(at: indexPath)?.textLabel?.textColor = .tintSelectColor
        UserDefaults.standard.set(arrayEQ[selectedIndex], forKey: "eqPreset")
        //self.navigationItem.leftBarButtonItem?.isEnabled = indexPath.row == 0 ? false : true
        
        setPresetEQ(indexPath)
        
        let totalSum = eqValueArray.map({$0}).reduce(0, +)
        print(totalSum)
        if totalSum == 0.0 {
            resetEqualizer.isEnabled = false
            return
        }
        resetEqualizer.isEnabled = indexPath.row == 0 ? true : false
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        tableView.cellForRow(at: indexPath)?.accessoryType = .none
        tableView.cellForRow(at: indexPath)?.textLabel?.textColor = textColorSet//.lightGray
    }
    
    func selectedPresetCell(){
        //self.navigationItem.leftBarButtonItem?.isEnabled = selectedIndex == 0 ? false : true
        
        
        let indexPath = IndexPath(row: selectedIndex, section: 0)
        tableViewEQ.selectRow(at: indexPath, animated: true, scrollPosition: .middle)
        
        let totalSum = eqValueArray.map({$0}).reduce(0, +)
        print(totalSum)
        if totalSum == 0.0 {
            resetEqualizer.isEnabled = false
            return
        }
        resetEqualizer.isEnabled = selectedIndex == 0 ? true : false
    }
    
    
    //MARK:- SET Preset EQ
    func setPresetEQ(_ indexPath: IndexPath){
        
        if indexPath.row > EQpresetArray.count - 1 {return}
        
        if indexPath.row == 0{
            if let EQArrayUserDefaults = UserDefaults.standard.value(forKey: "CUSTOMeqValueArray") as? Array<Float>{
                eqValueArray = EQArrayUserDefaults
                print("SET CUSTOM UserDefaults EQ")
            }else{
                eqValueArray = EQpresetArray[indexPath.row]
            }
        }else{
            eqValueArray = EQpresetArray[indexPath.row]
            print("SET preset in EQpresetArray")
        }
        
        UserDefaults.standard.set(eqValueArray, forKey: "eqValueArray")
        
        for (key, value) in eqValueArray.enumerated() {
            sliderArray[key].value = value
//            StreamKitPlayer.shared.audioPlayerSTK.setGain(value, forEqualizerBand: Int32(key))
//            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "setGain"), object: nil, userInfo: ["value": value, "forEqualizerBand": Int32(key)])
            delegate?.setGain(forEqualizerBand: Int32(key), value: value)
        }
    }
    
    
    
//    @objc func changeTextLabel (notification: NSNotification){
    func changeTextLabel (id: Int, value: Float){
//        let info = notification.userInfo as! [String: Any]
//        let value = info["value"] as! Float
//        let id = info["forEqualizerBand"] as! Int
        
        delegate?.setGain(forEqualizerBand: Int32(id), value: value)
        
        if selectedIndex != 0{
            selectedIndex = 0
            print(selectedIndex)
            UserDefaults.standard.set(arrayEQ[selectedIndex], forKey: "eqPreset")
            DispatchQueue.main.async {
                tableViewEQ.reloadData()
                self.selectedPresetCell()
            }
        }
        closeEqualizer.isEnabled = false
        
        if (timerLabel[id] != nil){
            timerLabel[id].invalidate()
            //print("invalidate")
        }
        
        arrayLabelTop[id].text = String(format: "%.2f", value)
        arrayLabelTop[id].textColor = .tintSelectColor
        
        timerLabel[id] = Timer.scheduledTimer(withTimeInterval: 0.5, repeats: false) { timer in
            arrayLabelTop[id].text = self.arraySliderText[id]
            arrayLabelTop[id].textColor = textColorSet
            //print("TEMER RUN: ", id)
            eqValueArray[id] = value
            UserDefaults.standard.set(eqValueArray, forKey: "eqValueArray")
            UserDefaults.standard.set(eqValueArray, forKey: "CUSTOMeqValueArray")
            resetEqualizer.isEnabled = true
            
            Timer.scheduledTimer(withTimeInterval: 1.0, repeats: false) {_ in
                closeEqualizer.isEnabled = true
            }
        }
        
        
    }
    
    func resetLabel(_ id: Int){
        print("TEMER RUN")
    }
    
}

