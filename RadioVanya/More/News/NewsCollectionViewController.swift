//
//  NewsCollectionViewController.swift
//  RadioVanya
//
//  Created by Александр Краснощеков on 13.10.2020.
//  Copyright © 2020 Александр Краснощеков. All rights reserved.
//

import UIKit

protocol NewsCollectionDelegate {
    func selectedNewsCell(_ news: News)
}

class NewsCollectionViewController: HorizontalSnappingController, UICollectionViewDelegateFlowLayout {

    var newsArray = [News]() {
        didSet{
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
        }
    }
    
    private let reuseIdentifier = "Cell"
    
    var delegate: NewsCollectionDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.layer.masksToBounds = false
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Register cell classes
        collectionView.register(NewsCell.self, forCellWithReuseIdentifier: reuseIdentifier)
        
        collectionView?.backgroundColor = .clear
        collectionView?.showsHorizontalScrollIndicator = false
        collectionView?.contentInset = .init(top: 0, left: 16, bottom: 0, right: 16)
        
        //newsJSON()
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return newsArray.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! NewsCell
        //cell.backgroundColor = .init(white: 0.9, alpha: 0.3)
        
        let news = newsArray[indexPath.item]
        cell.news = news

//        if !cell.isAnimated {
//
//            UIView.animate(withDuration: 0.5, delay: 0.5 * Double(indexPath.row), usingSpringWithDamping: 1, initialSpringVelocity: 0.5, options:  .transitionFlipFromLeft, animations: {
//                AnimationUtility.viewSlideInFromRight(toLeft: cell)
//            }, completion: { (done) in
//                cell.isAnimated = true
//            })
//        }
        
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let news = newsArray[indexPath.item]
        delegate?.selectedNewsCell(news)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 16
    }
    
    //MARK:- UICollectionViewDelegateFlowLayout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width: CGFloat = (view.frame.width / 2) - 32
        return .init(width: width, height: view.frame.height)
    }
}
