//
//  NewsCell.swift
//  RadioVanya
//
//  Created by Александр Краснощеков on 13.10.2020.
//  Copyright © 2020 Александр Краснощеков. All rights reserved.
//

import UIKit

class NewsCell: UICollectionViewCell {
    
    var news: News? {
        didSet{
            if let news = news {
                if let imageString = news.annotation_image, let imgUrl = URL(string: imageString) {
                    self.newsImageView.sd_setImage(with: imgUrl, completed: nil)
                }
                
                self.newsNameLabel.text = news.name
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ss"//2019-10-03T16:27:46.027Z
                //let dateFormatter = ISO8601DateFormatter()
                if let isoDate = news.created_at {
                    var dataString = isoDate
                    dataString.removeLast(5)
                    if let date = dateFormatter.date(from: dataString) {
                        self.newsDataLabel.text = date.toString()
                    }else{
                        self.newsDataLabel.text = nil
                    }
                }else{
                    self.newsDataLabel.text = nil
                }
            }
        }
    }
    
    var isAnimated: Bool = false
    
    let wrapperStackView: UIStackView = {
        let sv = UIStackView()
        sv.axis = .vertical
        sv.translatesAutoresizingMaskIntoConstraints = false
        return sv
    }()
    
    let newsNameLabel: UILabel = {
        let label = UILabel()
        label.textColor = .label//.black
        label.numberOfLines = 2
        label.font = .systemFont(ofSize: 14)
        return label
    }()
    
    let newsImageView: UIImageView = {
        let iv = UIImageView()
        
        iv.contentMode = .scaleAspectFill
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    let newsDataLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 12)
        label.textColor = .secondaryLabel//.lightGray
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        //self.backgroundColor = .white
        self.backgroundView = UIView()
        self.backgroundView?.backgroundColor = .clear
        //addSubview(self.backgroundView!)
        insertSubview(self.backgroundView!, belowSubview: contentView)
        self.backgroundView?.translatesAutoresizingMaskIntoConstraints = false
        self.backgroundView?.topAnchor.constraint(equalTo: topAnchor).isActive = true
        self.backgroundView?.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        self.backgroundView?.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        self.backgroundView?.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        
        
        self.backgroundView?.layer.shadowOffset = CGSize(width: -3.0, height: 3.0)
        self.backgroundView?.layer.shadowRadius = 6.0//6.0
        self.backgroundView?.layer.shadowOpacity = 0.3
        self.backgroundView?.layer.masksToBounds = false
        self.backgroundView?.layer.shadowPath = UIBezierPath(roundedRect: self.backgroundView?.bounds ?? .zero, cornerRadius: 12).cgPath
        self.backgroundView?.layer.backgroundColor = UIColor.clear.cgColor
        self.backgroundView?.layer.shouldRasterize = true
        self.backgroundView?.layer.rasterizationScale = UIScreen.main.scale
        
        setupViews()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.backgroundView?.layer.shadowColor = UIColor(named: "shadow")?.cgColor//lightGray //secondarySystemBackground
    }
    
    func setupViews() {
        contentView.layer.cornerRadius = 12
        contentView.layer.masksToBounds = true
        contentView.backgroundColor = .secondarySystemBackground//#colorLiteral(red: 0.9685427547, green: 0.9686817527, blue: 0.9685124755, alpha: 1)
        contentView.addSubview(newsDataLabel)
        newsDataLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -8).isActive = true
        newsDataLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16).isActive = true
        newsDataLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16).isActive = true
        //newsDataLabel.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 1, constant: -32).isActive = true
        
        contentView.addSubview(wrapperStackView)
        wrapperStackView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        wrapperStackView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        wrapperStackView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        wrapperStackView.bottomAnchor.constraint(equalTo: newsDataLabel.topAnchor, constant: -8).isActive = true
        
        let viewPaddin = UIView()
        viewPaddin.translatesAutoresizingMaskIntoConstraints = false
        
        let newsNameLabelStackView = UIStackView()
        newsNameLabelStackView.axis = .vertical
        newsNameLabelStackView.addArrangedSubview(newsNameLabel)
        newsNameLabelStackView.layoutMargins = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
        newsNameLabelStackView.isLayoutMarginsRelativeArrangement = true
        [newsImageView, viewPaddin, newsNameLabelStackView, UIView()].forEach {wrapperStackView.addArrangedSubview($0)}
        
        newsImageView.heightAnchor.constraint(equalTo: newsImageView.widthAnchor, multiplier: 0.75).isActive = true
        viewPaddin.heightAnchor.constraint(equalToConstant: 8).isActive = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
