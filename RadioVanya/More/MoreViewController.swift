//
//  MoreViewController.swift
//  RadioVanya
//
//  Created by Александр Краснощеков on 13.10.2020.
//  Copyright © 2020 Александр Краснощеков. All rights reserved.
//

import UIKit

extension MoreViewController: NewsCollectionDelegate{
    func selectedNewsCell(_ news: News) {
        print(news.name ?? "")
        let webViewController = WebViewController()
        let viewController = UINavigationController(rootViewController: webViewController)
        webViewController.news = news
        self.navigationController?.present(viewController, animated: true, completion: {
            
        })
    }
}

extension MoreViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return nameArrayTableCell.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return nameArrayTableCell[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 && indexPath.row == 1{
            return tableViewTimerCell(tableView, indexPath)
        }else {
           return tableViewCell(tableView, indexPath)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        didSelectRowAt(indexPath)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 && indexPath.row == 1 {
            return heightCellTimer// UITableView.automaticDimension
        } else {
            return 44
        }
    }
}

class MoreViewController: UIViewController {
    
    let newsController = NewsCollectionViewController()
    let equalizer = EQualizer()
    
    var openTimer: Bool = false
    var heightCellTimer: CGFloat = 44//206
    
    var nameArrayTableCell = [
        ["Эквалайзер", "Таймер сна"],
        ["Города", "По заявкам", "Контакты"],
        ["Вконтакте", "Инстаграм"],
        ["Оставить отзыв", "Поделиться приложением"]
    ]
    
    let tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .insetGrouped)// grouped
        tableView.backgroundColor = .systemBackground//.white
        tableView.translatesAutoresizingMaskIntoConstraints = false
        return tableView
    }()
    
    private let cellId = "cellId"
    private let cellIdTimerCell = "cellIdTimerCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .systemBackground//.white
        
        newsController.delegate = self
        
        setupNavBar()
        
        setupTableView()
        
        headerNewsCollectionView()
    }
    
    func setupNavBar() {
        let titleLabel = UILabel()
        titleLabel.text = "ЕЩЕ"
        titleLabel.font = UIFont.init(name: "BebasNeueBold", size: 24)
        self.navigationItem.titleView = titleLabel
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        deselectRow()
    }
    
    private func tableViewTimerCell(_ tableView: UITableView, _ indexPath: IndexPath) -> UITableViewCell {
        
        let name = nameArrayTableCell[indexPath.section][indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdTimerCell, for: indexPath) as! TimerCell
        
        cell.titleLabel.text = name
        //cell.textLabel?.isHidden = true
        
        cell.textLabel?.text = " "
        
        cell.backgroundColor = .secondarySystemBackground//.init(white: 0.9, alpha: 0.3)
        
        cell.accessoryType = openTimer ? .none : .disclosureIndicator
        
        cell.timerDatePickerHeigth?.constant = 162//openTimer ? 0 : 162
        
        cell.senderDatePicker = { (sender) in
            self.handlerTimerDatePicker(sender)
        }//timerDatePicker.addTarget(self, action: #selector(handlerTimerDatePicker), for: .valueChanged)
        
//        let selectedBGView: UIView = UIView(frame: cell.bounds)
//        selectedBGView.backgroundColor = .systemGray5//.tertiarySystemBackground//.init(white: 0.8, alpha: 0.3)//UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 1.0)
//        cell.selectedBackgroundView = selectedBGView
        
        return cell
    }
    
    private func tableViewCell(_ tableView: UITableView, _ indexPath: IndexPath) -> UITableViewCell {
        let name = nameArrayTableCell[indexPath.section][indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath)
        
        cell.textLabel?.text = name
        cell.backgroundColor = .secondarySystemBackground//.init(white: 0.9, alpha: 0.3)
        
        cell.accessoryType = .disclosureIndicator
        
//        let selectedBGView: UIView = UIView(frame: cell.bounds)
//        selectedBGView.backgroundColor = .systemGray5//.tertiarySystemBackground//.init(white: 0.8, alpha: 0.3)//UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 1.0)
//        cell.selectedBackgroundView = selectedBGView
        
        return cell
    }
    
    private func didSelectRowAt(_ indexPath: IndexPath){
        if indexPath.section == 0 {
            switch indexPath.row {
            case 0:
                pushViewController(viewController: equalizer)
            case 1:
                selectedTimerCell()
            default:
                print("default")
            }
        }else if indexPath.section == 1 {
            switch indexPath.row {
            case 0:
                pushViewController(viewController: CitysViewController())
            case 1:
                pushViewController(viewController: ProgramViewController())
            case 2:
                pushViewController(viewController: СontactsViewController())
            default:
                print("default")
            }
        }else if indexPath.section == 2 {
            switch indexPath.row {
            case 0:
                openSocialLink(Social.vk)
            case 1:
                openSocialLink(Social.instagram)
            default:
                print("default")
            }
        }else if indexPath.section == 3 {
            switch indexPath.row {
            case 0:
                openSocialLink(Social.ReviewAppStore)
            case 1:
                shareApp()
            default:
                print("default")
            }
        }
    }
    
    private func openSocialLink(_ urlString: String){
        deselectRow()
        Request.openUrl(urlString: urlString)
    }
    
    private func shareApp(){
        if let shareUrl = URL(string: Social.AppStore){
            let text = "Я слушаю Радио ВАНЯ в мобильном приложении. Присоединяйся и ты!"
            let vc = UIActivityViewController(activityItems: [text, shareUrl], applicationActivities: [])
            present(vc, animated: true, completion: nil)
        }
    }

    
    private func deselectRow() {
        if let indexPath = tableView.indexPathForSelectedRow {
            tableView.deselectRow(at: indexPath, animated: true)
        }
    }
    
    private func selectedTimerCell() {
        openTimer = !openTimer
        
        tableView.beginUpdates()
            heightCellTimer = openTimer ? 206 : 44
        tableView.endUpdates()
        
        //if !openTimer  {
            deselectRow()
        //}
        
        let indexPath: IndexPath = .init(row: 1, section: 0)
        if let cell = tableView.cellForRow(at: indexPath) as? TimerCell {
            cell.accessoryType = openTimer ? .none : .disclosureIndicator
        }
    }
    
    private func setupTableView(){
        
        tableView.delegate = self
        tableView.dataSource = self
        
        view.addSubview(tableView)
        tableView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: cellId)
        tableView.register(TimerCell.self, forCellReuseIdentifier: cellIdTimerCell)
    }
    
    func headerNewsCollectionView(){
        jsonVideoLoad(completion: { (bool) in
            if bool {
                let height: CGFloat = (((self.view.frame.width / 2 ) - 32 ) * 0.75) + 146//128
                //print(height)
                let tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: height))
                
                let titleLabel = UILabel()
                titleLabel.text = "ВЕСЕЛЫЕ НОВОСТИ"
                titleLabel.font = UIFont.init(name: "BebasNeueBold", size: 18)
                titleLabel.textColor = .label//.black
                titleLabel.translatesAutoresizingMaskIntoConstraints = false

                tableHeaderView.addSubview(titleLabel)
                titleLabel.topAnchor.constraint(equalTo: tableHeaderView.topAnchor, constant: 16).isActive = true
                titleLabel.leadingAnchor.constraint(equalTo: tableHeaderView.leadingAnchor, constant: 32).isActive = true
                //titleLabel.trailingAnchor.constraint(equalTo: tableHeaderView.trailingAnchor).isActive = true
                
                tableHeaderView.addSubview(self.newsController.view)
                self.newsController.view.translatesAutoresizingMaskIntoConstraints = false
                self.newsController.view.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 8).isActive = true
                self.newsController.view.leadingAnchor.constraint(equalTo: tableHeaderView.leadingAnchor).isActive = true
                self.newsController.view.trailingAnchor.constraint(equalTo: tableHeaderView.trailingAnchor).isActive = true
                self.newsController.view.bottomAnchor.constraint(equalTo: tableHeaderView.bottomAnchor, constant: -32).isActive = true
                
                self.tableView.tableHeaderView = tableHeaderView
            }
        })
    }
    
    private func jsonVideoLoad(completion: @escaping (Bool) -> Void){
        loadingHeaderView()
        let timestamp = NSDate().timeIntervalSince1970
        let urlString = "https://dev.radiovanya.ru/api/v1/news/?\(timestamp)"
        guard let url = URL(string: urlString) else { return }
        URLSession.shared.dataTask(with: url) { (data, _, err) in
            DispatchQueue.main.async {
                if let err = err {
                    print("Failed to get data from url:", err)
                    self.notLoadTryAgain()
                    completion(false)
                    return
                }
                
                guard let data = data else { completion(false); return } //self.notLoadTryAgain();
                
                do {
                    let news = try JSONDecoder().decode([News].self, from: data)
                    DispatchQueue.main.async() {
                        var arrayNews: [News] = news
                        arrayNews.removeFirst(2)
                        self.newsController.newsArray = arrayNews//news
                        completion(true)
                        //self.loadingGood()
                    }
                } catch let jsonErr {
                    print("Failed to decode:", jsonErr)
                    completion(false)
                    self.notLoadTryAgain()
                }
            }
        }.resume()
    }
    
    func loadingHeaderView() {
        let wrapperView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 44))
        let spinnerIndicatorView = UIActivityIndicatorView()
        spinnerIndicatorView.style = .medium
        spinnerIndicatorView.tintColor = .lightGray
        spinnerIndicatorView.translatesAutoresizingMaskIntoConstraints = false
        wrapperView.addSubview(spinnerIndicatorView)
        spinnerIndicatorView.centerXAnchor.constraint(equalTo: wrapperView.centerXAnchor).isActive = true
        spinnerIndicatorView.centerYAnchor.constraint(equalTo: wrapperView.centerYAnchor).isActive = true
        spinnerIndicatorView.startAnimating()
        
        self.tableView.tableHeaderView = wrapperView
    }
    
    func notLoadTryAgain() {
        self.tableView.tableHeaderView = UIView()
    }
    
    @objc func handlerTimerDatePicker(_ sender: UIDatePicker) {
        let date = sender.date
        let components = Calendar.current.dateComponents([.hour, .minute], from: date)
        let hour = components.hour!
        let minute = components.minute!
        let seconds = 0
        print("hour: ", hour, " minute: ", minute)
        
        let indexPath = IndexPath.init(row: 1, section: 0)
        if let cell = tableView.cellForRow(at: indexPath) as? TimerCell {
            cell.timerSwitcher.isHidden = false
            //cell.timerLabelCountdown.isHidden = false
            //cell.timerLabelCountdown.text = String(format:"%02i:%02i:%02i", hour, minute, seconds)
            cell.timerLabel.text = String(format:"%02i:%02i:%02i", hour, minute, seconds)
            //secodsTimer = (hour * 3600) + (minute * 60)
        }
    }
    
    //MARK:- HANDLER TABLE VIEW SELECTED
    private func pushViewController(viewController: UIViewController) {
        navigationController?.pushViewController(viewController, animated: true)
    }
}
