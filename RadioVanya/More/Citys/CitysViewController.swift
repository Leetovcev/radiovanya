//
//  CitysViewController.swift
//  RadioVanya
//
//  Created by Александр Краснощеков on 14.10.2020.
//  Copyright © 2020 Александр Краснощеков. All rights reserved.
//

import UIKit

extension CitysViewController: UITableViewDelegate, UITableViewDataSource {
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return cityArray.count
//    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sectionsCity[section].citys.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath)
        let cell = UITableViewCell(style: .value1, reuseIdentifier: cellId)
        cell.backgroundColor = .clear
        //let city = cityArray[indexPath.row]
        
        let section = sectionsCity[indexPath.section]
        let city = section.citys[indexPath.row]
        
        cell.textLabel?.text = city.city
        cell.detailTextLabel?.text = city.freq
        
        let selectedBGView: UIView = UIView(frame: cell.bounds)
        selectedBGView.backgroundColor = .init(white: 0.8, alpha: 0.3)
        cell.selectedBackgroundView = selectedBGView
        
        return cell
    }
    
    func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        return sectionsCity.map{$0.letter}
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sectionsCity[section].letter
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sectionsCity.count
    }
    
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        let label = UILabel.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.size.width, height: 30))
//        //label.textColor = UIColor.red
//        label.font = UIFont.boldSystemFont(ofSize: 18)
//        label.text = sectionsCity[section].letter
//        return label
//    }
}

class CitysViewController: UIViewController {
    
    let tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .insetGrouped)//grouped
        tableView.backgroundColor = .systemBackground//.white
        tableView.sectionIndexColor = .tintSelectColor
        tableView.translatesAutoresizingMaskIntoConstraints = false
        return tableView
    }()
    
    private let cellId = "cellId"
    
    //var cityArray = [cityResult]()
    var sectionsCity = [SectionCity]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNavBar()
        
        setupTableView()
        
        jsonCitysLoad()
    }
    
    func setupNavBar() {
        let titleLabel = UILabel()
        titleLabel.text = "ГОРОДА ВЕЩАНИЯ"
        titleLabel.font = UIFont.init(name: "BebasNeueBold", size: 24)
        self.navigationItem.titleView = titleLabel
        self.navigationController?.navigationBar.tintColor = .tintSelectColor
    }
    
    private func setupTableView(){
        
        tableView.delegate = self
        tableView.dataSource = self
        
        view.addSubview(tableView)
        tableView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: cellId)
        
        tableView.allowsSelection = false
    }
    
    private func jsonCitysLoad(){
        let timestamp = NSDate().timeIntervalSince1970
        let urlString = "http://www.radiorecord63.ru/vanya/api/citys/?\(timestamp)"
        guard let url = URL(string: urlString) else { return }
        URLSession.shared.dataTask(with: url) { (data, _, err) in
            DispatchQueue.main.async {
                if let err = err {
                    print("Failed to get data from url:", err)
                    //self.notLoadTryAgain()
                    return
                }
                
                guard let data = data else { return } //self.notLoadTryAgain();
                
                do {
                    let citys = try JSONDecoder().decode(Citys.self, from: data)
                    DispatchQueue.main.async() {
                        //self.cityArray = citys.result
                        
                        let groupedDictionary = Dictionary(grouping: citys.result, by: {String($0.city?.prefix(1) ?? "")})
                        // get the keys and sort them
                        let keys = groupedDictionary.keys.sorted()
                        // map the sorted keys to a struct
                        self.sectionsCity = keys.map{ SectionCity(letter: $0, citys: groupedDictionary[$0] ?? []) }

                        self.tableView.reloadData()
                        //self.loadingGood()
                    }
                } catch let jsonErr {
                    print("Failed to decode:", jsonErr)
                    //self.notLoadTryAgain()
                }
            }
        }.resume()
    }
}
