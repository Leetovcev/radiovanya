//
//  СontactsМшуцСщтекщддук.swift
//  RadioVanya
//
//  Created by Александр Краснощеков on 14.10.2020.
//  Copyright © 2020 Александр Краснощеков. All rights reserved.
//

import UIKit
import CoreLocation

extension СontactsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contactArray[section].contact.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return contactArray.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 46))
        
        
        let titleLabel = UILabel()
        titleLabel.text = contactArray[section].title
        titleLabel.textColor = .label
        titleLabel.font = UIFont.init(name: "BebasNeueBold", size: 18)
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        
        headerView.addSubview(titleLabel)
        
        titleLabel.topAnchor.constraint(equalTo: headerView.topAnchor, constant: 8).isActive = true
        titleLabel.leadingAnchor.constraint(equalTo: headerView.leadingAnchor, constant: 20).isActive = true
        titleLabel.trailingAnchor.constraint(equalTo: headerView.trailingAnchor).isActive = true
        titleLabel.bottomAnchor.constraint(equalTo: headerView.bottomAnchor, constant: -8).isActive = true
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .value1, reuseIdentifier: cellId)
        
        cell.backgroundColor = .secondarySystemBackground
        
        let contact = contactArray[indexPath.section].contact[indexPath.row]
        
        cell.imageView?.image = contact.img
        cell.imageView?.tintColor = .tintSelectColor
        cell.textLabel?.text = contact.title
        cell.textLabel?.numberOfLines = 2
        cell.detailTextLabel?.text = contact.detailTitle

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = contactArray[indexPath.section].contact[indexPath.row]
        didSelectRowAt(item)
    }
}

class СontactsViewController: UIViewController {
    
    let tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .insetGrouped)//grouped
        tableView.backgroundColor = .clear
        tableView.translatesAutoresizingMaskIntoConstraints = false
        return tableView
    }()
    
    private let cellId = "cellId"
    
    var contactArray = [Contacts]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .systemBackground
        
        setupNavBar()
        
        contactArraySetup()
        
        setupTableView()
    }
    
    func setupNavBar() {
        let titleLabel = UILabel()
        titleLabel.text = "КОНТАКТЫ"
        titleLabel.font = UIFont.init(name: "BebasNeueBold", size: 24)
        self.navigationItem.titleView = titleLabel
        self.navigationController?.navigationBar.tintColor = .tintSelectColor
    }
    
    private func setupTableView(){
        
        tableView.delegate = self
        tableView.dataSource = self
        
        view.addSubview(tableView)
        tableView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: cellId)//ContactCell.self
        
        tableView.contentInset.top = 16
    }
    
    private func contactArraySetup(){
        let firstSt = Contact.init(img: UIImage(systemName: "phone.fill"), title: "8 (812) 407-33-51", detailTitle: nil, type: .telephone)
        let secondSt = Contact.init(img: UIImage(systemName: "envelope.fill"), title: "reklama@m10m.ru", detailTitle: nil, type: .email)
        
        let contactSt = Contacts.init(title: "Отдел рекламы в Санкт-Петербурге", contact: [firstSt, secondSt])
        
        let firstSecretary = Contact.init(img: UIImage(systemName: "phone.fill"), title: "8 (812) 325-68-06", detailTitle: nil, type: .telephone)
        let contactSecretary = Contacts.init(title: "Секретарь", contact: [firstSecretary] )
        
        let firstAddress = Contact.init(img: UIImage(systemName: "location.fill"), title: "Россия, г. Санкт-Петербург,\nул. Шевченко, 27, 199406", detailTitle: nil, type: .adress)
        let contactAddress = Contacts.init(title: "Адрес", contact: [firstAddress])
        
        //Региональный отдел
        let firstReg = Contact.init(img: UIImage(systemName: "phone.fill"), title: "8 (812) 325-68-06 доб. 145", detailTitle: nil, type: .telephone)
        let secondReg = Contact.init(img: UIImage(systemName: "envelope.fill"), title: "region@m10m.ru", detailTitle: nil, type: .email)
        let contactReg = Contacts.init(title: "Региональный отдел", contact: [firstReg, secondReg])
        
        //
        let samaraPhone = Contact.init(img: nil, title: "Самара", detailTitle: "8 (846) 334-14-40", type: .telephone)
        let tltPhone = Contact.init(img: nil, title: "Тольятти", detailTitle: "8 (8482) 60-10-27", type: .telephone)
        let murmanskPhone = Contact.init(img: nil, title: "Мурманск", detailTitle: "8 (8152) 20-57-11", type: .telephone)
        let saratovPhone = Contact.init(img: nil, title: "Саратов", detailTitle: "8 (8452) 77-89-89", type: .telephone)
        let ijevskPhone = Contact.init(img: nil, title: "Ижевск", detailTitle: "8 (3412) 65-59-05", type: .telephone)
        let kemerovoPhone = Contact.init(img: nil, title: "Кемерово", detailTitle: "8 (3842) 36-00-63", type: .telephone)
        let kurganPhone = Contact.init(img: nil, title: "Курган", detailTitle: "8 (3522) 45-95-70", type: .telephone)
        let novoPhone = Contact.init(img: nil, title: "Новокузнецк", detailTitle: "8 (3843) 77-78-79", type: .telephone)
        let omskPhone = Contact.init(img: nil, title: "Омск", detailTitle: "8 (3812) 53-42-80", type: .telephone)
        
        let contactRegTel = Contacts.init(title: "отделы рекламы в регионах", contact: [samaraPhone, tltPhone, murmanskPhone, saratovPhone, ijevskPhone, kemerovoPhone, kurganPhone, novoPhone, omskPhone])
        
        [contactSt, contactSecretary, contactAddress, contactReg, contactRegTel].forEach{contactArray.append($0)}
    }
    
    private func didSelectRowAt(_ item: Contact){
        if let type = item.type {
            if type == .adress {
                if let address = item.title {
                    let geoCoder = CLGeocoder()
                    geoCoder.geocodeAddressString(address) { (placemarks, error) in
                        guard let placemarks = placemarks?.first else { return }
                        let location = placemarks.location?.coordinate ?? CLLocationCoordinate2D()
                        guard let url = URL(string:"http://maps.apple.com/?daddr=\(location.latitude),\(location.longitude)") else { return }
                        UIApplication.shared.open(url, options: [:]) { (_ ) in }
                    }
                }
            }else{
                let string = (item.img == nil) ? item.detailTitle : item.title
                if let string = string {
                    let dictionary = ["+": "", " ": "", "(": "", ")": "", "-": "", "доб. 145": ""]
                    let mobileResult = string.replace(dictionary)
                    let urlString = "\(type.rawValue)\(mobileResult)"
                    if let url = URL(string: urlString) {
                        UIApplication.shared.open(url, options: [:]) { (_ ) in }
                    }
                }
            }
        }
        deselectRow()
    }
    
    private func deselectRow() {
        if let indexPath = tableView.indexPathForSelectedRow {
            tableView.deselectRow(at: indexPath, animated: true)
        }
    }
}




