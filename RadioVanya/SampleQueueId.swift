//
//  SampleQueueId.swift
//  RadioVanya
//
//  Created by Александр Краснощеков on 11.10.2020.
//  Copyright © 2020 Александр Краснощеков. All rights reserved.
//

import Foundation

class SampleQueueId: NSObject {
    var count = 0
    var url: URL?
    
    init(url: URL?, andCount count: Int) {
        super.init()
            self.url = url
            self.count = count
    }

    override func isEqual(_ object: Any?) -> Bool {
        if object == nil {
            return false
        }

        if let _ = object as? SampleQueueId {
            return false
        }

        return ((object as? SampleQueueId)?.url) == url && (object as? SampleQueueId)?.count == count
    }

    func description() -> String? {
        return self.url?.description
    }
}
