//
//  Videos.swift
//  RadioVanya
//
//  Created by Александр Краснощеков on 12.02.2021.
//  Copyright © 2021 Александр Краснощеков. All rights reserved.
//

import Foundation
import UIKit
import WebKit

extension WebViewController: WKUIDelegate {
    
}

extension WebViewController: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
        startLoadinView()
    }

    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        loadingGood()
    }
}

class WebViewController: UIViewController {
    
    var video: Videos? {
        didSet{
            if let video = video {
                if let videoUrlString = video.youtube_link, let videoUrl = URL(string: videoUrlString) {
                    let myRequest = URLRequest(url: videoUrl)
                    webView.load(myRequest)
                }
                self.title = video.name
            }
        }
    }
    
    var news: News? {
        didSet{
            if let news = news {
                if let newsBody = news.body {
                    webView.loadHTMLString(newsBody, baseURL: nil)
                }
                if let newsName = news.name {
                    self.title = newsName
                }
            }
        }
    }
    
    let progressView: UIProgressView = {
        let pv = UIProgressView()
        pv.tintColor = .tintSelectColor
        pv.translatesAutoresizingMaskIntoConstraints = false
        return pv
    }()
    
    private let spinnerIndicatorView: UIActivityIndicatorView = {
        let iv = UIActivityIndicatorView()
        iv.style = .large
        iv.tintColor = .lightGray
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    lazy var webView: WKWebView = {
        let webConfiguration = WKWebViewConfiguration()
        let webView = WKWebView(frame: .zero, configuration: webConfiguration)
        
        webView.isOpaque = false
        webView.backgroundColor = .clear
        webView.scrollView.backgroundColor = .clear
        webView.uiDelegate = self
        webView.navigationDelegate = self
        webView.translatesAutoresizingMaskIntoConstraints = false
        return webView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .systemBackground
        
        setupLoadinView()
        
        setupWebView()
        
        setupProgressView()
    }
    
    private func setupProgressView(){
        view.addSubview(progressView)
        NSLayoutConstraint.activate([
            progressView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor),
            progressView.leftAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leftAnchor),
            progressView.rightAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.rightAnchor),
            progressView.heightAnchor.constraint(equalToConstant: 2)
        ])
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    private func setupLoadinView() {
        view.addSubview(spinnerIndicatorView)
        spinnerIndicatorView.centerXAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerXAnchor).isActive = true
        spinnerIndicatorView.centerYAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerYAnchor).isActive = true
    }
    
    
    private func setupWebView(){
        
        view.addSubview(webView)
        NSLayoutConstraint.activate([
            webView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            webView.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor),
            webView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
            webView.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor)
        ])
        
        webView.addObserver(self, forKeyPath: #keyPath(WKWebView.estimatedProgress), options: .new, context: nil)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "estimatedProgress" {
            progressViewHandler()
        }
    }
    
    private func progressViewHandler() {
        let progress: Float = Float(webView.estimatedProgress)
        UIView.animate(withDuration: 0.25) {
            self.progressView.setProgress(progress, animated: true)
        }
        
        if progress >= 0.99 {
            hideProgressView()
        }
    }
    
    private func startLoadinView() {
        spinnerIndicatorView.startAnimating()
    }
    
    private func notLoadTryAgain() {
        spinnerIndicatorView.stopAnimating()
    }
    
    private func loadingGood() {
        spinnerIndicatorView.stopAnimating()
        hideProgressView()
    }
    
    private func hideProgressView(){
        UIView.animate(withDuration: 0.25, delay: 0) {
            self.progressView.setProgress(1, animated: true)
        } completion: { (_ ) in
            self.progressView.isHidden = true
        }
    }
    
}
