//
//  EditMoreTableCell.swift
//  RadioVanya
//
//  Created by Александр Краснощеков on 03.04.2021.
//  Copyright © 2021 Александр Краснощеков. All rights reserved.
//

import UIKit

class EditMoreTableCell: UITableViewCell {
        
    let imageViewCell: UIImageView = {
        let iv = UIImageView()
        iv.tintColor = .label
        iv.contentMode = .scaleAspectFit
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = .label
        label.font = UIFont.systemFont(ofSize: 16)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let wrapperStackView: UIStackView = {
        let sv = UIStackView()
        sv.translatesAutoresizingMaskIntoConstraints = false
        return sv
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        separatorInset.left = 0
        backgroundColor = .secondarySystemBackground
        
        setupViews()
    }
    
    private func setupViews() {
        contentView.addSubview(wrapperStackView)
        NSLayoutConstraint.activate([
            wrapperStackView.topAnchor.constraint(equalTo: contentView.topAnchor),
            wrapperStackView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16),
            wrapperStackView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            wrapperStackView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
        ])
        
        let imageViewWrapperView = UIView()
        imageViewWrapperView.translatesAutoresizingMaskIntoConstraints = false
        [titleLabel, imageViewWrapperView].forEach {wrapperStackView.addArrangedSubview($0)}
        imageViewWrapperView.widthAnchor.constraint(equalTo: imageViewWrapperView.heightAnchor, multiplier: 1).isActive = true
        imageViewWrapperView.addSubview(imageViewCell)
        imageViewCell.centerXAnchor.constraint(equalTo: imageViewWrapperView.centerXAnchor).isActive = true
        imageViewCell.centerYAnchor.constraint(equalTo: imageViewWrapperView.centerYAnchor).isActive = true
        imageViewCell.widthAnchor.constraint(equalToConstant: 24).isActive = true
//        imageViewCell.heightAnchor.constraint(equalTo: imageViewCell.widthAnchor, multiplier: 1).isActive = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
