//
//  EditActionController.swift
//  RadioVanya
//
//  Created by Александр Краснощеков on 03.04.2021.
//  Copyright © 2021 Александр Краснощеков. All rights reserved.
//

import UIKit

protocol EditActionControllerDelegate  {
    func changeItemCount()
}

extension EditActionController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return heightForRowAt
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return section == 1 ? item[section].count > 0 ? titleForSectionString : nil : nil
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
//    func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
//        return "Erase"
//    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        let defaults = UserDefaults.standard
        let actionMusicService = defaults.stringArray(forKey: "actionMusicService") ?? [String]()

        if editingStyle == .delete {

            // remove the item from the data model
            //tableView.beginUpdates()
            isReload = true
            let removeItem: moreTableItem = item[indexPath.section][indexPath.row]
            item[indexPath.section].remove(at: indexPath.row)
//            tableView.deleteRows(at: [indexPath], with: .automatic)
            
            if item.count > 1 {
                item[indexPath.section + 1].insert(removeItem, at: 0)
            }else{
                item.insert([removeItem], at: indexPath.section + 1)
            }
            
            tableView.performUpdate({
                tableView.moveRow(at: indexPath, to: IndexPath.init(row: 0, section: indexPath.section + 1))
            }, completion: {
                tableView.reloadData()
            })

//            item[indexPath.section + 1].insert(element, at: 0)
//            tableView.insertRows(at: [IndexPath.init(row: indexPath.section + 1, section: 0)], with: .automatic)
//            item.insert([item[indexPath.section][indexPath.row]], at: 0)
            //tableView.endUpdates()
            
            let newArray = actionMusicService.filter { (elem) -> Bool in
                elem != removeItem.type?.rawValue
            }
            
            defaults.set(newArray, forKey: "actionMusicService")
            delegate?.changeItemCount()
            
            
//            tableView.beginUpdates()
//            if item.count - 1 >= indexPath.section + 1 {
//                if item[indexPath.section + 1].count > 0 {
//                    tableView.headerView(forSection: indexPath.section + 1)?.textLabel?.text = titleForSectionString
//                }
//            }
//            tableView.endUpdates()
            
            
//            isReload = false
//            tableView.reloadData()
            
            //self.item = [item[indexPath.section],[removeItem]]
            //print(item[indexPath.section][indexPath.row])

        } else if editingStyle == .insert {
            // Not used in our example, but if you were adding a new row, this is where you would do it.
            //tableView.beginUpdates()
            isReload = true
            let removeItem: moreTableItem = item[indexPath.section][indexPath.row]
            item[indexPath.section].remove(at: indexPath.row)
            //tableView.deleteRows(at: [indexPath], with: .automatic)
            
            var indexRow: Int = 0
            
            if indexPath.section > 0 {
                if item[indexPath.section - 1].count > 0 {
                    indexRow = item[indexPath.section - 1].count
                }
            }
        
            
            item[indexPath.section - 1].insert(removeItem, at: indexRow)//append(removeItem)
            //tableView.insertRows(at: [IndexPath.init(row: 0, section: 0)], with: .automatic)

            if let newElement = removeItem.type?.rawValue {
                var newArray = actionMusicService
                newArray.append(newElement)
                defaults.set(newArray, forKey: "actionMusicService")
                
                delegate?.changeItemCount()
            }
            
            let indexMove = IndexPath.init(row: indexRow, section: 0)
            tableView.performUpdate({
                tableView.moveRow(at: indexPath, to: indexMove)
            }, completion: {
                tableView.reloadData()
            })
            
            if !(item[indexPath.section].count > 0) {
                tableView.headerView(forSection: indexPath.section)?.textLabel?.text = ""
            }
            
            //tableView.endUpdates()
            
//            isReload = false
//            tableView.reloadData()
        }
    }
    
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        switch indexPath.section {
        case 0:
            return .delete
        case 1:
            return .insert
        default:
            return .none
        }
    }
}

extension EditActionController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return item.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return item[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! EditMoreTableCell
        cell.selectionStyle = .none
        let itemCell = item[indexPath.section][indexPath.row]
        
        cell.imageViewCell.image = itemCell.image
        cell.titleLabel.text = itemCell.title?.replace(target: "Трек в ", withString: "")
        return cell
    }
}

class EditActionController: UIViewController {
    
    let tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .insetGrouped)
        tableView.backgroundColor = .clear
        tableView.translatesAutoresizingMaskIntoConstraints = false
        return tableView
    }()
    
    var isReload: Bool = false
    
    var item = [[moreTableItem]](){
        didSet{
            if !isReload {
                tableView.reloadData()
            }
        }
    }
    let cellId = "cellId"
    let heightForRowAt: CGFloat = 60
    let titleForSectionString = "Музыкальные сервисы".uppercased()
    
    var delegate: EditActionControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Действия"
        
        view.backgroundColor = .systemBackground
        
        let rightNavButton = UIBarButtonItem(title: "Готово", style: .done, target: self, action: #selector(doneButtonHandler))
        rightNavButton.tintColor = .tintSelectColor
        navigationItem.rightBarButtonItem = rightNavButton
        
        setupTableView()
        
//        typeItemCell.allCases.forEach { print($0) }
    }
    
//    override func viewDidLayoutSubviews() {
//        super.viewDidLayoutSubviews()
//
//        if #available(iOS 10, *) {
//            UIView.animate(withDuration: 0.3) { self.view.layoutIfNeeded() }
//        }
//    }
    
    private func setupTableView(){
        tableView.register(EditMoreTableCell.self, forCellReuseIdentifier: cellId)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.isEditing = true
        
        view.addSubview(tableView)
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: view.topAnchor),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
        ])
    }
    
    @objc private func doneButtonHandler(){
        dismiss(animated: true) {
            
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        DispatchQueue.main.async {
//            self.tableView.reloadData()
//        }
    }
}

import UIKit

extension UITableView {

    /// Perform a series of method calls that insert, delete, or select rows and sections of the table view.
    /// This is equivalent to a beginUpdates() / endUpdates() sequence,
    /// with a completion closure when the animation is finished.
    /// Parameter update: the update operation to perform on the tableView.
    /// Parameter completion: the completion closure to be executed when the animation is completed.
   
    func performUpdate(_ update: ()->Void, completion: (()->Void)?) {
    
        CATransaction.begin()
        CATransaction.setCompletionBlock(completion)

        // Table View update on row / section
        beginUpdates()
        update()
        endUpdates()
    
        CATransaction.commit()
    }

}
