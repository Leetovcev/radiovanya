//
//  MoreViewController.swift
//  RadioVanya
//
//  Created by Александр Краснощеков on 14.02.2021.
//  Copyright © 2021 Александр Краснощеков. All rights reserved.
//

import UIKit
import SDWebImage

extension MoreButtonViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return heightForRowAt
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = self.item[indexPath.section][indexPath.row]
        delegate?.selectedMenuItem(item)
    }
}

extension MoreButtonViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return item.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return item[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! MoreTableCell
        let itemCell = item[indexPath.section][indexPath.row]
        
        cell.imageViewCell.image = itemCell.image
        cell.titleLabel.text = itemCell.title
        return cell
    }
}

protocol MoreButtonViewControllerDelegate {
    func selectedMenuItem (_ item: moreTableItem)
    
    func changedPositionView (_ y: CGFloat)
    
    func endedPositionView ()
    
    func closeMoreView()
    
    func editAction(_ item: [[moreTableItem]])
}

class MoreButtonViewController: UIViewController {
    
    var delegate: MoreButtonViewControllerDelegate?
    
    let cellId = "cellId"
    let heightHeader: CGFloat = 100
    let heightForRowAt: CGFloat = 60
    
    let headerView: UIView = {
        let view = UIView()
        
        let blurEffect = UIBlurEffect(style: .systemMaterial)//systemThickMaterial
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.isUserInteractionEnabled = false
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
//        blurEffectView.alpha = 0.87
        view.addSubview(blurEffectView)
        blurEffectView.translatesAutoresizingMaskIntoConstraints = false
        blurEffectView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        blurEffectView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        blurEffectView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        blurEffectView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    //CLOSE BUTTON DISMISS
    let closeButton: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 16
        view.layer.masksToBounds = true
        view.isUserInteractionEnabled = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let closeButtonImageView: UIImageView = {
        let iv = UIImageView()
        let config = UIImage.SymbolConfiguration(pointSize: 18, weight: .heavy, scale: .medium)
        iv.image = UIImage(systemName: "xmark", withConfiguration: config)?.withRenderingMode(.alwaysTemplate)
        iv.tintColor = .label//.black
        iv.contentMode = .scaleAspectFit
        iv.isUserInteractionEnabled = false
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    let closeButtonTrailingAnchorConstant: CGFloat = 12
    let closeButtonWidthAnchorConstraint: CGFloat = 32
    
    var track: itemsTrackAndMenu? {
        didSet{
            self.artistLabel.text = track?.itemsTrack?.artist
            self.songLabel.text = track?.itemsTrack?.track
            
            if let track = track?.itemsTrack, let trackImgString = track.image100, let trackImgUrl = URL(string: trackImgString) {
                self.trackImg.sd_setImage(with: trackImgUrl) { (image, error, type, url) in }
            }else{
                self.trackImg.image = #imageLiteral(resourceName: "600x600bb")
            }
            
            reloadItems()
            
            tableView.tableFooterView?.isHidden = !(track?.menu?.count ?? 1 > 1)
            tableView.verticalScrollIndicatorInsets.top = heightHeader
            tableView.contentInset.top = heightHeader
            tableView.setContentOffset(CGPoint(x: 0, y: -heightHeader), animated: false)
        }
    }
    
    var item = [[moreTableItem]]()
    
    
    func reloadItems() {
        item.removeAll()
        
        //item = track?.menu ?? []
        let defaults = UserDefaults.standard
        var actionMusicService = defaults.stringArray(forKey: "actionMusicService") ?? [String]()
        
        actionMusicService.removeAll { (test) -> Bool in
            test == "copy" || test == "share"
        }
        
        actionMusicService.forEach {
            print($0)
        }
                    
        if let items = track?.menu {
            let itemTest = items.last?.filter({ (one: moreTableItem) -> Bool in
                return actionMusicService.contains { (two) -> Bool in
                    return one.type?.rawValue == two
                }
            })
            
            if let first = items.first {
                self.item.append(first)
            }
            
            if let itemLast = itemTest, itemLast.count > 0 {
//                self.item.append(itemLast)
                var sortItem = [moreTableItem]()
                actionMusicService.forEach { it in
                    for val in itemLast {
                        if val.type?.rawValue == it {
                            sortItem.append(val)
                        }
                    }
                }
                self.item.append(sortItem)
            }
        }
        
        
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    let trackImg: UIImageView = {
        let iv = UIImageView()
        iv.layer.cornerRadius = 8
        iv.image = #imageLiteral(resourceName: "600x600bb")
        iv.layer.masksToBounds = true
        iv.layer.borderWidth = 0.5
        iv.layer.borderColor = UIColor.init(white: 0.7, alpha: 0.3).cgColor
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    let artistLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 16, weight: .semibold)
        label.numberOfLines = 2
        label.textColor = .secondaryLabel
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let songLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 16, weight: .semibold)
        label.numberOfLines = 2
        label.textColor = .label
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let tableView: UITableView = {
        let tv = UITableView(frame: .zero, style: .insetGrouped)
        tv.backgroundColor = .clear
        tv.showsVerticalScrollIndicator = false
        tv.translatesAutoresizingMaskIntoConstraints = false
        return tv
    }()
    
    let editActionsButton: UIButton = {
        let button = UIButton()
        button.setTitle("Редактировать действия...", for: .normal)
        button.setTitleColor(.tintSelectColor, for: .normal)
        button.contentHorizontalAlignment = .left
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .clear
        setupTableView()
        setupHeaderView()
        
        firsInitActionMusicServiceUserDefaults()
    }
    
    private func firsInitActionMusicServiceUserDefaults(){
        let defaults = UserDefaults.standard
        
        if !defaults.bool(forKey: "isFirstMusicService") {
            var arrayAdd = [String]()
            typeItemCell.allCases.forEach { item in
                arrayAdd.append(item.rawValue)
            }
            defaults.set(arrayAdd, forKey: "actionMusicService")
            defaults.set(true, forKey: "isFirstMusicService")
        }
    }
    
    private func setupHeaderView(){
        view.addSubview(headerView)
        NSLayoutConstraint.activate([
            headerView.topAnchor.constraint(equalTo: view.topAnchor),
            headerView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            headerView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            headerView.heightAnchor.constraint(equalToConstant: heightHeader)
        ])
        
        let topView = UIView()
        topView.backgroundColor = .systemGray4
        topView.layer.cornerRadius = 2.5
        topView.layer.masksToBounds = true
        topView.translatesAutoresizingMaskIntoConstraints = false
        
        headerView.addSubview(topView)
        NSLayoutConstraint.activate([
            topView.centerXAnchor.constraint(equalTo: headerView.centerXAnchor),
            topView.topAnchor.constraint(equalTo: headerView.topAnchor, constant: 8),
            topView.widthAnchor.constraint(equalToConstant: 44),
            topView.heightAnchor.constraint(equalToConstant: 5)
        ])
        
        let hStackView = UIStackView()
        hStackView.spacing = 16
        hStackView.alignment = .center
        hStackView.translatesAutoresizingMaskIntoConstraints = false
        headerView.addSubview(hStackView)
        NSLayoutConstraint.activate([
            hStackView.topAnchor.constraint(equalTo: headerView.topAnchor, constant: 21),
            hStackView.leadingAnchor.constraint(equalTo: headerView.leadingAnchor, constant: 16),
            hStackView.trailingAnchor.constraint(equalTo: headerView.trailingAnchor, constant: -16 - closeButtonWidthAnchorConstraint - closeButtonTrailingAnchorConstant),
            hStackView.bottomAnchor.constraint(equalTo: headerView.bottomAnchor, constant: -16),
        ])
        
        let vStackView = UIStackView()
        vStackView.axis = .vertical
        
        
        [songLabel, artistLabel].forEach {vStackView.addArrangedSubview($0)}
        
        [trackImg, vStackView].forEach {hStackView.addArrangedSubview($0)}
        trackImg.widthAnchor.constraint(equalTo: trackImg.heightAnchor, multiplier: 1).isActive = true
        
        let bottomSeparatorView = UIView()
        bottomSeparatorView.backgroundColor = .separator//.init(white: 0.3, alpha: 0.3)
        bottomSeparatorView.translatesAutoresizingMaskIntoConstraints = false
        headerView.addSubview(bottomSeparatorView)
        NSLayoutConstraint.activate([
            bottomSeparatorView.heightAnchor.constraint(equalToConstant: 0.5),
            bottomSeparatorView.leadingAnchor.constraint(equalTo: headerView.leadingAnchor),
            bottomSeparatorView.trailingAnchor.constraint(equalTo: headerView.trailingAnchor),
            bottomSeparatorView.bottomAnchor.constraint(equalTo: headerView.bottomAnchor),
        ])
        
        setupCloseButtonView()
        
        let panGesrure = UIPanGestureRecognizer(target: self, action: #selector(headerViewGesture))
        headerView.addGestureRecognizer(panGesrure)
    }
    
    private func setupCloseButtonView(){
        headerView.addSubview(closeButton)
        closeButton.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 12).isActive = true
        closeButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -closeButtonTrailingAnchorConstant).isActive = true
        closeButton.widthAnchor.constraint(equalToConstant: closeButtonWidthAnchorConstraint).isActive = true
        closeButton.heightAnchor.constraint(equalToConstant: closeButtonWidthAnchorConstraint).isActive = true
        
        let blurEffect = UIBlurEffect(style: .systemThickMaterial)//.light
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.isUserInteractionEnabled = false
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        closeButton.addSubview(blurEffectView)
        blurEffectView.translatesAutoresizingMaskIntoConstraints = false
        blurEffectView.topAnchor.constraint(equalTo: closeButton.topAnchor).isActive = true
        blurEffectView.leadingAnchor.constraint(equalTo: closeButton.leadingAnchor).isActive = true
        blurEffectView.trailingAnchor.constraint(equalTo: closeButton.trailingAnchor).isActive = true
        blurEffectView.bottomAnchor.constraint(equalTo: closeButton.bottomAnchor).isActive = true
        
        closeButton.addSubview(closeButtonImageView)
        closeButtonImageView.centerXAnchor.constraint(equalTo: closeButton.centerXAnchor).isActive = true
        closeButtonImageView.centerYAnchor.constraint(equalTo: closeButton.centerYAnchor).isActive = true
        //closeButtonImageView.widthAnchor.constraint(equalTo: closeButton.widthAnchor, multiplier: 1, constant: -12).isActive = true
        closeButtonImageView.heightAnchor.constraint(equalTo: closeButton.heightAnchor, multiplier: 1, constant: -12).isActive = true
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(closeMoreView))
        closeButton.addGestureRecognizer(tapGesture)
    }
    
    private func setupTableView(){
        tableView.register(MoreTableCell.self, forCellReuseIdentifier: cellId)
        tableView.delegate = self
        tableView.dataSource = self
        view.addSubview(tableView)
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: view.topAnchor),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
        ])
        setupFooterTableView()
    }
    
    var tableFooterHeight: CGFloat = 44
    
    private func setupFooterTableView() {
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: tableFooterHeight))
        tableView.tableFooterView = footerView
        
        footerView.addSubview(editActionsButton)
        NSLayoutConstraint.activate([
            editActionsButton.centerYAnchor.constraint(equalTo: footerView.centerYAnchor),
            editActionsButton.leadingAnchor.constraint(equalTo: footerView.leadingAnchor, constant: 32),
            editActionsButton.trailingAnchor.constraint(equalTo: footerView.trailingAnchor, constant: -32),

        ])
        editActionsButton.addTarget(self, action: #selector(editActionsHandler), for: .touchUpInside)
    }
    
    var startHeaderViewPosition: CGFloat = 0
    
    @objc private func headerViewGesture(_ gesture: UIPanGestureRecognizer){
        let y = gesture.translation(in: view).y
        
        switch gesture.state {
        case .began:
            startHeaderViewPosition = y
        case .changed:
            delegate?.changedPositionView(startHeaderViewPosition - y)
        case .ended:
            delegate?.endedPositionView()
        default:
            return
        }
    }
    
    @objc private func closeMoreView(){
        delegate?.closeMoreView()
    }
    
    @objc private func editActionsHandler(){
        var addServise = [moreTableItem]()
        var remServise = [moreTableItem]()
        
        if let trackMenu = track?.menu, trackMenu.count > 1, let sociaItem = trackMenu.last {
            let defaults = UserDefaults.standard
            let actionMusicService = defaults.stringArray(forKey: "actionMusicService") ?? [String]()
            
            remServise = sociaItem.filter { (item) -> Bool in
                return actionMusicService.contains { (userDef) -> Bool in
                    return item.type?.rawValue == userDef
                }
            }
            
            if remServise.count > 0 {
                var sortItem = [moreTableItem]()
                actionMusicService.forEach { it in
                    for val in remServise {
                        if val.type?.rawValue == it {
                            sortItem.append(val)
                        }
                    }
                }
                remServise = sortItem
            }
            
            addServise = sociaItem.filter { (item) -> Bool in
                return !remServise.contains { (userDef) -> Bool in
                    return item.type == userDef.type
                }
            }
            
            delegate?.editAction([remServise, addServise])
        }
        
        /*
        if item.count > 1 {
            let defaults = UserDefaults.standard
            let actionMusicService = defaults.stringArray(forKey: "actionMusicService") ?? [String]()
            
            
            
            
            
//            item[1].forEach { item in
//                actionMusicService.forEach { type in
//                    if item.type.debugDescription == type {
//                        addServise.append(item)
//                    }else{
//                        remServise.append(item)
//                    }
//                }
//            }
//
//            delegate?.editAction([addServise, remServise])
            
//            typeItemCell.allCases.forEach { socItem in
//                if (actionMusicService.first(where: { (item) -> Bool in
//                    socItem.rawValue == item
//                }) != nil) {
//
//                }
//            }
//
//            delegate?.editAction([remServise, addServise])
            
            if actionMusicService.count > 0 {
                actionMusicService.forEach { type in
                    item[1].forEach { item in
                        if item.type?.rawValue == type {
                            addServise.append(item)
                        }else{
                            remServise.append(item)
                            return
                        }
                    }
                }

                delegate?.editAction([remServise, addServise])
            }else{
                delegate?.editAction([item[1]])
            }
            
        }
        */
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
    }
}


