//
//  TabBarController.swift
//  RadioVanya
//
//  Created by Александр Краснощеков on 08.10.2020.
//  Copyright © 2020 Александр Краснощеков. All rights reserved.
//

import UIKit
import Foundation
import StreamingKit

//extension TabBarController: UIViewControllerTransitioningDelegate {
//    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
//        transition.transitionMode = .present
//        transition.startingPoint = testRadioView.center
//        transition.circleColor = testRadioView.backgroundColor!
//
//        return transition
//    }
//
//    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
//        transition.transitionMode = .dismiss
//        transition.startingPoint = testRadioView.center
//        transition.circleColor = testRadioView.backgroundColor!
//
//        return transition
//    }
//}

extension TabBarController: EditActionControllerDelegate{
    func changeItemCount() {
        moreController?.reloadItems()
        if let currentMenuItems = currentMenuItems {
            currentHeightMoreViewHandler(currentMenuItems)
        }
    }
}

extension TabBarController: MoreButtonViewControllerDelegate{
    func editAction(_ item: [[moreTableItem]]) {
        editActionPresent(item)
    }
    
    func closeMoreView() {
        closeMoreViewPlayerController()
    }
    
    func changedPositionView(_ y: CGFloat) {
        moreViewPosition(y)
    }
    
    func endedPositionView(){
        moreViewPositionEnded()
    }
    
    func selectedMenuItem(_ item: moreTableItem) {
        if let artist = item.artist, let song = item.song {
            let artistSong: String = "\(artist) \(song)"
            if let searchString = artistSong.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
                var urlString: String = ""
                switch item.type {
                case .appleMusic:
                    if let itunesUrl = item.itunesUrl {
                        urlString = itunesUrl
                    }
                case .vk:
                    urlString = "https://m.vk.com/audios0?q=\(searchString)&section=my"
                case .spotify:
                    urlString = "https://open.spotify.com/search/\(searchString)"
                case .youtube:
                    urlString = "https://music.youtube.com/search?q=\(searchString)"
                case .yandexmusic:
                    urlString = "https://music.yandex.ru/search?text=\(searchString)&type=tracks"
                case .copy:
                    copyText(artistSong)
                case .share:
                    shareTrack(artistSong)
                default:
                    print("")
                }
                self.closeMoreViewPlayerController()
                
                if let url = URL(string: urlString) {
                    UIApplication.shared.open(url, options: [:]) { (_ ) in }
                }
            }
        }
    }
}

extension TabBarController: RaadioPlayerControlDelegate {
    func handlerMoreView(item: itemsTrack) {
        openMoreViewPlayerController(item)
    }
    
    
    func statePlaying(state: STKAudioPlayerState) {
        
        radioStationViewController.changeCellRadioStatusPlaying(state: state, animation: true)
        switch state {
        case .playing:
            print("DELEGATE - play")
        case .paused:
            print("DELEGATE - paused")
        case .stopped:
            print("DELEGATE - stoped")
        default:
            return
        }
    }
    
    func closePlayerView() {
        closePopup(animated: true, completion: {
            print("Closed")
        })
    }
    
    func controlsPlayer(button: UIButton) {
        let tag = button.tag
        switch tag {
        case 1:
            radioStationViewController.backSelectedItemStatio()
        case 3:
            radioStationViewController.nextSelectedItemStatio()
        default:
            return
        }
    }
}

extension TabBarController: RadioStationSelectDelegate {
    func playPauseCell() {
        radioPlayer.playToggle()
    }
    
    func selectRadio(station: RadioStation) {

        if !isPresentPopupBar {
            
            presentPopupBar(withContentViewController: radioPlayer, animated: true, completion: {
                self.isPresentPopupBar = true
            })
            
            popupBar.marqueeScrollEnabled = true
            popupBar.imageView.layer.cornerRadius = 6
            popupBar.imageView.contentMode = .scaleAspectFill
            
            popupBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.label]
            popupBar.subtitleTextAttributes = [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.774078846, green: 0.1601946652, blue: 0.1577996314, alpha: 1)]
            
            popupContentView.popupCloseButton.isHidden = true//.alpha = 0
            
            setupMorePlayerView()
            
            let separatorView = UIView()
            separatorView.backgroundColor = .separator//UIColor.init(white: 0.5, alpha: 0.5)
            separatorView.translatesAutoresizingMaskIntoConstraints = false
            popupBar.addSubview(separatorView)
            separatorView.heightAnchor.constraint(equalToConstant: 0.5).isActive = true
            separatorView.topAnchor.constraint(equalTo: popupBar.topAnchor).isActive = true
            separatorView.leadingAnchor.constraint(equalTo: popupBar.leadingAnchor).isActive = true
            separatorView.trailingAnchor.constraint(equalTo: popupBar.trailingAnchor).isActive = true
        }
        
        if selectedRadioId != station.streamId {
            selectedRadioId = station.streamId
            if let audioPlayer = radioPlayer.audioPlayer {
                audioPlayer.stop()
            }
            
            radioPlayer.playlistArray.removeAll()
            radioPlayer.startActivityIndicatorView()
            //??
            //radioPlayer.playlistTableView.reloadData()
            
            if let stringImg = station.image300, let imgUrl = URL(string: stringImg) {
                let imageView = UIImageView()
                imageView.sd_setImage(with: imgUrl) { (image, error, cach, url) in
                    self.radioPlayer.popupItem.image = image
                    self.radioPlayer.trackImageView.image = image
                    if let image = image {
                        UIView.animate(withDuration: 0.5) {
                            self.radioPlayer.view.backgroundColor = image.averageColor
                        }
                    }
                }
            }else{
                radioPlayer.popupItem.image = #imageLiteral(resourceName: "600x600bb")
                self.radioPlayer.trackImageView.image = #imageLiteral(resourceName: "600x600bb")
            }
            
            
            radioPlayer.popupItem.title = station.title
            radioPlayer.popupItem.subtitle = nil
            
            radioPlayer.songLabel.text = "Радио"
            if let stationTitle = station.title {
                radioPlayer.artistLabel.text = stationTitle.replace(target: "Радио", withString: "")
            }
             
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                self.radioPlayer.radioStation = station
                self.radioPlayer.popupItem.subtitle = station.title
            })
        }else{
            radioPlayer.playToggle()
        }
        
        
    }
}

extension TabBarController: EQualizerDelegate{
    func eqIsOn(value: Bool) {
        radioPlayer.audioPlayer?.equalizerEnabled = value
    }
    
    func setGain(forEqualizerBand: Int32, value: Float) {
        radioPlayer.audioPlayer?.setGain(value, forEqualizerBand: forEqualizerBand)
    }
}

protocol NotificationViewDelegate {
    func showNotificationView(image: UIImage?, title: String)
}

class TabBarController: UITabBarController {
    
    var delegateNV: NotificationViewDelegate?
    
    var isPresentPopupBar: Bool = false
    let radioPlayer = PlayerViewController()
    let radioStationViewController = RadioStationViewController()
    let moreViewController = MoreViewController()
    let editActionController = EditActionController()
    
    var selectedRadioId: Int?
    
    //LaunchScreen
    let launchScreenWrapperView: UIView = {
        let view = UIView()
        view.backgroundColor = #colorLiteral(red: 0.7725490196, green: 0.1607843137, blue: 0.1568627451, alpha: 1)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let launchScreenWrapperImageView: UIImageView = {
        let iv = UIImageView()
        iv.image = #imageLiteral(resourceName: "logo_vanya")
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    let morePlayerViewFader: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.init(white: 0, alpha: 0.75)
        view.alpha = 0
        view.isUserInteractionEnabled = false
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var morePlayerViewTopAnchor: NSLayoutConstraint?
    
    let morePlayerView: MorePlayerView = {
        let view = MorePlayerView()
//        view.layer.cornerRadius = 16
//        view.layer.masksToBounds = true
//        view.backgroundColor = .systemBackground
        let blurEffect = UIBlurEffect(style: .systemMaterial)//.light //systemThickMaterial
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.isUserInteractionEnabled = false
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
//        blurEffectView.alpha = 0.87
        view.addSubview(blurEffectView)
        blurEffectView.translatesAutoresizingMaskIntoConstraints = false
        blurEffectView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        blurEffectView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        blurEffectView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        blurEffectView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupLaunchScreen()
        
        radioPlayer.delegate = self
        radioStationViewController.delegate = self
        moreViewController.equalizer.delegate = self
        editActionController.delegate = self
        
        let selectedRadioImg = UIImage(systemName: "dot.radiowaves.left.and.right")?.withTintColor(.tintSelectColor, renderingMode: .alwaysOriginal)

        let radioStation = UINavigationController(rootViewController: radioStationViewController)//RadioStationViewController()
        radioStation.tabBarItem = UITabBarItem(title: "Радио", image: UIImage(systemName: "dot.radiowaves.left.and.right"), selectedImage: selectedRadioImg)//, tag: 0)
        
        let selectedVideosImg = UIImage(systemName: "tv.music.note")?.withTintColor(.tintSelectColor, renderingMode: .alwaysOriginal)
        
        let videosViewController = UINavigationController(rootViewController: VideosViewController())
        videosViewController.navigationBar.prefersLargeTitles = true
        videosViewController.tabBarItem = UITabBarItem(title: "Видео", image: UIImage(systemName: "tv.music.note"), selectedImage: selectedVideosImg)//, tag: 2)
        
//        let newsViewController = UINavigationController(rootViewController: UIViewController())
//        newsViewController.tabBarItem = UITabBarItem(title: "Видео", image: #imageLiteral(resourceName: "news_icon"), tag: 3)
        
        let selectedMoreImg = UIImage(systemName: "list.dash")?.withTintColor(.tintSelectColor, renderingMode: .alwaysOriginal)
        
        let more = UINavigationController(rootViewController: moreViewController)
        moreViewController.tabBarItem = UITabBarItem(title: "Еще", image: UIImage(systemName: "list.dash"), selectedImage: selectedMoreImg)//, tag: 4)//line.horizontal.3 ellipsis.circle
        
        viewControllers = [radioStation, videosViewController, more]
        
        setupUIApplicationShortcutIcon()
        
//        setupTestRadioView()
    }
    
    let transition = CircularTransition()
    
    let testRadioView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 32
        view.layer.masksToBounds = true
        let imageView = UIImageView()
        imageView.image = #imageLiteral(resourceName: "record_image600_white_fill")
        imageView.contentMode = .scaleAspectFit
        view.addSubview(imageView)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        imageView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        imageView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 8).isActive = true
        imageView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -8).isActive = true
        view.backgroundColor = #colorLiteral(red: 0.9990238547, green: 0.3811987638, blue: 0.006764368154, alpha: 1)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
//    private func setupTestRadioView() {
//        view.addSubview(testRadioView)
//        testRadioView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16).isActive = true
//        testRadioView.bottomAnchor.constraint(equalTo: tabBar.topAnchor, constant: -16).isActive = true
//        testRadioView.widthAnchor.constraint(equalToConstant: 64).isActive = true
//        testRadioView.heightAnchor.constraint(equalTo: testRadioView.widthAnchor, multiplier: 1).isActive = true
//
//        testRadioView.isUserInteractionEnabled = true
//        testRadioView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(testRadioViewHandler)))
//    }
    
//    @objc private func testRadioViewHandler() {
//        let viewController = SecondViewController()
//        viewController.modalPresentationStyle = .custom
//        viewController.transitioningDelegate = self
//        present(viewController, animated: true) {
////            viewController.setGradientBackground()
//        }
//    }
    
    func setupUIApplicationShortcutIcon(){
//        UIApplicationShortcutIcon * photoIcon = [UIApplicationShortcutIcon iconWithTemplateImageName: @"selfie-100.png"]; // your customize icon
//        UIApplicationShortcutItem * photoItem = [[UIApplicationShortcutItem alloc]initWithType: @"selfie" localizedTitle: @"take selfie" localizedSubtitle: nil icon: photoIcon userInfo: nil];
//        UIApplicationShortcutItem * videoItem = [[UIApplicationShortcutItem alloc]initWithType: @"video" localizedTitle: @"take video" localizedSubtitle: nil icon: [UIApplicationShortcutIcon iconWithType: UIApplicationShortcutIconTypeCaptureVideo] userInfo: nil];
//
//        [UIApplication sharedApplication].shortcutItems = @[photoItem,videoItem];
        
        let testIcon = UIApplicationShortcutItem(type: "station", localizedTitle: "Radio Record", localizedSubtitle: "Недавно прослушано", icon: UIApplicationShortcutIcon(systemImageName: "play.circle"), userInfo: ["play" : "0" as NSSecureCoding])
        
        let testIcon1 = UIApplicationShortcutItem(type: "station", localizedTitle: "Radio Trancemission", localizedSubtitle: "Недавно прослушано", icon: UIApplicationShortcutIcon(systemImageName: "play.circle"), userInfo: ["play" : "1" as NSSecureCoding])
        
        UIApplication.shared.shortcutItems = [testIcon, testIcon1]
    }
    
    
    var moreController: MoreButtonViewController?
    var morePlayerViewHeightAnchor: NSLayoutConstraint?
    
    private func setupMorePlayerView(){
        popupContentView.addSubview(morePlayerViewFader)
        morePlayerViewFader.leadingAnchor.constraint(equalTo: popupContentView.leadingAnchor).isActive = true
        morePlayerViewFader.trailingAnchor.constraint(equalTo: popupContentView.trailingAnchor).isActive = true
        morePlayerViewFader.topAnchor.constraint(equalTo: popupContentView.topAnchor).isActive = true
        morePlayerViewFader.bottomAnchor.constraint(equalTo: popupContentView.bottomAnchor).isActive = true
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(closeMoreViewPlayerController))
        morePlayerViewFader.addGestureRecognizer(tapGestureRecognizer)
        
        popupContentView.addSubview(morePlayerView)
        morePlayerViewTopAnchor = morePlayerView.topAnchor.constraint(equalTo: popupContentView.bottomAnchor)
        morePlayerViewTopAnchor?.isActive = true
        morePlayerView.leadingAnchor.constraint(equalTo: popupContentView.leadingAnchor).isActive = true
        morePlayerView.trailingAnchor.constraint(equalTo: popupContentView.trailingAnchor).isActive = true
        morePlayerViewHeightAnchor = morePlayerView.heightAnchor.constraint(equalToConstant: 0)
        morePlayerViewHeightAnchor?.isActive = true
        
        moreController = MoreButtonViewController()
        if let moreController = moreController, let moreConView = moreController.view {
            moreController.delegate = self
            moreConView.translatesAutoresizingMaskIntoConstraints = false
            morePlayerView.addSubview(moreConView)
            NSLayoutConstraint.activate([
                moreConView.leadingAnchor.constraint(equalTo: morePlayerView.leadingAnchor),
                moreConView.trailingAnchor.constraint(equalTo: morePlayerView.trailingAnchor),
                moreConView.topAnchor.constraint(equalTo: morePlayerView.topAnchor),
                moreConView.bottomAnchor.constraint(equalTo: morePlayerView.bottomAnchor)
            ])
            self.addChild(moreController)
        }
    }
    
//    var audioPlayer: STKAudioPlayer
//    convenience init(audioPlayer: STKAudioPlayer) {
//        self.init(audioPlayer: audioPlayer)
//
//        self.audioPlayer = audioPlayer
//        // Do other setup
//    }
    
    fileprivate func setupLaunchScreen() {
        view.addSubview(launchScreenWrapperView)
        launchScreenWrapperView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        launchScreenWrapperView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        launchScreenWrapperView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        launchScreenWrapperView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
        launchScreenWrapperView.addSubview(launchScreenWrapperImageView)
        launchScreenWrapperImageView.centerXAnchor.constraint(equalTo: launchScreenWrapperView.safeAreaLayoutGuide.centerXAnchor).isActive = true
        
//        let window = UIApplication.shared.windows[0]
//        let safeFrame = window.safeAreaLayoutGuide.layoutFrame
//        let topSafeAreaHeight = safeFrame.minY
        
        launchScreenWrapperImageView.centerYAnchor.constraint(equalTo: launchScreenWrapperView.safeAreaLayoutGuide.centerYAnchor).isActive = true// constant:topSafeAreaHeight == 0 ? 10 : 0
        launchScreenWrapperImageView.widthAnchor.constraint(equalToConstant: 150).isActive = true
        launchScreenWrapperImageView.heightAnchor.constraint(equalTo: launchScreenWrapperImageView.widthAnchor, multiplier: 1).isActive = true
        
        UIView.animate(withDuration: 0.35, delay: 0.4, options: .curveEaseIn, animations: {
            self.launchScreenWrapperView.alpha = 0
        }) { (_ ) in
            
            self.radioStationViewController.hideStatusBar = false
            self.radioStationViewController.navigationController?.setNavigationBarHidden(false, animated: true)

            self.radioStationViewController.stationArray = Request.stationArray.map { $0 }//.reversed()
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.7) {
            UIView.animate(withDuration: 0.35) {
                self.tabBar.transform = .identity
            }
        }
    }
    
    private var firstRun: Bool = false
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if !firstRun {
            DispatchQueue.main.async {
                self.tabBar.transform = CGAffineTransform(translationX: self.tabBar.frame.minX, y: self.tabBar.frame.minY + 100)
            }
            firstRun = true
        }
    }
    
    var currentHeightMoreView: CGFloat?
    var currentMenuItems: [[moreTableItem]]?
    
    private func openMoreViewPlayerController(_ track: itemsTrack) {
        if let moreController = moreController {

            let items = makeMoreItemsHandler(track)
            currentMenuItems = items
            
            currentHeightMoreViewHandler(items)
            
            moreController.track = itemsTrackAndMenu.init(itemsTrack: track, menu: items)
            
        }
    }
    
    private func currentHeightMoreViewHandler(_ items: [[moreTableItem]]) {
        var itemsForHeight = [[moreTableItem]]()
        var firesItems = [moreTableItem]()
        var lastItems = [moreTableItem]()
        
        if items.count > 1, let sociaItem = items.last {
            let defaults = UserDefaults.standard
            let actionMusicService = defaults.stringArray(forKey: "actionMusicService") ?? [String]()
            
            lastItems = sociaItem.filter { (item) -> Bool in
                return actionMusicService.contains { (userDef) -> Bool in
                    return item.type?.rawValue == userDef
                }
            }
            
            if let fir = items.first {
                firesItems = fir
            }
            
            itemsForHeight = [firesItems, lastItems]
        }else{
            if let fir = items.first {
                itemsForHeight = [fir]
            }
        }
        
        if let moreController = moreController {
            let height = min(view.frame.size.height - 44, getHeightMoreMenu(itemsForHeight, moreController: moreController))
            currentHeightMoreView = height
        }
        
        openMoreView()
    }
    
    private func openMoreView() {
        if let currentHeightMoreView = currentHeightMoreView {
            morePlayerViewFader.isUserInteractionEnabled = true
            morePlayerViewTopAnchor?.constant = -currentHeightMoreView//(view.frame.height / 2)
            morePlayerViewHeightAnchor?.constant = currentHeightMoreView
            
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseIn) {
                self.morePlayerViewFader.alpha = 1
                self.view.layoutIfNeeded()
            } completion: { (_ ) in
                
            }
        }
    }
    
    private func makeMoreItemsHandler(_ track: itemsTrack) -> [[moreTableItem]] {
        var items = [[moreTableItem]]()
        
        let config = UIImage.SymbolConfiguration(pointSize: 24, weight: .medium, scale: .medium)
        let share = moreTableItem.init(image: UIImage(systemName: "arrowshape.turn.up.right", withConfiguration: config)?.withRenderingMode(.alwaysTemplate), title: "Поделится треком", artist: track.artist, song: track.track, itunesUrl: nil, type: .share)//square.and.arrow.up
        
        let copy = moreTableItem.init(image: UIImage(systemName: "doc.on.doc", withConfiguration: config)?.withRenderingMode(.alwaysTemplate), title: "Скопировать название", artist: track.artist, song: track.track, itunesUrl: nil, type: .copy)
        
        items.append([share, copy])
        
        if let itunesUrl = track.itunesUrl {
            let defaults = UserDefaults.standard
            let actionMusicService = defaults.stringArray(forKey: "actionMusicService") ?? [String]()
            
            let appleMusicAction = moreTableItem.init(image: #imageLiteral(resourceName: "appleMusicAction").withRenderingMode(.alwaysTemplate), title: "Трек в Apple Music", artist: track.artist, song: track.track, itunesUrl: itunesUrl, type: .appleMusic)
            let vk = moreTableItem.init(image: #imageLiteral(resourceName: "vk").withRenderingMode(.alwaysTemplate), title: "Трек в ВКонтакте", artist: track.artist, song: track.track, itunesUrl: nil, type: .vk)
            let spotify = moreTableItem.init(image: #imageLiteral(resourceName: "spotify").withRenderingMode(.alwaysTemplate), title: "Трек в Spotify", artist: track.artist, song: track.track, itunesUrl: nil, type: .spotify)
            let youtube = moreTableItem.init(image: #imageLiteral(resourceName: "youtube").withRenderingMode(.alwaysTemplate), title: "Трек в YouTube Music", artist: track.artist, song: track.track, itunesUrl: nil, type: .youtube)
            let yandexmusic = moreTableItem.init(image: #imageLiteral(resourceName: "yandexmusic").withRenderingMode(.alwaysTemplate), title: "Трек в Yandex Music", artist: track.artist, song: track.track, itunesUrl: nil, type: .yandexmusic)
            
            let arraySocial = [appleMusicAction, vk, spotify, youtube, yandexmusic]
            
            var newArray = [moreTableItem]()
            var lastArray = [moreTableItem]()
            
            if actionMusicService.count > 0 {
                for (inddex, item ) in arraySocial.enumerated() {
                    if actionMusicService.count - 1 >= inddex {
                        if item.type?.rawValue == actionMusicService[inddex] {
                            newArray.append(item)
                        }else{
                            lastArray.append(item)
                        }
                    }else{
                        lastArray.append(item)
                    }
                }
            }
            
            lastArray.forEach { newArray.append($0) }
            items.append(newArray)
        }
        
        return items
    }
    
    private func getHeightMoreMenu(_ items: [[moreTableItem]], moreController: MoreButtonViewController) -> CGFloat {
        var height: CGFloat = 0
        items.forEach { (item) in
            height += 16 + (CGFloat(item.count) * moreController.heightForRowAt)
        }
        height += moreController.heightHeader + 36 + 18

//        let topSafeArea: CGFloat
        let bottomSafeArea: CGFloat
        
        if #available(iOS 11.0, *) {
//            topSafeArea = view.safeAreaInsets.top
            bottomSafeArea = view.safeAreaInsets.bottom
        } else {
//            topSafeArea = topLayoutGuide.length
            bottomSafeArea = bottomLayoutGuide.length
        }
        height += bottomSafeArea
        height += items.count > 1 ? moreController.tableView.tableFooterView?.frame.height ?? 0 : 0

        return height
    }
    
    @objc private func closeMoreViewPlayerController() {
        morePlayerViewFader.isUserInteractionEnabled = false
        morePlayerViewTopAnchor?.constant = 0
        
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseIn) {
            self.morePlayerViewFader.alpha = 0
            self.view.layoutIfNeeded()
        } completion: { (_ ) in
            
        }
    }
    
    private func moreViewPosition(_ y: CGFloat){
        if y < 0 {
            if let currentHeightMoreView = currentHeightMoreView{
                let constant = -currentHeightMoreView + abs(y)
                morePlayerViewTopAnchor?.constant = constant
                
                let alpha = (currentHeightMoreView - abs(y)) / currentHeightMoreView
                morePlayerViewFader.alpha = alpha
            }
        }
    }
    
    private func moreViewPositionEnded() {
        if let currentHeightMoreView = currentHeightMoreView, let nowPosition = morePlayerViewTopAnchor?.constant {
            if abs(nowPosition) < ((currentHeightMoreView / 3) * 2.3) {
                closeMoreViewPlayerController()
            }else{
                openMoreView()
            }
        }
    }
    
    private func copyText(_ string: String){
        UIPasteboard.general.string = string
        delegateNV?.showNotificationView(image: UIImage(systemName: "checkmark.circle.fill"), title: "Скопировано")//checkmark.circle
    }
    
    private func shareTrack(_ string: String){
        if let shareUrl = URL(string: Social.AppStore){
            let text = "Я слушаю \(string) в мобильном приложении Радио ВАНЯ. Присоединяйся и ты!"
            let vc = UIActivityViewController(activityItems: [text, shareUrl], applicationActivities: [])
            present(vc, animated: true, completion: nil)
        }
    }
    
    
    
    private func editActionPresent(_ item: [[moreTableItem]]){
        
        editActionController.isReload = false
        editActionController.item = item
        let controller = UINavigationController(rootViewController: editActionController)
        present(controller, animated: true) {
            
        }
    }
}






